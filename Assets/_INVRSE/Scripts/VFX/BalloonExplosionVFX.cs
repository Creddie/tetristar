﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonExplosionVFX : MonoBehaviour
{
	[SerializeField] private Color[] possibleColors;
	private ParticleSystem[] vfxs;

	private void Awake()
	{
		vfxs = GetComponentsInChildren<ParticleSystem>();
	}

	private void OnEnable()
	{
		var rando = Random.Range(0, possibleColors.Length);
		var col = possibleColors[rando];
		for (int i = 0; i < vfxs.Length; i++)
		{
			var mod = vfxs[i].main;
			mod.startColor = col;
		}
	}
}