﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectiveTracerType
{
	Collection,
	Hazard
	//UFO
}

public class ObjectiveTracerVFX : MonoBehaviour
{
	private float duration;
	private Vector3 startPos;
	private Transform target;
	private bool isActive;
	private float timer;

	private void Awake()
	{
		duration = GetComponent<TileEffects>().Duration;
	}

	private void Update()
	{
		if(isActive)
		{
			timer += Time.deltaTime;
			if(timer < duration) transform.position = Vector3.Slerp(startPos, target.position, timer / duration);
		}
	}

	public void DoMovement(Vector3 pos)
	{
		startPos = pos;
		target = PlayerHUD.Instance.ObjectiveTracerTargetQueue.First.Value.parent;
		PlayerHUD.Instance.ObjectiveTracerTargetQueue.RemoveFirst();
		isActive = true;
	}

	private void OnDisable()
	{
		if(isActive)
		{
			isActive = false;
			timer = 0f;
			transform.position = target.position;
			var type = PlayerHUD.Instance.ObjectiveTracerTargetTypeQueue.First.Value;
			PlayerHUD.Instance.ObjectiveTracerTargetTypeQueue.RemoveFirst();
			switch (type)
			{
				case ObjectiveTracerType.Collection:
					PlayerHUD.Instance.ExecuteTileCountUpdate(PlayerHUD.Instance.ObjectiveTracerCollectionDataQueue.First.Value);
					PlayerHUD.Instance.ObjectiveTracerCollectionDataQueue.RemoveFirst();
					break;
				case ObjectiveTracerType.Hazard:
					PlayerHUD.Instance.UpdateHazardCounter(1);
					break;
			}

			//PlayerHUD.Instance.UpdateMoveCounters();
			if (null != target)
			{
				var button = target.GetComponent<ButtonTemplate>();
				button.Highlight(true, target);
				button.PlaySound("CelebrationToast");
			}
		}
	}
}