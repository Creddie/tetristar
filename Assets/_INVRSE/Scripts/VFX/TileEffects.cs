﻿using UnityEngine;

public class TileEffects : MonoBehaviour
{
	public float Duration;

	private SpawnPools.DespawnVFXCallback Despawn;
	private int Type;

	private float StartTime;
	private ParticleSystem[] particles;

	private void Awake()
	{
		particles = GetComponentsInChildren<ParticleSystem>();
	}

	public void Init(int type, SpawnPools.DespawnVFXCallback callback)
	{
		Type = type;
		Despawn = callback;
	}

	public void Play()
	{
		foreach (ParticleSystem ps in particles)
		{
			ps.Stop();
			ps.Play();
		}
		StartTime = Time.time;
	}

	private void Update()
	{
		if (Time.time - StartTime > Duration)
			Despawn(this, Type);
	}
}
