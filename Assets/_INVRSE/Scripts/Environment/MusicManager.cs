﻿using System.Collections;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
	public static MusicManager Instance;
	[HideInInspector] public AudioSource ThisAudioSource;
	private float timer;
	public float fadeTime = 1f;
	[HideInInspector] public float volOrig;
	private IEnumerator switchSongCoro;
	private IEnumerator fadingCoro;

	private void Awake()
	{
		if (null != Instance) Destroy(gameObject);
		else
		{
			Instance = this;
			transform.SetParent(null);
			ThisAudioSource = GetComponent<AudioSource>();
			volOrig = ThisAudioSource.volume;
			ThisAudioSource.volume = 0f;
			DontDestroyOnLoad(gameObject);
		}
	}

	//private void Start()
	//{
	//	ThisAudioSource.volume *= SaveLoadManager.VolumeMusic();
	//}

	public void SetupNextSong(float timer, AudioClip song)
	{
		if (null != switchSongCoro) StopCoroutine(switchSongCoro);
		switchSongCoro = WaitingForNextSong(timer, song);
		StartCoroutine(switchSongCoro);
	}

	private IEnumerator WaitingForNextSong(float songTime, AudioClip song)
	{
		yield return new WaitForSecondsRealtime(songTime);
		ThisAudioSource.clip = song;
		ThisAudioSource.Play();
	}

	public void FadeMusic(bool fadeOut, AudioClip newSong = null)
	{
		if (fadeOut && null != switchSongCoro) StopCoroutine(switchSongCoro);
		if (null != fadingCoro) StopCoroutine(fadingCoro);
		fadingCoro = FadingMusic(fadeOut, newSong);
		StartCoroutine(fadingCoro);
	}

	private IEnumerator FadingMusic(bool fadeOut, AudioClip newSong)
	{
		var startVol = ThisAudioSource.volume;
		var endVol = fadeOut ? 0f : volOrig;
		timer = 0f;

		if (!fadeOut)
		{
			if (newSong != null && ThisAudioSource.clip != newSong)
			{
				ThisAudioSource.Stop();
				ThisAudioSource.clip = newSong;
				ThisAudioSource.Play();
			}
		}

		while (timer < fadeTime)
		{
			timer += Time.deltaTime;
			ThisAudioSource.volume = Mathf.Lerp(startVol, endVol, timer / fadeTime) * SaveLoadManager.VolumeMusic();
			yield return new WaitForEndOfFrame();
		}

		ThisAudioSource.volume = endVol * SaveLoadManager.VolumeMusic();

		if (fadeOut)
		{
			if (newSong != null) FadeMusic(false, newSong);
			else ThisAudioSource.Stop();
		}
	}
}