﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetOrbit : MonoBehaviour
{
	[SerializeField]
	private float speed = 10f;
	private Vector3 rotVec;

	private void Awake()
	{
		rotVec = new Vector3(0, speed, 0);
	}

	private void Update()
	{
		transform.localEulerAngles += Time.deltaTime * rotVec;
	}
}