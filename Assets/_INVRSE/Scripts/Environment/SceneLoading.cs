﻿using System.Collections;
using UnityEngine;

public class SceneLoading : MonoBehaviour
{
	public static SceneLoading Instance;

	public float LoadingTime;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		StartCoroutine(Loading());
	}

	private IEnumerator Loading()
	{
		yield return new WaitForSeconds(LoadingTime);
		Player.Instance.FadeEverything(true);
	}
}