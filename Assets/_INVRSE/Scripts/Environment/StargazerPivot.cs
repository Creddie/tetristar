﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StargazerPivot : MonoBehaviour
{
	private void Awake()
	{
		StartCoroutine(WaitingToLoad());
	}

	private IEnumerator WaitingToLoad()
	{
		//yield return new WaitForEndOfFrame();
		//yield return new WaitForEndOfFrame();
		yield return new WaitForSeconds(.1f);
		ChangeState(SaveLoadManager.StargazerMode());
	}

	public void ChangeState(bool activate)
	{
		var xVal = activate ? -90f : 0f;
		transform.localEulerAngles = new Vector3(xVal, transform.localEulerAngles.y, transform.localEulerAngles.z);
	}
}