﻿using UnityEngine;
using Oculus.Platform;

public class OculusStoreIntegration : MonoBehaviour
{
	[SerializeField] private string appId_Rift = "2327014367321011";
	[SerializeField] private string appId_Mobile = "2459010227503165";

	private void Start()
	{
		var headsetType = OVRPlugin.GetSystemHeadsetType().ToString();
		var isMobile = headsetType.Contains("Go") || headsetType.Contains("Gear");
		var id = isMobile ? appId_Mobile : appId_Rift;
		Core.AsyncInitialize(id);
		Entitlements.IsUserEntitledToApplication().OnComplete(CallbackMethod);
	}

	private void CallbackMethod(Message message)
	{
		if (message.IsError) UnityEngine.Application.Quit();
	}
}