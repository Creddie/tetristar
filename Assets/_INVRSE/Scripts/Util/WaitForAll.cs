﻿using System;

public class WaitForAll
{
	public delegate void SimpleCallback();
	private readonly SimpleCallback Callback;

	private int MyLock = 0;

	public WaitForAll(SimpleCallback callback)
	{
		Callback = callback;
	}

	public void Lock()
	{
		MyLock++;
	}

	public void Unlock()
	{
		if (MyLock == 0)
			throw new Exception("Called Unlock() before calling Lock()");
		MyLock--;
		if (MyLock == 0)
			Callback();
	}
}