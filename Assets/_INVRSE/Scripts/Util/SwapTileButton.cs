﻿using UnityEngine;

public class SwapTileButton : MonoBehaviour
{
#if UNITY_EDITOR
	public int ID;

	public void OnClick()
	{
		GetComponentInParent<SwapTileUI>().OnButtonClick(ID);
	}
#endif
}