﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * unity doesn't serialize 2D arrays, so this is a flattened 2D Array
 * 
 * also a previous iteration of this class used generics instead of bools, but unity doesn't serialize those either
 */
[Serializable]
public class Flat2DArrayBool : IEnumerable<HazardLayer.SpawnInfo>
{
	[SerializeField]
	private bool[] Grid;
	[SerializeField]
	private int GridWidth;
	[SerializeField]
	private int GridHeight;
	[SerializeField]
	private int CellWidth;
	[SerializeField]
	private int CellHeight;

	[SerializeField]
	public int OffSetX;
	[SerializeField]
	public int OffSetY;

	[SerializeField]
	private int W;
	[SerializeField]
	private int H;

	[NonSerialized]
	public Vector2 Scroll;

	public Flat2DArrayBool()
	{
		Grid = new bool[0];
		GridWidth = 0;
		GridHeight = 0;
		OffSetX = 0;
		OffSetY = 0;
		CellWidth = 1;
		CellHeight = 1;
		W = 0;
		H = 0;
	}

	public bool Get(int x, int y)
	{
		if (y < OffSetY || x < OffSetX)
			return false;
		y -= OffSetY;
		x -= OffSetX;
		x = x / CellWidth;
		y = y / CellHeight;
		return Grid[x + (y * W)];
	}

	public void Set(int x, int y, bool val)
	{
		if (y < OffSetY || x < OffSetX)
			return;
		y -= OffSetY;
		x -= OffSetX;
		x = x / CellWidth;
		y = y / CellHeight;
		Grid[x + (y * W)] = val;
	}

	public int Width { get { return GridWidth; } }
	public int Height { get { return GridHeight; } }

	public void SetSize(int width, int height, int cellSizeX = 1, int cellSizeY = 1)
	{
		int w = (width / cellSizeX) + (width % cellSizeX != 0 ? 1 : 0);
		int h = (height / cellSizeY) + (height % cellSizeY != 0 ? 1 : 0);
		if (Grid == null)
		{
			Grid = new bool[w * h];
			GridWidth = width;
			GridHeight = height;
			CellWidth = cellSizeX;
			CellHeight = cellSizeY;
			W = w;
			H = h;
			return;
		}
		if (GridWidth == width && GridHeight == height && CellWidth == cellSizeX && CellHeight == cellSizeY)
			return;

		bool[] tmp = Grid;
		Grid = new bool[w * h];
		for (int x = 0; x < w; x++)
			if (x < W)
				for (int y = 0; y < h; y++)
					if (y < H)
						Grid[x + (y * w)] = tmp[x + (y * (GridWidth / CellWidth))];
		GridWidth = width;
		GridHeight = height;
		CellWidth = cellSizeX;
		CellHeight = cellSizeY;
		W = w;
		H = h;
	}

	public void SetAll(bool val)
	{
		for (int i = 0; i < Grid.Length; i++)
			Grid[i] = val;
	}

	public void SetAllColumn(int col, bool val)
	{
		for (int i = 0; i < GridHeight; i++)
			Set(col, i, val);
	}

	public void SetAllRow(int row, bool val)
	{
		for (int i = 0; i < GridWidth; i++)
			Set(i, row, val);
	}

	public IEnumerator<HazardLayer.SpawnInfo> GetEnumerator() { return new SpawnInfoEnumeerator(this); }
	IEnumerator IEnumerable.GetEnumerator() { return new SpawnInfoEnumeerator(this); }

	public class SpawnInfoEnumeerator : IEnumerator<HazardLayer.SpawnInfo>
	{
		private Flat2DArrayBool Array;
		private int i;
		private HazardLayer.SpawnInfo current;

		public SpawnInfoEnumeerator(Flat2DArrayBool array)
		{
			Array = array;
			i = -1;
			current = new HazardLayer.SpawnInfo();
		}

		HazardLayer.SpawnInfo IEnumerator<HazardLayer.SpawnInfo>.Current { get { return current; } }
		object IEnumerator.Current { get { return current; } }

		public void Dispose() { /* nothing */ }

		public bool MoveNext()
		{
			i++;
			while (i < Array.Grid.Length)
			{
				if (Array.Grid[i])
				{
					current = new HazardLayer.SpawnInfo() { coords = new Coords[Array.CellWidth * Array.CellHeight] };
					int y = i / Array.W;
					int x = i % Array.W;
					x *= Array.CellWidth;
					y *= Array.CellHeight;
					x += Array.OffSetX;
					y += Array.OffSetY;

					if (x < Array.GridWidth && y < Array.GridHeight)
					{
						int ai = 0;
						for (int ax = 0; ax < Array.CellWidth; ax++)
						{
							for (int ay = 0; ay < Array.CellHeight; ay++)
							{
								current.coords[ai].x = x + ax;
								current.coords[ai].y = y + ay;
								ai++;
							}
						}
						current.avgCoord.x = ((x) + (x + Array.CellWidth - 1)) / 2f;
						current.avgCoord.y = ((y) + (y + Array.CellHeight - 1)) / 2f;
						current.isTopHalf = ((int)current.avgCoord.y) < Array.GridHeight / 2 + Array.GridHeight % 2;
						return true;
					}
				}
				i++;
			}
			return false;
		}

		public void Reset()
		{
			i = -1;
		}
	}
}
