﻿using System.Collections.Generic;
using UnityEngine;

public class MiniSpawnPool<T> where T : MonoBehaviour
{
	private readonly LinkedList<T> Pool;
	private readonly Transform Parent;
	private readonly GameObject Prefab;

	public MiniSpawnPool(string name, Transform parent, GameObject spawnPrefab)
	{
		Parent = new GameObject(name).transform;
		Parent.SetParent(parent, false);

		Prefab = spawnPrefab;

		Pool = new LinkedList<T>();
	}

	public T Spawn()
	{
		if (Pool.First == null)
			return Object.Instantiate(Prefab, null, false).GetComponent<T>();
		else
		{
			T result = null;
			result = Pool.First.Value;
			Pool.RemoveFirst();
			result.gameObject.SetActive(true);
			return result;
		}
	}

	public void Despawn(T obj)
	{
		Pool.AddFirst(obj);
		obj.transform.SetParent(Parent, false);
		obj.gameObject.SetActive(false);
	}
}