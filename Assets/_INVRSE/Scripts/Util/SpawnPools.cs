﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnPools
{
	public delegate void DespawnVFXCallback(TileEffects me, int type);
	public delegate void DespawnScoreCallback(SlotScoreCard me);

	private readonly SharedGameSettings Settings;
	private readonly Transform TilesParent;
	private readonly Transform VFXParent;
	private readonly Transform ScoreParent;
	private readonly LinkedList<GameObject>[] TilePools;
	private readonly LinkedList<TileEffects>[] VFXTilePools;
	private readonly LinkedList<SlotScoreCard> ScorePools;

	private int TypeNormalVFX { get { return Settings.Tiles.Length; } }
	private int TypeExplosionVFX { get { return Settings.Tiles.Length + 1; } }
	[HideInInspector] public int TypeInitVFX { get { return Settings.Tiles.Length + 2; } }
	[HideInInspector] public int TypeObjectiveCompleteTracerVFX { get { return Settings.Tiles.Length + 3; } }
	[HideInInspector] public int TypeUFOLand { get { return Settings.Tiles.Length + 4; } }

	public SpawnPools(SharedGameSettings settings, Transform parent)
	{
		Settings = settings;

		TilesParent = new GameObject("Game Tiles").transform;
		TilesParent.SetParent(parent, false);
		VFXParent = new GameObject("VFX Particles").transform;
		VFXParent.SetParent(parent, false);
		ScoreParent = new GameObject("Score Toasts").transform;
		ScoreParent.SetParent(parent, false);

		TilePools = new LinkedList<GameObject>[settings.Tiles.Length];
		for (int i = 0; i < TilePools.Length; i++)
			TilePools[i] = new LinkedList<GameObject>();

		VFXTilePools = new LinkedList<TileEffects>[settings.Tiles.Length + 5];
		for (int i = 0; i < VFXTilePools.Length; i++)
			if (i >= settings.Tiles.Length || settings.Tiles[i].VFXPrefabOverride != null)
				VFXTilePools[i] = new LinkedList<TileEffects>();

		ScorePools = new LinkedList<SlotScoreCard>();
	}

	public GameTile SpawnTile(int type)
	{
		GameObject result;
		LinkedList<GameObject> l = TilePools[type];
		if (l.First == null)
		{
			SharedGameSettings.GameTileInfo tileInfo = Settings.Tiles[type];
			result = Object.Instantiate(tileInfo.TilePrefab, null, false);
			Animation a = result.AddComponent<Animation>();
			a.playAutomatically = false;
			foreach (AnimationClip c in Settings.TileAnimations)
				a.AddClip(c, c.name);
			a.clip = Settings.TileAnimations[0];
			GameTile tilComp = null;
			GameObject vfxSpecialHighlight = null;
			GameObject vfxGrab = null;
			if (!Settings.HideAllVFX)
			{
				if (Settings.Tiles[type].isSpecial)
				{
					GameObject go = null;
					//UFO
					if (type == 21)
					{
						go = Settings.UFOHighlightVFX;
						Object.Destroy(tilComp);
						tilComp = result.AddComponent<GameTileUFO>();
						//TODO: maybe target other things sometimes
						result.GetComponent<GameTileUFO>().TargetType = PlayerHUD.Instance.desiredTileTypes[0];
					}
					else go = Settings.SpecialHighlightVFX;
					Object.Instantiate(go, Vector3.zero, Quaternion.identity, result.transform);
				}
				//dormant VFX for special highlight, start drag
				vfxSpecialHighlight = Object.Instantiate(Settings.SpecialRangeHighlightVFX, result.transform.position, result.transform.rotation, result.transform);
				vfxSpecialHighlight.gameObject.SetActive(false);
				vfxGrab = Object.Instantiate(Settings.GrabTileVFX, result.transform.position, result.transform.rotation, result.transform);
			}
			if(null == tilComp) tilComp = result.AddComponent<GameTile>();
			if (null != vfxSpecialHighlight)
			{
				tilComp.SpecialHighlightVFX = vfxSpecialHighlight.GetComponent<ParticleSystem>();
				tilComp.GrabTileVFX = vfxGrab.GetComponent<ParticleSystem>();
			}
			return tilComp;
		}
		else
		{
			result = l.First.Value;
			l.RemoveFirst();
			result.SetActive(true);
			return result.GetComponent<GameTile>();
		}
	}

	public void DespawnTile(GameObject obj, int type)
	{
		TilePools[type].AddFirst(obj);
		obj.transform.SetParent(TilesParent, false);
		obj.SetActive(false);
	}

	public void SpawnVFXExplosion(Transform slot)
	{
		SpawnVFX(TypeExplosionVFX, slot);
	}

	public void SpawnVFX(int type, Transform slot)
	{
		if (Settings.HideAllVFX) return;
		if (VFXTilePools[type] == null) type = TypeNormalVFX;
		LinkedList<TileEffects> l = VFXTilePools[type];
		TileEffects vfx = null;
		if (l.First == null)
		{
			GameObject particlePrefab;
			if (type == TypeNormalVFX) particlePrefab = Settings.NormalComboVFX;
			else if (type == TypeExplosionVFX) particlePrefab = Settings.NormalExplosionVFX;
			else if (type == TypeInitVFX) particlePrefab = Settings.InitTileSpawnFX;
			else if (type == TypeObjectiveCompleteTracerVFX) particlePrefab = Settings.ObjectiveCompleteTracerVFX;
			else if (type == TypeUFOLand) particlePrefab = Settings.UFOLandVFX;
			else
				particlePrefab = Settings.Tiles[type].VFXPrefabOverride;
			vfx = Object.Instantiate(particlePrefab, VFXParent).GetComponent<TileEffects>();
			vfx.Init(type, DespawnVFX);
		}
		else
		{
			vfx = l.First.Value;
			l.RemoveFirst();
		}
		vfx.transform.SetParent(slot, false);
		vfx.transform.localPosition = Vector3.zero;
		vfx.gameObject.SetActive(true);
		vfx.Play();
		if (type == TypeObjectiveCompleteTracerVFX)
		{
			vfx.transform.SetParent(VFXParent, false);
			vfx.GetComponent<ObjectiveTracerVFX>().DoMovement(slot.position);
		}
	}

	private void DespawnVFX(TileEffects vfx, int type)
	{
		vfx.gameObject.SetActive(false);
		vfx.transform.SetParent(VFXParent, false);
		VFXTilePools[type].AddFirst(vfx);
	}

	public void SpawnScore(int score, Transform slot)
	{
		SlotScoreCard card = null;
		if (ScorePools.First == null)
		{
			card = Object.Instantiate(Settings.ScoreCardPrefab, ScoreParent).GetComponent<SlotScoreCard>();
			card.Despawn = DespawnScore;
			card.transform.localPosition = Vector3.zero;
		}
		else
		{
			card = ScorePools.First.Value;
			ScorePools.RemoveFirst();
		}
		card.transform.SetParent(slot, false);
		card.UpdateScore(score);
		card.gameObject.SetActive(true);
	}

	private void DespawnScore(SlotScoreCard card)
	{
		card.gameObject.SetActive(false);
	}
}