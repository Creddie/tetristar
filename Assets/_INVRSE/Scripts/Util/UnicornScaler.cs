﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnicornScaler : MonoBehaviour
{
	[SerializeField] private float scale = 150;

	private void Awake()
	{
		transform.localScale *= scale;
		enabled = false;
	}
}