﻿using UnityEngine;
using UnityEngine.UI;

public class SwapTileUI : MonoBehaviour
{
#if UNITY_EDITOR
	public static GameObject Current;

	public SharedGameSettings Settings;
	public RectTransform ButtonParent;
	public GameObject ButtonPrefab;
	public Text TitleText;

	public GameTile ToSwap { get; set; }

	private void OnEnable()
	{
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
	}

	private void OnDisable()
	{
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
	}

	private void Start()
	{
		if (Current != null)
			Destroy(Current);
		Current = transform.parent.gameObject;
		for (int i = 0; i < Settings.Tiles.Length; i++)
		{
			GameObject btn = Instantiate(ButtonPrefab, ButtonParent, false);
			btn.GetComponentInChildren<Text>().text = Settings.Tiles[i].Name;
			btn.GetComponent<SwapTileButton>().ID = i;
			if (Settings.Tiles[i].isSpecial)
				btn.GetComponent<Image>().color = Settings.SwapSpecialTint;
		}
		ButtonParent.sizeDelta = new Vector2(ButtonParent.sizeDelta.x, (Settings.Tiles.Length + 1) * ButtonPrefab.GetComponent<RectTransform>().sizeDelta.y);
		TitleText.text = "Swap: " + Settings.Tiles[ToSwap.Type].Name + "(" + ToSwap.X + "," + ToSwap.Y + ")";
	}

	public void OnButtonClick(int ID)
	{
		if (ID != -1)
			ToSwap.EditorSwapTile(ID);
		Destroy(gameObject);
	}
#endif
}