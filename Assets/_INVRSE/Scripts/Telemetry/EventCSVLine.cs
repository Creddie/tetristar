﻿using UnityEngine.SceneManagement;

public class EventCSVLine
{
	public static string HeaderRow()
	{
		return "Event ID," +
				"Timestamp," +
				"Session ID," +
				"Device ID," +
				"Level Name," +
				"Event Type," +
				"Move Type," +
				//"Swipe Direction," +
				"Chain Start ID," +
				"Chain Depth Count," +
				"Time Taken," +
				"Moves Taken," +
				"Total Score," +
				"Time in Story," +
				"Level Rating\n";
	}

	public override string ToString()
	{
		return EventID + "," +
				Timestamp + "," +
				TelemetryManager.SessionID + "," +
				TelemetryManager.DeviceID + "," +
				LevelName + "," +
				Type + "," +
				PlayerMoveType + "," +
				//PlayerSwipeDirection + "," +
				ChainStartEventID + "," +
				ChainDepthCount + "," +
				TimeTaken + "," +
				MovesTaken + "," +
				TotalScore + "," +
				TimeInStory + "," +
				LevelRating + "\n";
	}

	public enum EventType
	{
		LoadLevel,
		FinishLevel,
		PlayerMove,
		ChainMatch,
		ChainSpecial,
		PlayerQuit,
		LevelSurvey,
		ShuffleBoard,
	}

	public enum MoveType
	{
		MiscAction,
		SwipeAndMatch,
		ActivateSpecial,
		Chain,
		FailedSwap
	}

	public readonly string EventID;
	public readonly string Timestamp;
	public readonly string LevelName;
	public readonly EventType Type;

	public MoveType PlayerMoveType;
	//public SwipeDirection PlayerSwipeDirection;
	public string ChainStartEventID;
	public string ChainDepthCount;
	//for level end
	public string TimeTaken;
	public string MovesTaken;
	public string TotalScore;
	public string TimeInStory;
	public string LevelRating;
	//for level start
	//public string HazardCount;
	//public string NullTileCount;

	public EventCSVLine(EventType type)
	{
		EventID = System.Guid.NewGuid().ToString();
		Timestamp = System.DateTime.UtcNow.ToLocalTime().ToString();
		LevelName = SceneManager.GetActiveScene().name;
		Type = type;
	}
}