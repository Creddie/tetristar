﻿public class SaveDataCSV
{
	public static string HeaderRow()
	{
		return "Key," +
				"Value," +
				"Value Secondary\n";
	}

	public override string ToString()
	{
		return Key + "," +
				Value + "," +
				ValueSecondary + "\n";
	}

	public readonly string Key;
	public readonly string Value;
	public readonly string ValueSecondary;

	public SaveDataCSV(string key, int value, int valueSecondary = 0)
	{
		Key = key;
		Value = value.ToString();
		ValueSecondary = valueSecondary.ToString();
	}

	//public SaveDataCSV(string key, float value)
	//{
	//	Key = key;
	//	Value = value.ToString();
	//}
}