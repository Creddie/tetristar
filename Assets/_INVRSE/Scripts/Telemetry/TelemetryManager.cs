﻿using System.IO;
using UnityEngine;
using System.Collections.Generic;

public class TelemetryManager
{
	public static readonly string DeviceID;
	public static readonly string SessionID;
	private static readonly string EventLogFileName = Application.persistentDataPath + "/telemetry_events.csv";
	private static readonly string MatchLogFileName = Application.persistentDataPath + "/telemetry_matches.csv";

	public static string LastMoveID { get; private set; }

	static TelemetryManager()
	{
		DeviceID = SystemInfo.deviceUniqueIdentifier;
		SessionID = System.Guid.NewGuid().ToString();

		if (!File.Exists(EventLogFileName)) File.WriteAllText(EventLogFileName, EventCSVLine.HeaderRow());
		if (!File.Exists(MatchLogFileName)) File.WriteAllText(MatchLogFileName, MatchCSVLine.HeaderRow());
		Debug.Log("----------");
		Debug.Log(EventLogFileName);
		Debug.Log(MatchLogFileName);
		Debug.Log("----------");
	}

	public static void LogEvent(EventCSVLine evt, MatchCSVLine[] matches = null)
	{
		if (evt.Type == EventCSVLine.EventType.PlayerMove) LastMoveID = evt.EventID;

		File.AppendAllText(EventLogFileName, evt.ToString());
		if (matches != null)
			foreach (MatchCSVLine m in matches)
				File.AppendAllText(MatchLogFileName, m.ToString());
	}
}