﻿using System.IO;
using UnityEngine;
using System.Collections.Generic;

//TODO: combine similar methods into more generic ones

public class SaveLoadManager
{
	#region Variables
	//level progression
	public static List<LevelSaveDataInfo> LevelSaveData = new List<LevelSaveDataInfo>();
	private static readonly string fileNameLevelProgression = Application.persistentDataPath + "/data_level_progression.csv";
	private static string textStringLevelProgression;
	private static string[] entriesLevelProgression;
	//specials unlocked
	public static Dictionary<string, string> SpecialTooltipsData = new Dictionary<string, string>();
	private static readonly string fileNameSpecialTooltips = Application.persistentDataPath + "/data_special_tooltips.csv";
	private static string textStringSpecialTooltips;
	private static string[] entriesSpecialTooltips;
	private static string[] specialTooltipKeys = new string[6] { "tooltip_cone", "tooltip_color", "tooltip_blaster", "tooltip_5x5", "tooltip_cross", "tooltip_half_cross" };
	//options
	public static Dictionary<string, string> OptionsData = new Dictionary<string, string>();
	private static readonly string fileNameOptions = Application.persistentDataPath + "/data_options.csv";
	private static string textStringOptions;
	private static string[] entriesOptions;
	public static string[] OptionsKeys = new string[8] { "safety_mode", "stargazer_mode", "invert_controls", "volume_overall", "volume_music", "volume_sfx", "gender", "player_distance" };
	public static bool HeroIsMale = true;

	public class LevelSaveDataInfo
	{
		public string LevelKey;
		public string Highscore;
		public string StarsEarned;
	}
	#endregion

	#region Data Test
	public static bool TryShowSpecialTooltip(BombType type, bool save)
	{
		if (PlayerHUD.TooltipIsActive) return false;
		var val = 0;
		switch (type)
		{
			case BombType.BlasterAuto: val = 2; break;
			case BombType.ColorBomb: val = 1; break;
			case BombType.ConeBomb3x: break;
			case BombType.FullCross: val = 4; break;
			case BombType.Neighbors5x5: val = 3; break;
			case BombType.HalfCross: val = 5; break;
			default: return false;
		}
		foreach (var key in SpecialTooltipsData.Keys)
		{
			if (key == specialTooltipKeys[val])
			{
				if (SpecialTooltipsData[key] == 1.ToString()) return false;
				else
				{
					if (save)
					{
						textStringSpecialTooltips = textStringSpecialTooltips.Replace(key + "," + 0.ToString(), key + "," + 1.ToString());
						File.WriteAllText(fileNameSpecialTooltips, textStringSpecialTooltips);
						UpdateSpecialTooltipsTextString();
						DynamicTooltipManager.Instance.TryShowTooltip(type, false, true);
					}
					return true;
				}
			}
		}
		return false;
	}

	public static float PlayerDistance()
	{
		var val = 0;
		foreach (var v in OptionsData)
		{
			if (v.Key == OptionsKeys[7])
			{
				int.TryParse(v.Value, out val);
				return val / 100f;
			}
		}
		return val;
	}

	public static bool SafetyMode()
	{
		var val = false;
		foreach (var v in OptionsData)
			if (v.Key == OptionsKeys[0])
				val = v.Value == 0.ToString() ? false : true;
		return val;
	}

	public static bool StargazerMode()
	{
		var val = false;
		foreach (var v in OptionsData)
			if (v.Key == OptionsKeys[1])
				val = v.Value == 0.ToString() ? false : true;
		return val;
	}

	public static bool InvertedControls()
	{
		var val = false;
		foreach (var v in OptionsData)
			if (v.Key == OptionsKeys[2])
				val = v.Value == 0.ToString() ? false : true;
		return val;
	}

	public static float VolumeOverall()
	{
		foreach (var v in OptionsData)
		{
			if (v.Key == OptionsKeys[3])
			{
				var i = 0;
				int.TryParse(v.Value, out i);
				return i / 100f;
			}
		}
		return 1f;
	}

	public static float VolumeMusic()
	{
		foreach (var v in OptionsData)
		{
			if (v.Key == OptionsKeys[4])
			{
				var i = 0;
				int.TryParse(v.Value, out i);
				return i / 100f;
			}
		}
		return 1f;
	}

	public static float VolumeSFX()
	{
		foreach (var v in OptionsData)
		{
			if (v.Key == OptionsKeys[5])
			{
				var i = 0;
				int.TryParse(v.Value, out i);
				return i / 100f;
			}
		}
		return 1f;
	}

	private static void SetGender()
	{
		foreach (var v in OptionsData)
		{
			if (v.Key == OptionsKeys[6])
			{
				var i = 0;
				int.TryParse(v.Value, out i);
				HeroIsMale = i == 0 ? false : true;
				return;
			}
		}
		//in case file was created before this option was added
		var data = new SaveDataCSV(OptionsKeys[6], 100);
		SaveOptionsData(data);
	}
	#endregion

	#region Init
	public static void Init(LevelList levels)
	{
		if (!File.Exists(fileNameLevelProgression)) InitLevelProgression(levels);
		else UpdateLevelProgressionTextString();

		if (!File.Exists(fileNameSpecialTooltips)) InitSpecialTooltips();
		UpdateSpecialTooltipsTextString();

		if (!File.Exists(fileNameOptions)) InitOptions();
		UpdateOptionsTextString();
		SetGender();
	}

	private static void InitOptions()
	{
		File.WriteAllText(fileNameOptions, SaveDataCSV.HeaderRow());
		SaveDataCSV data = null;
		for (int i = 0; i < OptionsKeys.Length; i++)
		{
			var val = i == 0 || i > 2 ? 100 : 0;   //safety mode is on by default, volumes default to 1
			data = new SaveDataCSV(OptionsKeys[i], val);
			File.AppendAllText(fileNameOptions, data.ToString());
		}
	}

	private static void InitSpecialTooltips()
	{
		File.WriteAllText(fileNameSpecialTooltips, SaveDataCSV.HeaderRow());
		SaveDataCSV data = null;
		for (int i = 0; i < specialTooltipKeys.Length; i++)
		{
			data = new SaveDataCSV(specialTooltipKeys[i], 0);
			File.AppendAllText(fileNameSpecialTooltips, data.ToString());
		}
	}

	public static void InitLevelProgression(LevelList levels)
	{
		File.WriteAllText(fileNameLevelProgression, SaveDataCSV.HeaderRow());
		UpdateLevelProgressionTextString();
		for (int i = 2; i < levels.Levels.Length; i++)  //skip start and map scenes
		{
			if (i == 5) continue;   //cutscene
			var data = new SaveDataCSV("highscore_" + levels.LevelNames[i], 0);
			SaveLevelProgression(data);
		}
	}
	#endregion

	#region Update Data
	public static void ResetDataNewGame(LevelList levels)
	{
		File.Delete(fileNameLevelProgression);
		InitLevelProgression(levels);
		File.Delete(fileNameSpecialTooltips);
		InitSpecialTooltips();
		UpdateSpecialTooltipsTextString();

		var val = HeroIsMale ? 100 : 0;
		var data = new SaveDataCSV(OptionsKeys[6], val);
		File.Delete(fileNameOptions);
		InitOptions();
		UpdateOptionsTextString();
		SaveOptionsData(data);
	}

	private static void UpdateLevelProgressionTextString()
	{
		textStringLevelProgression = File.ReadAllText(fileNameLevelProgression);
		entriesLevelProgression = textStringLevelProgression.Split(new char[] { '\n' });
		LoadLevelProgressionData();
	}

	private static void UpdateSpecialTooltipsTextString()
	{
		textStringSpecialTooltips = File.ReadAllText(fileNameSpecialTooltips);
		entriesSpecialTooltips = textStringSpecialTooltips.Split(new char[] { '\n' });
		LoadSpecialTooltipData();
	}

	private static void UpdateOptionsTextString()
	{
		textStringOptions = File.ReadAllText(fileNameOptions);
		entriesOptions = textStringOptions.Split(new char[] { '\n' });
		LoadOptionsData();
	}
	#endregion

	#region Loading
	public static void LoadLevelProgressionData()
	{
		LevelSaveData.Clear();
		for (int i = 1; i < entriesLevelProgression.Length - 1; i++)
		{
			var entryData = entriesLevelProgression[i].Split(new char[] { ',' });
			var saveData = new LevelSaveDataInfo
			{
				LevelKey = entryData[0],
				Highscore = entryData[1],
				StarsEarned = entryData[2]
			};
			LevelSaveData.Add(saveData);
		}
	}

	public static void LoadSpecialTooltipData()
	{
		SpecialTooltipsData.Clear();
		for (int i = 1; i < entriesSpecialTooltips.Length - 1; i++)
		{
			var entryData = entriesSpecialTooltips[i].Split(new char[] { ',' });
			SpecialTooltipsData.Add(entryData[0], entryData[1]);
		}
	}

	private static void LoadOptionsData()
	{
		OptionsData.Clear();
		for (int i = 1; i < entriesOptions.Length - 1; i++)
		{
			var entryData = entriesOptions[i].Split(new char[] { ',' });
			OptionsData.Add(entryData[0], entryData[1]);
		}
	}
	#endregion

	#region Saving
	public static void SaveLevelProgression(SaveDataCSV data)
	{
		for (int i = 1; i < entriesLevelProgression.Length - 1; i++)
		{
			var entryData = entriesLevelProgression[i].Split(new char[] { ',' });
			var key = entryData[0];
			if (key == data.Key)
			{
				var currentValueInt = 0;
				var currentValueSecondaryInt = 0;
				var dataValueInt = 0;
				var dataValueSecondaryInt = 0;
				int.TryParse(entryData[1], out currentValueInt);
				int.TryParse(entryData[2], out currentValueSecondaryInt);
				int.TryParse(data.Value, out dataValueInt);
				int.TryParse(data.ValueSecondary, out dataValueSecondaryInt);
				var newValue = Mathf.Max(currentValueInt, dataValueInt);
				var newValueSecondary = Mathf.Max(currentValueSecondaryInt, dataValueSecondaryInt);
				textStringLevelProgression = textStringLevelProgression.Replace(key + "," + currentValueInt + "," + currentValueSecondaryInt, key + "," + newValue.ToString() + "," + newValueSecondary.ToString());
				File.WriteAllText(fileNameLevelProgression, textStringLevelProgression);
				UpdateLevelProgressionTextString();
				return;
			}
		}
		File.AppendAllText(fileNameLevelProgression, data.ToString());
		UpdateLevelProgressionTextString();
	}

	public static void SaveOptionsData(SaveDataCSV data)
	{
		for (int i = 0; i < entriesOptions.Length - 1; i++)
		{
			var entryData = entriesOptions[i].Split(new char[] { ',' });
			var key = entryData[0];
			if (key == data.Key)
			{
				var currentValueInt = 0;
				int.TryParse(entryData[1], out currentValueInt);
				textStringOptions = textStringOptions.Replace(key + "," + currentValueInt, key + "," + data.Value.ToString());
				File.WriteAllText(fileNameOptions, textStringOptions);
				UpdateOptionsTextString();
				return;
			}
		}
		File.AppendAllText(fileNameOptions, data.ToString());
		UpdateOptionsTextString();
	}
	#endregion
}