﻿public class MatchCSVLine
{
	public static string HeaderRow()
	{
		return "Match ID," +
				"Event ID," +
				"Tile Type," +
				"Tile Count," +
				"Score\n";
	}

	public override string ToString()
	{
		return MatchID + "," +
				EventID + "," +
				TileType + "," +
				TileCount + "," +
				Score + "\n";
	}

	public readonly string MatchID;
	public readonly string EventID;
	public readonly string TileType;
	public readonly int TileCount;
	public readonly int Score;

	public MatchCSVLine(string EventID, string TileType, int TileCount, int Score)
	{
		this.EventID = EventID;
		this.TileType = TileType;
		this.TileCount = TileCount;
		this.Score = Score;
		MatchID = System.Guid.NewGuid().ToString();
	}
}