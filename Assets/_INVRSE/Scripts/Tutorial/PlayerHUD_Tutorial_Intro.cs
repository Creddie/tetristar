﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUD_Tutorial_Intro : PlayerHUD
{
	[SerializeField] private float delayBetweenTips = 1f;
	[SerializeField] private float initialChangeDelay = 10f;
	[SerializeField] private float secondaryChangeDelay = 2f;
	private int cycles;
	private bool canProgress;

	public override void IncreaseMoveCounter()
	{
		if (!canProgress) return;
		base.IncreaseMoveCounter();
		if (MovesTaken == 1 && cycles < 2)
		{
			if (cycles == 0)
			{
				StoryManager.Instance.Tooltips[cycles].GetComponent<WordBubbleScaler>().ChangeState(false);
				cycles += 1;
			}
			StartCoroutine(WaitingToChange());
		}
		else if(MovesTaken == 2) StartCoroutine(WaitingForFinalFade());
	}

	private IEnumerator WaitingToChange()
	{
		cycles += 1;
		if(StoryManager.Instance.Tooltips[cycles - 1].activeSelf)
			StoryManager.Instance.Tooltips[cycles - 1].GetComponent<WordBubbleScaler>().ChangeState(false);
		yield return new WaitForSeconds(delayBetweenTips);
		StoryManager.Instance.Tooltips[cycles].SetActive(true);
		yield return new WaitForSeconds(secondaryChangeDelay);
		canProgress = true;
	}

	public void InitialChange(float delay)
	{
		StartCoroutine(WaitingForInitialChange(delay));
	}

	private IEnumerator WaitingForInitialChange(float delay)
	{
		yield return new WaitForSeconds(initialChangeDelay + delay);
		StartCoroutine(WaitingToChange());
	}

	private IEnumerator WaitingForFinalFade()
	{
		var v = StoryManager.Instance.Tooltips[cycles].GetComponent<WordBubbleScaler>();
		v.ChangeState(false);
		yield return new WaitForSeconds(v.ScaleTime);
		StoryManager.Instance.ToggleTutorial(false);
	}

	public override void CheckTileCount(GameTile val)
	{
		if(canProgress && GameLevel.Instance.Logic.CascadeComboCount == 1) base.CheckTileCount(val);
	}
}