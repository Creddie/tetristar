﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUD_Tutorial_Hazards : PlayerHUD
{
	private bool done;
	[SerializeField] private float tooltipTimer = 5f;
	private IEnumerator tooltipCoro;

	public void StartTooltip(float delay)
	{
		tooltipCoro = WaitingToChangeTooltips(delay);
		StartCoroutine(tooltipCoro);
	}

	public override void DecreaseHazardCounter(Transform pos)
	{
		base.DecreaseHazardCounter(pos);
		if(!done)
		{
			done = true;
			StartCoroutine(WaitingToEnd());
			StopCoroutine(tooltipCoro);
		}
	}

	private IEnumerator WaitingToEnd()
	{
		if (StoryManager.Instance.tutorialWindow.activeSelf)
		{
			var index = StoryManager.Instance.Tooltips[0].activeSelf ? 0 : 1;
			var p = StoryManager.Instance.Tooltips[index].GetComponent<WordBubbleScaler>();
			p.ChangeState(false);
			yield return new WaitForSeconds(p.ScaleTime);
			StoryManager.Instance.ToggleTutorial(false);
		}
	}

	private IEnumerator WaitingToChangeTooltips(float delay)
	{
		yield return new WaitForSeconds(tooltipTimer + delay);
		if(!StoryManager.Instance.tutorialWindow.activeSelf)
		{
			StoryManager.Instance.tutorialWindow.SetActive(true);
			StoryManager.Instance.ToggleTutorial(true);
			tooltipCoro = WaitingToChangeTooltips(0f);
			StartCoroutine(tooltipCoro);
		}
		else
		{
			var p = StoryManager.Instance.Tooltips[0].GetComponent<WordBubbleScaler>();
			p.ChangeState(false);
			yield return new WaitForSeconds(p.ScaleTime);
			StoryManager.Instance.Tooltips[1].SetActive(true);
		}
	}
}