﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonBatch : MonoBehaviour
{
	public static BalloonBatch Instance;

	public GameObject[] Balloons;

	private void Awake()
	{
		Instance = this;
		for (int i = 1; i < Balloons.Length; i++) Balloons[i].SetActive(false);
	}
}