﻿public class GameLogic_Balloons : GameLogic
{
	public GameLogic_Balloons(GameLevel lvl) : base(lvl) { }

	protected override BombType GetBombToSpawn(TileMatch m)
	{
		if (CascadeComboCount > 1) return BombType.NOT_A_BOMB;
		else if (m.IsBomb || m.TilesMatched.Length <= 3) return BombType.NOT_A_BOMB;
		else
		{
			PlayerHUD_Tutorial_Specials.InstanceSpecial.MakeSpecial();
			return BombType.HalfCross;
		}
	}

	protected override void ClearTiles(TileMatch m)
	{
		PlayerHUD_Tutorial_Specials.InstanceSpecial.AddBalloon(m.TilesMatched[0].transform.parent);
		base.ClearTiles(m);
	}

	public override void DetonateBomb(GameTile bomb, SwipeDirection dir, int type = -1)
	{
		PlayerHUD_Tutorial_Specials.InstanceSpecial.UseSpecial(bomb.transform.parent);
		base.DetonateBomb(bomb, dir, type);
	}
}