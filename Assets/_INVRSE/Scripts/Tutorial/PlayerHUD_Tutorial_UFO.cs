﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUD_Tutorial_UFO : PlayerHUD
{
	[Header("Tutorial Settings")]
	[SerializeField] private float delayBetweenTips = 1f;
	[SerializeField] private float initialTooltipChangeTime = 5f;
	[SerializeField] private GameObject tutorialWindow;
	[SerializeField] private float tutorialScaleTime = .5f;
	[SerializeField] private float tutorialDelayTime = 2f;
	[SerializeField] private GameObject[] tooltips;

	private bool hasLoaded;
	private float scaleTimer;
	private bool readyToStop;
	private Vector3 tutorialWindowNormalSize;

	protected override void Start()
	{
		base.Start();
		var p = FindObjectOfType<Player>();
		tutorialWindowNormalSize = tutorialWindow.transform.localScale;
		var val = GameLevel.Instance.Settings.TutorialWindowOffset;
		var offset = SaveLoadManager.StargazerMode() ? new Vector3(val.x, val.z, val.y) : val;
		tutorialWindow.transform.position = p.transform.position + offset;
		tutorialWindow.transform.SetParent(p.transform.parent, true);
		tutorialWindow.transform.localScale = Vector3.zero;
	}

	public void StartTooltips()
	{
		if(!hasLoaded)
		{
			hasLoaded = true;
			StartCoroutine(WaitingToStart());
		}
	}

	public void ToggleTutorial(bool getLit)
	{
		if (null == tutorialWindow) return;
		StartCoroutine(ScalingTutorial(getLit));
	}

	private IEnumerator ScalingTutorial(bool getLit)
	{
		scaleTimer = 0f;
		//var start = tutorialWindow.transform.localScale;
		//var end = getLit ? Vector3.one : Vector3.zero;
		while (scaleTimer < tutorialScaleTime)
		{
			scaleTimer += Time.deltaTime;
			//tutorialWindow.transform.localScale = Vector3.Lerp(start, end, scaleTimer / tutorialScaleTime);
			var curve = getLit ? GameLevel.Instance.Settings.SizeCurveTooltipGrow : GameLevel.Instance.Settings.SizeCurveTooltipShrink;
			tutorialWindow.transform.localScale = curve.Evaluate(scaleTimer / tutorialScaleTime) * tutorialWindowNormalSize;
			yield return new WaitForEndOfFrame();
		}
		//tutorialWindow.transform.localScale = end;
	}

	#region Tooltips
	private IEnumerator WaitingToStart()
	{
		yield return new WaitForSeconds(tutorialDelayTime);
		ToggleTutorial(true);
		tooltips[0].SetActive(true);
		yield return new WaitForSeconds(initialTooltipChangeTime);
		StartCoroutine(ChangingTooltips());
	}

	private IEnumerator ChangingTooltips()
	{
		tooltips[0].GetComponent<WordBubbleScaler>().ChangeState(false);
		yield return new WaitForSeconds(delayBetweenTips);
		tooltips[1].SetActive(true);
		readyToStop = true;
	}

	public void StopTooltips()
	{
		if (!readyToStop && !hasLoaded) return;
		hasLoaded = true;
		StartCoroutine(FadingOutTooltips());
	}

	private IEnumerator FadingOutTooltips()
	{
		var v = tooltips[1].GetComponent<WordBubbleScaler>();
		v.ChangeState(false);
		yield return new WaitForSeconds(v.ScaleTime);
		ToggleTutorial(false);
	}
	#endregion

	public override bool DidPlayerWin()
	{
		for (int i = 0; i < desiredTileTypes.Length; i++)
		{
			if (tileCounts[i] < desiredTileCounts[i])
				return false;
		}
		StopTooltips();
		return true;
	}
}