﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD_Tutorial_Specials : PlayerHUD
{
	public static PlayerHUD_Tutorial_Specials InstanceSpecial;
	private int collected;
	[HideInInspector] public int CurrentStage;
	private int specialsUsed;
	private IEnumerator waitingCoro;
	private int cycles;

	[Header("Tutorial Settings")]
	[SerializeField] private float delayBetweenTips = 1f;
	private bool madeSpecial;
	[SerializeField] private float offsetHUD = 10f;	//because balloons
	[SerializeField] private float initialTooltipChangeTime = 5f;

	protected override void Awake()
	{
		base.Awake();
		InstanceSpecial = this;
	}

	protected override void Start()
	{
		base.Start();
		transform.position = new Vector3(transform.position.x, transform.position.y + offsetHUD, transform.position.z);
	}

	private void OnDisable()
	{
		InstanceSpecial = null;
	}

	protected override void Setup()
	{
		textComps[0].transform.parent.gameObject.SetActive(false);
		for (int i = 3; i < textComps.Length; i++)
			textComps[i].transform.parent.gameObject.SetActive(false);

		var comp = textComps[1];
		comp.GetComponentInParent<Image>().sprite = desiredTileSprites[0];
		UpdateTileCollectionCounter(0);
		comp = textComps[2];
		comp.GetComponentInParent<Image>().sprite = desiredTileSprites[1];
		collected = -1;
		UpdateTileCollectionCounter(1);
		//adjust UI bend
		GetComponentInChildren<CurvedUI.CurvedUISettings>().Angle = Mathf.RoundToInt(22.5f);
	}

	public void InitialChange(float delay)
	{
		StartCoroutine(WaitingForInitialChange(delay));
	}

	private IEnumerator WaitingForInitialChange(float delay)
	{
		yield return new WaitForSeconds(initialTooltipChangeTime + delay);
		StartCoroutine(ChangingTooltips());
	}

	public override void UpdateTileCollectionCounter(int i)
	{
		switch(i)
		{
			case 0:
				if (specialsUsed > desiredTileCounts[0]) return;
				textComps[1].SetText(Mathf.Max(0, (desiredTileCounts[0] - specialsUsed)).ToString());
				if (specialsUsed >= desiredTileCounts[0]) CurrentStage += 1;
				break;
			case 1:
				if (collected >= desiredTileCounts[1]) return;
				collected += 1;
				 textComps[2].SetText(Mathf.Max(0, (desiredTileCounts[1] - collected)).ToString());
				if (collected >= desiredTileCounts[1]) CurrentStage += 1;
				BalloonBatch.Instance.Balloons[collected].SetActive(true);
				break;
		}
	}

	public override void ExecuteTileCountUpdate(int val)
	{
		UpdateTileCollectionCounter(val);
	}

	public void AddBalloon(Transform location)
	{
		if (collected >= desiredTileCounts[1]) return;
		if (GameLevel.Instance.Settings.HideAllVFX)
		{
			UpdateTileCollectionCounter(1);
			if (collected >= desiredTileCounts[1]) CurrentStage += 1;
		}
		else
		{
			ObjectiveTracerTargetTypeQueue.AddLast(ObjectiveTracerType.Collection);
			ObjectiveTracerCollectionDataQueue.AddLast(1);
			SpawnTieBetweenLevelAndHUD(location, textComps[2].transform);
		}
	}

	public void MakeSpecial()
	{
		if (madeSpecial || cycles == 0) return;
		madeSpecial = true;
		StartCoroutine(ChangingTooltips());
	}

	public void UseSpecial(Transform location)
	{
		if (specialsUsed >= desiredTileCounts[0] || cycles < 2) return;
		if (specialsUsed == 0) StartCoroutine(FadingOutTooltips());
		specialsUsed += 1;
		if (GameLevel.Instance.Settings.HideAllVFX)
		{
			UpdateTileCollectionCounter(0);
			if (specialsUsed >= desiredTileCounts[0]) CurrentStage += 1;
		}
		else
		{
			ObjectiveTracerTargetTypeQueue.AddLast(ObjectiveTracerType.Collection);
			ObjectiveTracerCollectionDataQueue.AddLast(0);
			SpawnTieBetweenLevelAndHUD(location, textComps[1].transform);
		}
	}

	#region Tooltips
	private IEnumerator ChangingTooltips()
	{
		StoryManager.Instance.Tooltips[cycles].GetComponent<WordBubbleScaler>().ChangeState(false);
		cycles++;
		yield return new WaitForSeconds(delayBetweenTips);
		StoryManager.Instance.Tooltips[cycles].SetActive(true);
	}

	private IEnumerator FadingOutTooltips()
	{
		var v = StoryManager.Instance.Tooltips[cycles].GetComponent<WordBubbleScaler>();
		v.ChangeState(false);
		yield return new WaitForSeconds(v.ScaleTime);
		StoryManager.Instance.ToggleTutorial(false);
	}
	#endregion
}