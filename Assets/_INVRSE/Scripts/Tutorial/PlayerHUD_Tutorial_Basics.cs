﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUD_Tutorial_Basics : PlayerHUD
{
	public static bool CanSwipe = true;

	[Header("Tutorial References")]
	[SerializeField] private WordBubbleScaler vignetteRotate;
	[SerializeField] private WordBubbleScaler vignetteMatch;
	private Transform player;
	public GameObject PlayerHandOverride;
	private IEnumerator tooltipCoro;


	[Header("Tutorial Settings")]
	[SerializeField] private int checkInterval = 10;
	private int frameCounter;
	private bool doneSwitch;
	[SerializeField] private float delayBetweenTooltips = 1f;
	[SerializeField] private float tooltipDelay = 15f;
	[SerializeField] private float spinThreshold = 80f;

	protected override void Awake()
	{
		base.Awake();
		vignetteMatch.gameObject.SetActive(false);
		player = FindObjectOfType<BlasterManager>().transform;
		GameLevel.IsInteractive = true;
		tooltipCoro = WaitingForTooltip();
		StartCoroutine(tooltipCoro);
	}

	protected override void Start()
	{
		base.Start();
		CanSwipe = false;
	}

	protected override void Update()
	{
		base.Update();
		frameCounter++;
		if(frameCounter == checkInterval)
		{
			frameCounter = 0;
			var v = SaveLoadManager.StargazerMode() ? player.localEulerAngles.z : player.localEulerAngles.y;
			v = v > 180f ? -(360f - v) : v;
			if(!doneSwitch && (v > spinThreshold || v < -spinThreshold))
			{
				doneSwitch = true;
				vignetteRotate.ChangeState(false);
				CanSwipe = true;
				StopCoroutine(tooltipCoro);
				tooltipCoro = WaitingForTooltip();
				StartCoroutine(tooltipCoro);
				StartCoroutine(WaitingToChange());
				if (StoryManager.Instance.tutorialWindow.activeSelf) StoryManager.Instance.Tooltips[0].GetComponent<WordBubbleScaler>().ChangeState(false);
			}
		}
	}

	private IEnumerator FadingTutorial()
	{
		var v = StoryManager.Instance.Tooltips[0].GetComponent<WordBubbleScaler>();
		yield return new WaitForSeconds(v.ScaleTime);
		StoryManager.Instance.ToggleTutorial(false);
	}

	private IEnumerator WaitingForTooltip()
	{
		yield return new WaitForSeconds(tooltipDelay);
		var index = doneSwitch ? 1 : 0;
		StoryManager.Instance.Tooltips[index].SetActive(true);
		if(!doneSwitch) StoryManager.Instance.StartTutorial();
	}

	private IEnumerator WaitingToChange()
	{
		GameLevel.Instance.RevealBoard(true);
		yield return new WaitForSeconds(delayBetweenTooltips);
		vignetteMatch.gameObject.SetActive(true);
	}

	public override void IncreaseMoveCounter()
	{
		base.IncreaseMoveCounter();
		if(MovesTaken == 1)
		{
			vignetteMatch.ChangeState(false);
			StopCoroutine(tooltipCoro);
			if (StoryManager.Instance.Tooltips[1].activeSelf)
			{
				StoryManager.Instance.Tooltips[1].GetComponent<WordBubbleScaler>().ChangeState(false);
				StartCoroutine(FadingTutorial());
			}
		}
	}
}