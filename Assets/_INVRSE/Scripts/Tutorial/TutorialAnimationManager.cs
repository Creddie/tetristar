﻿using UnityEngine;

public class TutorialAnimationManager : MonoBehaviour
{
	[SerializeField] private GameObject mobileController;
	[SerializeField] private GameObject touchController;
	[SerializeField] private Renderer[] buttonRenderers;
	[SerializeField] private Material highlightMat;
	private Material originalMat;
	[SerializeField] private bool isTrigger;
	private bool isTouchController;

	private void Awake()
	{
		var nam = OVRPlugin.GetSystemHeadsetType().ToString();
		if (nam.Contains("Go") || nam.Contains("Gear"))
		{
			originalMat = buttonRenderers[0].material;
			touchController.SetActive(false);
		}
		else
		{
			isTouchController = true;
			originalMat = buttonRenderers[1].material;
			mobileController.SetActive(false);
		}
	}

	public void ToggleButtonHighlight(int getLit)
	{
		if (null == PlayerHandRemote.InstanceRemote) return;
		var mat = getLit == 1 ? highlightMat : originalMat;
		var renderer = isTouchController ? buttonRenderers[1] : buttonRenderers[0];
		renderer.material = mat;
		renderer = isTrigger ? PlayerHandRemote.InstanceRemote.Trigger : PlayerHandRemote.InstanceRemote.Touchpad;
		renderer.material = mat;
	}

	private void OnDisable()
	{
		if (null == PlayerHandRemote.InstanceRemote) return;
		var renderer = isTrigger ? PlayerHandRemote.InstanceRemote.Trigger : PlayerHandRemote.InstanceRemote.Touchpad;
		renderer.material = originalMat;
	}
}