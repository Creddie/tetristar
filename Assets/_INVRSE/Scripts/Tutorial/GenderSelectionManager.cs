﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenderSelectionManager : MonoBehaviour
{
	[SerializeField] private GameObject male;
	[SerializeField] private GameObject female;

	private void Start()
	{
		male.SetActive(SaveLoadManager.HeroIsMale);
		female.SetActive(!SaveLoadManager.HeroIsMale);
	}
}