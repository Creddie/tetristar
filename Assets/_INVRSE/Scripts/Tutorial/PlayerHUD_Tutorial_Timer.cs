﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUD_Tutorial_Timer : PlayerHUD
{
	[Header("Tutorial Settings")]
	[SerializeField] private float delayBetweenTips = 1f;
	[SerializeField] private float initialTooltipChangeTime = 5f;
	[SerializeField] private GameObject tutorialWindow;
	[SerializeField] private float tutorialScaleTime = 1f;
	[SerializeField] private float tutorialDelayTime = 2f;
	[SerializeField] private GameObject[] tooltips;

	private float scaleTimer;
	private Vector3 tutorialWindowNormalSize;

	protected override void Start()
	{
		base.Start();
		tutorialWindowNormalSize = tutorialWindow.transform.localScale;
		tutorialWindow.transform.SetParent(Player.Instance.transform.parent, true);
		tutorialWindow.transform.localScale = Vector3.zero;
		StartCoroutine(WaitingToLoad());
	}

	private IEnumerator WaitingToLoad()
	{
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		var val = GameLevel.Instance.Settings.TutorialWindowOffset;
		var offset = SaveLoadManager.StargazerMode() ? new Vector3(val.x, val.z, -val.y) : val;
		tutorialWindow.transform.localPosition = Player.Instance.transform.localPosition + offset;
	}

	#region Tooltips
	public void StartTooltip()
	{
		StartCoroutine(WaitingToStart());
	}

	private IEnumerator WaitingToStart()
	{
		yield return new WaitForSeconds(tutorialDelayTime);
		ToggleTutorial(true);
		//tooltips[0].SetActive(true);
		yield return new WaitForSeconds(initialTooltipChangeTime);
		StartCoroutine(ChangingTooltips());
	}

	public void ToggleTutorial(bool getLit)
	{
		if (null == tutorialWindow) return;
		StartCoroutine(ScalingTutorial(getLit));
	}

	private IEnumerator ScalingTutorial(bool getLit)
	{
		scaleTimer = 0f;
		//var start = tutorialWindow.transform.localScale;
		//var end = getLit ? Vector3.one : Vector3.zero;
		while (scaleTimer < tutorialScaleTime)
		{
			scaleTimer += Time.deltaTime;
			//tutorialWindow.transform.localScale = Vector3.Lerp(start, end, scaleTimer / tutorialScaleTime);
			var curve = getLit ? GameLevel.Instance.Settings.SizeCurveTooltipGrow : GameLevel.Instance.Settings.SizeCurveTooltipShrink;
			tutorialWindow.transform.localScale = curve.Evaluate(scaleTimer / tutorialScaleTime) * tutorialWindowNormalSize;
			yield return new WaitForEndOfFrame();
		}
		//tutorialWindow.transform.localScale = end;
	}

	private IEnumerator ChangingTooltips()
	{
		tooltips[0].GetComponent<WordBubbleScaler>().ChangeState(false);
		yield return new WaitForSeconds(delayBetweenTips);
		tooltips[1].SetActive(true);
		yield return new WaitForSeconds(initialTooltipChangeTime);
		StopTooltips();
	}

	public void StopTooltips()
	{
		StartCoroutine(FadingOutTooltips());
	}

	private IEnumerator FadingOutTooltips()
	{
		var v = tooltips[1].GetComponent<WordBubbleScaler>();
		v.ChangeState(false);
		yield return new WaitForSeconds(v.ScaleTime);
		ToggleTutorial(false);
	}
	#endregion
}