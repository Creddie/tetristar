﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicTooltipManager : MonoBehaviour
{
	public static DynamicTooltipManager Instance;

	[Header("References")]
	[SerializeField] private WordBubbleScaler tooltipCone;
	[SerializeField] private WordBubbleScaler tooltipBlaster;
	[SerializeField] private WordBubbleScaler tooltip5x5;
	[SerializeField] private WordBubbleScaler tooltipColor;
	[SerializeField] private WordBubbleScaler tooltipFullCross;
	[SerializeField] private WordBubbleScaler tooltipHalfCross;
	[SerializeField] private WordBubbleScaler tooltipHelpMatch;
	[SerializeField] private GameObject[] maleStencils;
	[SerializeField] private GameObject[] femaleStencils;

	private void Awake()
	{
		Instance = this;
		StartCoroutine(WaitingToSetup());
	}

	private IEnumerator WaitingToSetup()
	{
		yield return new WaitForSecondsRealtime(.1f);
		if (SaveLoadManager.HeroIsMale) foreach (var v in maleStencils) v.SetActive(true);
		else foreach (var v in femaleStencils) v.SetActive(true);
	}

	public void TryShowTooltip(BombType type, bool getLit, bool bypass)
	{
		if (PlayerHUD.TooltipIsActive && getLit) return;
		if (bypass || SaveLoadManager.TryShowSpecialTooltip(type, false))
		{
			WordBubbleScaler val = null;
			switch (type)
			{
				case BombType.BlasterAuto: val = tooltipBlaster; break;
				case BombType.ColorBomb: val = tooltipColor; break;
				case BombType.ConeBomb3x: val = tooltipCone; break;
				case BombType.FullCross: val = tooltipFullCross; break;
				case BombType.Neighbors5x5: val = tooltip5x5; break;
				case BombType.HalfCross: val = tooltipHalfCross; break;
				///TODO: override other active tooltips
				default: val = tooltipHelpMatch; break;
			}
			if(getLit)
			{
				if (val.gameObject.activeSelf) val.ChangeState(true);
				else val.gameObject.SetActive(true);
			}
			else if(val.gameObject.activeSelf) val.ChangeState(false);
		}
	}
}