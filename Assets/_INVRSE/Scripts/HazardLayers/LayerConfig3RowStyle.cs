﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewLayer", menuName = "Tetristar/Layer Config (3 Row Style)")]
public class LayerConfig3RowStyle : LayerConfig
{
	public GameObject topRowTilePrefab;
	public GameObject MidTopRowTilePrefab;
	public GameObject CenterRowTilePrefab;

	public override Hazard InstantiatePrefab(HazardLayer.SpawnInfo spawn)
	{
		GameObject prefab = null;
		switch((int) spawn.avgCoord.y)
		{
			case 0:
			case 5:
				prefab = topRowTilePrefab;
				break;
			case 1:
			case 4:
				prefab = MidTopRowTilePrefab;
				break;
			case 2:
			case 3:
			default:
				prefab = CenterRowTilePrefab;
				break;
		}
		Hazard h = Instantiate(prefab).AddComponent<Hazard>();
		h.Slots = spawn.coords;
		if (!spawn.isTopHalf)
			h.transform.Rotate(new Vector3(0, 0, 180));
		return h;
	}
}