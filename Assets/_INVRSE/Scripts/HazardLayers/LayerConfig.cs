﻿using UnityEngine;

public abstract class LayerConfig : ScriptableObject
{
	public enum AlignType { None, AlignNormals, RotateYOnly }

	public int TileSizeX = 1;
	public int TileSizeY = 1;
	public AlignType Align = AlignType.None;
	public float ScaleOverride = 0;
	public GameObject DisappearVFX;

	public abstract Hazard InstantiatePrefab(HazardLayer.SpawnInfo spawn);


}