﻿using UnityEngine;

/**
 * Each Hazard Layer instantiates and points to the layer beneath it.
 */
public class HazardLayer : MonoBehaviour
{
	public enum TintType { NoTint, TintAllRenderers, TintFirstRenderer }

	public struct SpawnInfo
	{
		public Coords[] coords;
		public bool isTopHalf;
		public Vector2 avgCoord;
	}

	[System.Serializable]
	public struct HazardLayerInstance
	{
		public LayerConfig LayerConfigTemplate;
		public TintType TintType;
		public Color TintColor;
		public Flat2DArrayBool Grid;
	}

	[HideInInspector]
	public Transform HazardsParent;
	[HideInInspector]
	public HazardLayer Next;
	[HideInInspector]
	public GameObject DisappearVFX;

	private Hazard[,] Hazards;
	private MiniSpawnPool<TileEffects> SpawnPool;
	private SharedGameSettings Settings;

	public void Init(GameLevel level)
	{
		Settings = level.Settings;
		Hazards = new Hazard[level.GridSizeX, level.GridSizeY];

		for (int i = 0; i < HazardsParent.childCount; i++)
		{
			Hazard haz = HazardsParent.GetChild(i).GetComponent<Hazard>();
			foreach (Coords c in haz.Slots)
				Hazards[c.x, c.y] = haz;
		}

		SpawnPool = new MiniSpawnPool<TileEffects>("VFX Pool", transform, DisappearVFX);

		if (Next != null)
			Next.Init(level);
	}

	public bool BreakHazard(int x, int y)
	{
		Hazard h = Hazards[x, y];
		if (h != null)
		{
			h.gameObject.SetActive(false);
			foreach (Coords c in h.Slots) Hazards[c.x, c.y] = null;
			if (!Settings.HideAllVFX)
			{
				TileEffects vfx = SpawnPool.Spawn();
				vfx.Init(0, DespawnEffect);
				vfx.transform.SetParent(transform, false);
				vfx.transform.position = h.transform.position;
				vfx.Play();
			}
			GameLevel.Instance.PlaySound(x, y, "RemoveObstruction");
			PlayerHUD.Instance.DecreaseHazardCounter(h.transform);
			return true;
		}
		else if (Next != null) return Next.BreakHazard(x, y);
		else return false;

	}

	private void DespawnEffect(TileEffects me, int type)
	{
		SpawnPool.Despawn(me);
	}

	public bool CheckIfPlayerWon()
	{
		foreach (Hazard h in Hazards)
			if (h != null)
				return false;
		if (Next != null)
			return Next.CheckIfPlayerWon();
		return true;
	}
}