﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewLayer", menuName = "Tetristar/Layer Config (Default Style)")]
public class LayerConfigDefaultStyle : LayerConfig
{
	public GameObject LayerTilePrefab;

	public override Hazard InstantiatePrefab(HazardLayer.SpawnInfo spawn)
	{
		Hazard h = Instantiate(LayerTilePrefab).AddComponent<Hazard>();
		h.Slots = spawn.coords;
		return h;
	}
}