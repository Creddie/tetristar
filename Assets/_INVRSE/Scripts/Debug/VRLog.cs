﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class VRLog : MonoBehaviour
{
	private static VRLog instance = null;

	public static void Log(string s)
	{
		if (instance != null)
			instance.Print(s);
	}

	public int LogLimit = 25;

	private Text t;
	private LinkedList<string> log;

	private void Awake()
	{
		instance = this;
		t = GetComponent<Text>();
		t.text = "";
		log = new LinkedList<string>();
	}

	public void Print(string s)
	{
		if (log.Count > LogLimit)
			log.RemoveLast();
		log.AddFirst(s);

		string result = "";
		foreach (string l in log)
			result += "\n" + l;
		t.text = result;
	}
}