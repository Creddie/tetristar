﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class FPSDisplay : MonoBehaviour
{
	public Text t;
	private float deltaTime = 0f;
	private bool isActive = false;

	private void Awake()
	{
		t.transform.parent.gameObject.SetActive(isActive);
	}

	void Update()
	{
		InputHandler();
		if(isActive) UpdateMeter();
	}

	private void InputHandler()
	{
		if (OVRInput.GetUp(OVRInput.Button.Back) ||
			(((OVRInput.GetUp(OVRInput.RawButton.A) && !PlayerHandManager.IsLeftHanded) ||
			(OVRInput.GetUp(OVRInput.RawButton.X) && PlayerHandManager.IsLeftHanded))
			&& !OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger)))
		{
			isActive = !isActive;
			t.transform.parent.gameObject.SetActive(isActive);
		}
	}

	private void UpdateMeter()
	{
		deltaTime += (Time.unscaledDeltaTime - deltaTime) * .1f;
		float msec = deltaTime * 1000f;
		float fps = 1f / deltaTime;
		string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
		t.text = text;
	}
}