﻿public enum SwipeDirection
{
	Undefined,
	Up,
	Down,
	Left,
	Right
}