﻿using UnityEngine;

public interface VRInteractable
{
	void OnPointerEnter();
	void OnPointerExit();
	void OnPointerClick();
	void OnDrag(Vector3 delta);
	bool OnDragEnd();
	void OnTouchDrag(Vector2 delta);
	void OnTouchDragEnd();
	bool IsDraggable();
	bool IsTouchDraggable();
}