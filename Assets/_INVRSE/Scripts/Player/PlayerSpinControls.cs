﻿using UnityEngine;

public class PlayerSpinControls : MonoBehaviour, VRInteractable
{
	public static PlayerSpinControls Instance;
	public static bool IsSpinning;
	public static bool IsDragging;

	public float GridRadiusBuffer = 20f;
	public float StarRotateAngularDrag = 100;
	public float StarRotationStopThreshold = 0.00001f;
	public float StarRotateForce = 1f;
	public float StarRotateMaxSpeed = 500f;
	public float RotationMaxAngle = 60f;
	public float JoyStickScale = 1f;
	public float JoyStickThreshold = 0.05f;

	//[HideInInspector] public Vector3 RotationSpeed = Vector3.zero;
	private Vector3 RotationSpeed = Vector3.zero;
	private Vector3 RotationAngle = Vector3.zero;
	//private Vector2 TouchDragDistance = Vector2.zero;
	//private bool TouchDragThresholdHit = false;

	private void Awake()
	{
		Instance = this;
		SphereCollider coll = GetComponent<SphereCollider>();
		GameLevel lvl = FindObjectOfType<GameLevel>();
		if (lvl != null) coll.radius = lvl.GridRadius + GridRadiusBuffer;
		else coll.enabled = false;
	}

	public void OnDrag(Vector3 delta)
	{
		float x = Vector3.Dot(delta, transform.right);
		float y = Vector3.Dot(delta, transform.up);
		RotationSpeed += new Vector3(y, -x, 0) * StarRotateForce;
		if (RotationSpeed.magnitude > StarRotateMaxSpeed)
			RotationSpeed = RotationSpeed.normalized * StarRotateMaxSpeed;
	}

	private void FixedUpdate()
	{
		if(PlayerHandManager.Instance.BothHands() && !Player.IsMoving && !IsDragging)
		{
			//var activeStick = PlayerHandManager.IsLeftHanded ? OVRInput.RawAxis2D.LThumbstick : OVRInput.RawAxis2D.RThumbstick;
			//var stickInput = OVRInput.Get(activeStick);
			//var left = OVRInput.Get(OVRInput.RawAxis2D.LThumbstick);
			//var right = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
			//var stickInput = Mathf.Abs(left.magnitude) > Mathf.Abs(right.magnitude) ? left : right;
			var stickInput = OVRInput.Get(OVRInput.RawAxis2D.LThumbstick) + OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
			//Vector3 joysticks = new Vector3(Input.GetAxis("Vertical"), -Input.GetAxis("Horizontal"), 0);
			var joysticks = new Vector3(stickInput.y, -stickInput.x, 0f);
			//Debug.Log(joysticks.magnitude);
			if (joysticks.magnitude >= JoyStickThreshold)
			{
				RotationSpeed = joysticks * JoyStickScale * StarRotateForce;
				if (!IsSpinning)
				{
					IsSpinning = true;
					PlayerHand.Instance.Fader.ChangeFade(true);
				}
			}
			else
			{
				IsSpinning = false;
				PlayerHand.Instance.Fader.ChangeFade(false);
			}
		}

        if (RotationSpeed == Vector3.zero) return;
		RotationAngle += RotationSpeed * Time.unscaledDeltaTime;
		RotationAngle = new Vector3(Mathf.Min(RotationMaxAngle, Mathf.Max(-RotationMaxAngle, RotationAngle.x)), RotationAngle.y, RotationAngle.z);
		SetSpinRotation();
		RotationSpeed *= Mathf.Min(StarRotateAngularDrag * Time.deltaTime, 0.95f);
		if (RotationSpeed.magnitude < StarRotationStopThreshold) RotationSpeed = Vector3.zero;
	}

	public void SetSpinRotation()
	{
		var upwardDirection = SaveLoadManager.StargazerMode() ? Vector3.back : Vector3.up;
		transform.parent.localRotation = Quaternion.Inverse(Quaternion.AngleAxis(RotationAngle.x, Vector3.right) * Quaternion.AngleAxis(RotationAngle.y, upwardDirection));
	}

	//interface functions
	public void OnPointerEnter() { }
	public void OnPointerExit() { }
	public void OnPointerClick() { }
	public bool OnDragEnd() { return false; }
	public bool IsDraggable() { return false; }
	public bool IsTouchDraggable() { return true; }
	public void OnTouchDrag(Vector2 delta)
	{
		//if (TouchDragThresholdHit)
		//	return;

		//TouchDragDistance += delta;
		//float horiz = Vector3.Dot(-transform.right, TouchDragDistance);
		//float vert = Vector3.Dot(transform.up, TouchDragDistance);
		//float absH = Mathf.Abs(horiz);
		//float absV = Mathf.Abs(vert);
		////spin planet
		//if (TouchDragThresholdHit || (absH > GameLevel.Instance.Settings.TouchDragThreshold || absV > GameLevel.Instance.Settings.TouchDragThreshold))
		//{
		//	TouchDragThresholdHit = true;
		//	var newVec = new Vector2(TouchDragDistance.x, -TouchDragDistance.y) * GameLevel.Instance.Settings.TouchDragMagnitude;
		//	newVec = transform.InverseTransformVector(newVec);
		//	OnDrag(newVec);
		//	//RotationSpeed += new Vector3(horiz, -vert, FindObjectOfType<Player>().transform.position.z) * StarRotateForce * GameLevel.Instance.Settings.TouchDragMagnitude;
		//	//if (RotationSpeed.magnitude > StarRotateMaxSpeed)
		//	//	RotationSpeed = RotationSpeed.normalized * StarRotateMaxSpeed;
		//}
	}
	public void OnTouchDragEnd()
	{
		//TouchDragThresholdHit = false;
		//TouchDragDistance = Vector3.zero;
	}
}