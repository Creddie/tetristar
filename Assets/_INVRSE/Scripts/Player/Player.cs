﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
	#region Variables
	public static bool IsMoving;
	public static Player _Instance;
	public static Player Instance
	{
		get
		{
			if(null == _Instance) _Instance = FindObjectOfType<Player>();
			return _Instance;
		}
		set { _Instance = value; }
	}

	public bool IsLevel = true;

	[Header("Debug Options")]
	public bool ShowFramerate;
	public bool ShowVRLog;

	[Header("References")]
	public SharedGameSettings Settings;
	public GameObject FramerateDisplay;
	public GameObject VRLog;
	public FadeVignette Fader;
	public PlayerSpinControls coll;
	public Transform PivotUI;

	[Header("Settings")]
	[SerializeField] private float distanceMovementRate = 1f;
	[SerializeField] private float handChangeDirectionThreshold = .1f;
	[SerializeField] private Vector2 minMaxPlayerDistance;
	private Vector2 minMaxMovementDistanceAdjusted;

	private bool waitingToResume = false;
	private float previousDistanceBetweenHands;
	private float moveStep;
	private Vector3 targetPosition;
	private bool movingCloser;
	private bool isActive;
	//private bool didLoseFocus;

#if UNITY_EDITOR
	[Header("Editor Simulator Settings")]
	public float SimMoveSpeed = 200f;

	private Transform SimWand;
#endif
	#endregion

	private void Awake()
	{
		OVRScreenFade.IsFading = true;
		SaveLoadManager.Init(Settings.LevelList);
		TelemetryManager.LogEvent(new EventCSVLine(EventCSVLine.EventType.LoadLevel));
		FramerateDisplay.SetActive(ShowFramerate);
		VRLog.SetActive(ShowVRLog);
		coll.gameObject.SetActive(IsLevel);
		PivotUI.position = transform.position;
		minMaxMovementDistanceAdjusted = new Vector2(transform.localPosition.z + minMaxPlayerDistance.x, transform.localPosition.z + minMaxPlayerDistance.y);
		isActive = !SceneManager.GetActiveScene().name.Contains("CutScene");
		PlayerHUD_Tutorial_Basics.CanSwipe = true;
		StartCoroutine(WaitingToLoad());

		if (isActive)
		{
			OVRManager.VrFocusLost += () => ChangeHeadsetFocusState(true);
			OVRManager.VrFocusAcquired += () => ChangeHeadsetFocusState(false);
			//Valve.VR.SteamVR_Events.InputFocus.AddListener(delegate { ChangeHeadsetFocusState(!didLoseFocus); });
			//Valve.VR.SteamVR_Events.InputFocus.Listen(delegate { ChangeHeadsetFocusState(!didLoseFocus); });
			//Valve.VR.SteamVR_Settings.instance.pauseGameWhenDashboardVisible = true;
		}

		if (IsLevel)
		{
			var lvl = FindObjectOfType<GameLevel>();
			coll.transform.position = lvl.transform.position;
		}
	}

	private IEnumerator WaitingToLoad()
	{
		yield return new WaitForEndOfFrame();
		MenuScreen.SetPlayerStargazing(true);
		//OVRManager.display.RecenterPose();
	}

	private void Update()
	{
		InputHandler();
	}

	private void InputHandler()
	{
		if(isActive && !BlasterManager.Instance.IsBlasting && PlayerHandManager.Instance.BothHands()) ChangePlayerDistance();
	}

	public void FadeEverything(bool fadeIn)
	{
		PlayerHand.Instance.DisableBeam(PlayerHand.BeamType.Normal);
		PlayerHand.Instance.DisableBeam(PlayerHand.BeamType.PlanetSpin);
		if (fadeIn) OVRScreenFade.Instance.FadeIn();
		else OVRScreenFade.Instance.FadeOut();
	}

	private void ChangePlayerDistance()
	{
		if (OVRInput.Get(OVRInput.RawButton.LHandTrigger) && OVRInput.Get(OVRInput.RawButton.RHandTrigger))
		{
			if(!IsMoving)
			{
				IsMoving = true;
				PlayerHandManager.CanChangeHands = false;
				PlayerHand.Instance.Fader.ChangeFade(true);
				PlayerHand.Instance.DisableBeam(PlayerHand.BeamType.Normal);
				PlayerHand.Instance.DisableBeam(PlayerHand.BeamType.PlanetSpin);
			}
			var hands = FindObjectsOfType<OVRTrackedRemote>();
			var dist = Vector3.Distance(hands[0].transform.position, hands[1].transform.position);
			if (previousDistanceBetweenHands == 0f)
			{
				previousDistanceBetweenHands = dist;
				return;
			}

			///move away
			if (dist > previousDistanceBetweenHands + handChangeDirectionThreshold) movingCloser = true;
			///move closer
			else if(dist < previousDistanceBetweenHands - handChangeDirectionThreshold) movingCloser = false;

			previousDistanceBetweenHands = dist;
			var delta = Time.unscaledDeltaTime * distanceMovementRate;
			delta = movingCloser ? delta : -delta;
			var deltaPosition = SaveLoadManager.StargazerMode() ? transform.localPosition.y + delta : transform.localPosition.z + delta;
			///min / max limits
			if (deltaPosition > minMaxMovementDistanceAdjusted.x || deltaPosition < minMaxMovementDistanceAdjusted.y) return;

			var vec = SaveLoadManager.StargazerMode() ?
				new Vector3(transform.localPosition.x, deltaPosition, transform.localPosition.z) :
				new Vector3(transform.localPosition.x, transform.localPosition.y, deltaPosition);
	
			while (Vector3.Distance(transform.localPosition, vec) > .01f)
			{
				moveStep += Time.unscaledDeltaTime * distanceMovementRate;
				var newPos = Vector3.Lerp(transform.localPosition, vec, moveStep);
				transform.localPosition = newPos;
				PivotUI.localPosition = newPos;
			}

			transform.localPosition = vec;
			PivotUI.localPosition = vec;
		}
		else if(IsMoving)
		{
			IsMoving = false;
			previousDistanceBetweenHands = 0f;
			moveStep = 0f;
			PlayerHandManager.CanChangeHands = true;
			PlayerHand.Instance.Fader.ChangeFade(false);
		}
	}

	private void ChangeHeadsetFocusState(bool lostFocus)
	{
		//Debug.Log("change focus");
		//didLoseFocus = lostFocus;
		if (lostFocus)
		{
			if (MenuScreen.IsPaused) return;
			else waitingToResume = true;
		}
		else if (!waitingToResume) return;
		else waitingToResume = false;
		Time.timeScale = lostFocus ? 0f : 1f;
		MenuScreen.IsPaused = lostFocus;
	}

#if UNITY_EDITOR && !UNITY_STANDALONE
	private void LateUpdate()
	{
		float delta = Time.unscaledDeltaTime;

		if (SimWand == null) GetWand();
		SimWand.gameObject.SetActive(true);

		Transform toSpin;
		if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) toSpin = transform;
		else toSpin = SimWand;

		Quaternion rotX = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * delta * SimMoveSpeed, Vector3.up);
		Quaternion rotY = Quaternion.AngleAxis(-Input.GetAxis("Mouse Y") * delta * SimMoveSpeed, toSpin.right);
		toSpin.rotation = rotY * rotX * toSpin.rotation;

		if (Input.GetKeyDown(KeyCode.R)) toSpin.rotation = transform.rotation;
	}

	private void GetWand()
	{
		PlayerHand hand = GetComponentInChildren<PlayerHand>(true);
		if (hand != null)
		{
			SimWand = hand.transform;
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
		}
	}
#endif
	//TODO: not called when quitting from Oculus Universal Menu
	private void OnApplicationQuit()
	{
		LogQuitEvent();
	}

	private static void LogQuitEvent()
	{
		if (SceneManager.GetActiveScene().buildIndex != 0 && !GameLevel.IsFinished)
		{
			var evt = new EventCSVLine(EventCSVLine.EventType.PlayerQuit);
			evt.TimeTaken = PlayerHUD.LevelTimer.ToString();
			evt.MovesTaken = PlayerHUD.MovesTaken.ToString();
			evt.TotalScore = Scoreboard.LevelScore.ToString();
			evt.TimeInStory = null == StoryManager.Instance ? 0.ToString() : StoryManager.Instance.TimeInStory.ToString();
			TelemetryManager.LogEvent(evt);
		}
	}
}