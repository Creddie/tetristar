﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandRemote : PlayerHand
{
	public static PlayerHandRemote InstanceRemote;

	[Header("Remote References")]
	[HideInInspector] public Renderer Trigger;
	[HideInInspector] public Renderer Touchpad;
	[SerializeField] private GameObject[] remotes;
	[SerializeField] private Renderer[] triggers;
	[SerializeField] private Renderer[] touchpads;

	private void Start()
	{
		///go or gear
		var nam = OVRPlugin.GetSystemHeadsetType().ToString();
		if (nam.Contains("Go") || nam.Contains("Gear"))
		{
			remotes[0].SetActive(true);
			remotes[1].SetActive(false);
			Trigger = triggers[0];
			Touchpad = touchpads[0];
		}
		///rift or quest
		else
		{
			remotes[0].SetActive(false);
			remotes[1].SetActive(true);
			Trigger = triggers[1];
			Touchpad = touchpads[1];
			if (IsLeftHand)
			{
				var scal = remotes[1].transform.localScale;
				var newScal = new Vector3(-scal.x, scal.y, scal.z);
				remotes[1].transform.localScale = newScal;
			}
		}
	}

	protected override void Update()
	{
		if(this != InstanceRemote) InstanceRemote = this;
		base.Update();
	}
}