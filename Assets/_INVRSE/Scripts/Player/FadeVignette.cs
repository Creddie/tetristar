﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeVignette : MonoBehaviour
{
	[SerializeField]
	private Renderer vignette;
	[SerializeField]
	private float vignetteFadeInTime = .5f;
	[SerializeField]
	private float vignetteFadeOutTime = 2f;
	[SerializeField]
	private float fadeOutDelay = .5f;
	private float timer;
	private IEnumerator vignetteCoro;
	[SerializeField]
	private Color fullColor;
	[SerializeField]
	private Color fadedColor;

	private bool IsFadeOn = false;

	private void Awake()
	{
		var col = vignette.material.color;
		vignette.material.color = fadedColor;
		vignette.enabled = false;
	}

	public void ChangeFade(bool fadeIn)
	{
		if (IsFadeOn == fadeIn || !gameObject.activeInHierarchy || (fadeIn && (!PlayerSpinControls.Instance.gameObject.activeSelf || !SaveLoadManager.SafetyMode())))
			return;

		IsFadeOn = fadeIn;
		if (null != vignetteCoro) StopCoroutine(vignetteCoro);
		vignetteCoro = ExecuteFade(fadeIn);
		StartCoroutine(vignetteCoro);
	}

	private IEnumerator ExecuteFade(bool fadeIn)
	{
		if (fadeIn) vignette.enabled = true;
		timer = 0f;
		var beginColor = vignette.material.color;
		var targetColor = fadeIn ? fullColor : fadedColor;
		var tim = fadeIn ? vignetteFadeInTime : vignetteFadeOutTime;
		if (!fadeIn) yield return new WaitForSecondsRealtime(fadeOutDelay);
		while (timer < tim)
		{
			timer += Time.unscaledDeltaTime;
			vignette.material.color = Color.Lerp(beginColor, targetColor, timer / tim);
			yield return new WaitForEndOfFrame();
		}
		vignette.material.color = targetColor;
		if (!fadeIn) vignette.enabled = false;
	}
}