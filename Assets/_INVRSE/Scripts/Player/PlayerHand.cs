﻿using System.Collections;
using UnityEngine;

public class PlayerHand : MonoBehaviour
{
	#region Variables
	public static PlayerHand Instance;

	public enum BeamType
	{
		Normal,
		PlanetSpin,
		RainbowBlaster
	}

	[SerializeField] private SharedGameSettings settings;
	public LayerMask InteractionLayers;
	public LayerMask UIOnlyLayers;
	public LayerMask SpinControlLayer;
	public float RaycastDistance = 1000f;
	public Transform PointerStartingPoint;

	private VRInteractable Spinner;
	private VRInteractable Selected;
	private bool StarHovering = false;
	private bool IsDragging = false;
	private Vector3 CurrentPointHit;
	private Vector3 PreviousPointHit;
	//private Vector2 PreviousTouchCoord;
	//private bool IsTouchDragging = false;
	private IEnumerator pointerChangeCoro;
	[HideInInspector] public bool IsLeftHand;

	[Header("Beams")]
	public ArcReactor_Launcher BlasterBeam;
	[SerializeField] private ArcReactor_Launcher normalBeam;
	[SerializeField] private ArcReactor_Launcher spinPlanetBeam;
	[SerializeField] private float pointerColorChangeTime = .25f;
	private float pointerChangeTimer;

	[Header("Wand")]
	[SerializeField] private Animator wandAnimComp;
	[SerializeField] private GameObject[] wandObject;

	[System.NonSerialized] public FadeVignette Fader;
	[System.NonSerialized] public bool IsLevel;

	private OVRInput.RawButton indexTriggerInput;
	private OVRInput.RawButton handTriggerInput;
	private OVRInput.RawButton touchpadInput;
	#endregion

	protected virtual void Update()
	{
		if (Instance != this)
			if(!PlayerHandManager.Instance.BothHands() || (IsLeftHand && PlayerHandManager.IsLeftHanded) || (!IsLeftHand && !PlayerHandManager.IsLeftHanded))
				Instance = this;

		if (OVRScreenFade.IsFading) return;

		Raycast();
		Buttons();
		Visuals();
		PreviousPointHit = CurrentPointHit;
	}

	private void OnEnable()
	{
		Selected = null;
		IsDragging = false;
		StarHovering = false;
	}

	private void OnDisable()
	{
		Selected = null;
		IsDragging = false;
		StarHovering = false;
		DisableBeam(BeamType.Normal);
		DisableBeam(BeamType.PlanetSpin);
	}

	public void SetInput()
	{
		indexTriggerInput = IsLeftHand ? OVRInput.RawButton.LIndexTrigger : OVRInput.RawButton.RIndexTrigger;
		handTriggerInput = IsLeftHand ? OVRInput.RawButton.LHandTrigger : OVRInput.RawButton.RHandTrigger;
		touchpadInput = IsLeftHand ? OVRInput.RawButton.LTouchpad : OVRInput.RawButton.RTouchpad;
	}

	public void DisableBeam(BeamType type)
	{
		ArcReactor_Launcher beam = null;
		switch (type)
		{
			case BeamType.Normal: beam = normalBeam; break;
			case BeamType.PlanetSpin: beam = spinPlanetBeam; break;
			case BeamType.RainbowBlaster: beam = BlasterBeam; break;
		}
		if (0 != beam.Rays.Count)
		{
			beam.Rays[0].arc.playbackType = ArcReactor_Arc.ArcsPlaybackType.once;
			beam.Rays[0].arc.elapsedTime = beam.Rays[0].arc.lifetime + 1;
		}
	}

	private bool CanInteract()
	{
		return (GameLevel.Instance == null || (GameLevel.Instance != null && GameLevel.IsInteractive)) || !StoryManager.IsDone;
	}

	private void Raycast()
	{
		#region Star/Planet Raycast
		RaycastHit starHit;
		PlayerSpinControls spin = null;
		Physics.Raycast(transform.position, transform.forward, out starHit, RaycastDistance, SpinControlLayer);

		if (starHit.collider != null)
		{
			if ((spin = starHit.collider.GetComponent<PlayerSpinControls>()) != null)
			{
				if (!PlayerHUD_Tutorial_Basics.CanSwipe && !StarHovering) ChangePointerColor(false);
				StarHovering = true;
				Spinner = spin;
			}
			else
			{
				if (!PlayerHUD_Tutorial_Basics.CanSwipe && StarHovering) ChangePointerColor(true);
				StarHovering = false;
			}
			CurrentPointHit = starHit.point;
		}
		else StarHovering = false;
		#endregion

		#region Selectable Obj Raycast
		if (!IsDragging && CanInteract())
		{
			RaycastHit selHit;
			VRInteractable target;
			Physics.Raycast(transform.position, transform.forward, out selHit, RaycastDistance, GameLevel.Instance != null && GameLevel.IsFinished ? UIOnlyLayers : InteractionLayers);
			if (selHit.collider != null && (target = selHit.collider.GetComponent<VRInteractable>()) != null)
			{
				if (BlasterManager.Instance.IsBlasting && StoryManager.IsDone)
				{
					var til = selHit.transform.GetComponent<GameTile>();
					if (null != til)
					{
						if (til.IsBomb)
						{
							if (til.TypeOfBomb == BombType.BlasterAuto || til.TypeOfBomb == BombType.ShuffleBoard) return;
							if (til.TypeOfBomb == BombType.UFO)
							{
								til.ActivateSpecial(false, SwipeDirection.Undefined);
								return;
							}
							if (!BlasterManager.Instance.SpecialsToBlast.Contains(til))
							{
								GameLevel.Instance.Spawner.SpawnVFX(til.Type, GameLevel.Instance.GridSlots[til.X, til.Y].transform);
								GameLevel.Instance.PlaySound(til.X, til.Y, "Explode");
								BlasterManager.Instance.SpecialsToBlast.Add(til);
							}
						}
						else
						{
							GameLevel.Instance.Spawner.SpawnScore(GameLevel.Instance.Logic.GetStandardPoints(1), GameLevel.Instance.GridSlots[til.X, til.Y].transform);
							GameLevel.Instance.Logic.DeativateTile(til);
						}
					}
				}
				else
				{
					if (Selected != target)
					{
						if (Selected != null) Selected.OnPointerExit();
						Selected = target;
						Selected.OnPointerEnter();
						var gt = selHit.collider.GetComponent<GameTile>();
						if (null != gt && (gt.IsBomb || GameLevel.Instance.Logic.CheckForPossibleMatches(gt)) && PlayerHUD_Tutorial_Basics.CanSwipe) ChangePointerColor(false);
					}
					CurrentPointHit = selHit.point;
				}
			}
			else Deselect();
		}
		#endregion

		if (!CanInteract())
		{
			if (Selected != null && IsDragging) Selected.OnDragEnd();
			IsDragging = false;
			Fader.ChangeFade(false);
			Deselect();
		}
	}

	private void Buttons()
	{
		//keys
		bool trigger = OVRInput.Get(indexTriggerInput);
		bool triggerDown = OVRInput.GetDown(indexTriggerInput);
		bool triggerUp = OVRInput.GetUp(indexTriggerInput);
		bool touchpad = OVRInput.Get(touchpadInput) || OVRInput.Get(handTriggerInput);
		bool touchpadDown = OVRInput.GetDown(touchpadInput) || OVRInput.GetDown(handTriggerInput);
		bool touchpadUp = OVRInput.GetUp(touchpadInput) || OVRInput.GetUp(handTriggerInput);
		//swiping
		//bool touchDrag = OVRInput.Get(OVRInput.Touch.PrimaryTouchpad);
		//bool touchDragUp = OVRInput.GetUp(OVRInput.Touch.PrimaryTouchpad);
		//Vector2 touchCoords = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);
#if UNITY_EDITOR
		trigger = trigger || Input.GetMouseButton(0);
		triggerDown = triggerDown || Input.GetMouseButtonDown(0);
		triggerUp = triggerUp || Input.GetMouseButtonUp(0);
		touchpad = touchpad || Input.GetMouseButton(1);
		touchpadDown = touchpadDown || Input.GetMouseButtonDown(1);
		touchpadUp = touchpadUp || Input.GetMouseButtonUp(1);
		bool swap = Input.GetKeyUp(KeyCode.Space);
		//swiping
		//touchDrag = touchDrag || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
		//touchDragUp = touchDragUp || Input.GetKeyUp(KeyCode.LeftControl) || Input.GetKeyUp(KeyCode.RightControl);
		//touchCoords += new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * Level.Settings.TouchDragThreshold;

		if (SwapTileUI.Current != null && (swap || Input.GetKeyUp(KeyCode.Escape)))
		{
			DestroyImmediate(SwapTileUI.Current);
			SwapTileUI.Current = null;
		}
#endif
		if (SaveLoadManager.InvertedControls())
		{
			var b1 = touchpad;
			var b2 = touchpadDown;
			var b3 = touchpadUp;
			touchpad = trigger;
			touchpadDown = triggerDown;
			touchpadUp = triggerUp;
			trigger = b1;
			triggerDown = b2;
			triggerUp = b3;
		}
		//wand anims
		if(null != wandAnimComp && wandObject[0].activeSelf)
		{
			var isIdle = false;
			var isFast = false;
			var isSlow = false;
			if (!trigger && !touchpad) isIdle = true;
			else
			{
				if (trigger) isFast = true;
				if (touchpad) isSlow = true;
			}
			wandAnimComp.SetBool("Idle", isIdle);
			wandAnimComp.SetBool("Fast", isFast);
			wandAnimComp.SetBool("Slow", isSlow);
		}
		//only button interaction when story is playing or game is paused
		if (!StoryManager.IsDone || MenuScreen.IsPaused)
		{
			if (null != Selected)
			{
				if (Selected is MenuSlider && (touchpad || trigger))
				{
					IsDragging = true;
					Selected.OnDrag(PointerStartingPoint.position);
				}
				else if (touchpadUp || triggerUp)
				{
					Selected.OnDragEnd();
					Selected.OnPointerClick();
					IsDragging = false;
				}
			}
			return;
		}
		//planet rotate
		if ((StarHovering || (IsDragging && !MenuScreen.IsPaused)) && (touchpad || touchpadUp))
		{
			if (touchpadUp)
			{
				IsDragging = false;
				if(!PlayerSpinControls.IsSpinning) Fader.ChangeFade(false);
				PlayerSpinControls.IsDragging = false;
				Spinner.OnDragEnd();
				DisableBeam(BeamType.PlanetSpin);
			}
			else if(!Player.IsMoving)
			{
				IsDragging = true;
				PlayerSpinControls.IsDragging = true;
				Fader.ChangeFade(true);
				Spinner.OnDrag(CurrentPointHit - PreviousPointHit);
				Deselect();
				if(StarHovering)
				{
					if (0 == BlasterBeam.Rays.Count && 0 == spinPlanetBeam.Rays.Count)
					{
						spinPlanetBeam.LaunchRay();
						spinPlanetBeam.Rays[0].arc.playbackType = ArcReactor_Arc.ArcsPlaybackType.pingpong;
						DisableBeam(BeamType.Normal);
					}
				}
				else if(0 != spinPlanetBeam.Rays.Count) DisableBeam(BeamType.PlanetSpin);
			}
		}
		else if ((!touchpad || !StarHovering) && 0 != spinPlanetBeam.Rays.Count) DisableBeam(BeamType.PlanetSpin);
		//drag planet with touchpad
		//else if (StarHovering && (touchDrag || touchDragUp))
		//{
		//	if (touchDragUp)
		//	{
		//		IsTouchDragging = false;
		//		Fader.ChangeFade(false);
		//		Spinner.OnTouchDragEnd();
		//	}
		//	else
		//	{
		//		if (!IsTouchDragging)
		//		{
		//			PreviousTouchCoord = touchCoords;
		//			Fader.ChangeFade(true);
		//			IsTouchDragging = true;
		//		}
		//		var swipeSize = touchCoords - PreviousTouchCoord;
		//		VRLog.Log("" + swipeSize);
		//		Spinner.OnTouchDrag(swipeSize);
		//	}
		//}
		else if (Selected != null && PlayerHUD_Tutorial_Basics.CanSwipe)
		{
#if UNITY_EDITOR
			if (swap && Selected is GameTile) ((GameTile)Selected).EditorSwapTileUI();
			else
#endif
			//gems
			if (!BlasterManager.Instance.IsBlasting && (Selected.IsDraggable() || IsDragging) && (trigger || triggerUp))
			{
				if (triggerUp)
				{
					IsDragging = false;
					if (!Selected.OnDragEnd()) Selected.OnPointerClick();
				}
				else
				{
					IsDragging = true;
					Selected.OnDrag(CurrentPointHit - PreviousPointHit);
				}
			}
			//click event
			else if (touchpadUp || triggerUp) Selected.OnPointerClick();
		}
		//PreviousTouchCoord = touchCoords;
	}

	private void Visuals()
	{
		if (settings.HideAllVFX) return;

		//if (Player.IsMoving)
		//{
		//	DisableBeam(BeamType.Normal);
		//	DisableBeam(BeamType.PlanetSpin);
		//	return;
		//}

		if (StarHovering || Selected != null)
		{
			normalBeam.enabled = true;
			if (0 == normalBeam.Rays.Count && 0 == spinPlanetBeam.Rays.Count) normalBeam.LaunchRay();
		}
		else if(normalBeam.enabled)
		{
			normalBeam.enabled = false;
			DisableBeam(BeamType.Normal);
		}
	}

	private void Deselect()
	{
		if (Selected != null)
		{
			if(!PlayerHUD_Tutorial_Basics.CanSwipe && !StarHovering) ChangePointerColor(true);
			Selected.OnPointerExit();
			Selected = null;
		}
	}

	public void ChangePointerColor(bool toNormal)
	{
		//if (!PlayerHUD_Tutorial_Basics.CanSwipe || OVRScreenFade.IsFading) return;
		//if (OVRScreenFade.IsFading) return;
		if (0 == normalBeam.Rays.Count) normalBeam.LaunchRay(); //return;
		if (null != pointerChangeCoro) StopCoroutine(pointerChangeCoro);
		pointerChangeCoro = ChangingPointerColor(toNormal);
		StartCoroutine(pointerChangeCoro);
	}

	private IEnumerator ChangingPointerColor(bool toNormal)
	{
		if (0 == normalBeam.Rays.Count) StopCoroutine(pointerChangeCoro);
		pointerChangeTimer = 0f;
		var arc = normalBeam.Rays[0].arc.arcs[0];
		var start = arc.colorOptions.startColor;
		var end = toNormal ? settings.PointerColorNormal : settings.PointerColorMatch;
		while (pointerChangeTimer < pointerColorChangeTime)
		{
			pointerChangeTimer += Time.unscaledDeltaTime;
			var tim = pointerChangeTimer / pointerColorChangeTime;
			var cols = new GradientColorKey[5];
			cols[0].color = Color.Lerp(start.colorKeys[0].color, end.colorKeys[0].color, tim);
			cols[1].color = Color.Lerp(start.colorKeys[1].color, end.colorKeys[1].color, tim);
			cols[2].color = Color.Lerp(start.colorKeys[2].color, end.colorKeys[2].color, tim);
			cols[3].color = Color.Lerp(start.colorKeys[3].color, end.colorKeys[3].color, tim);
			cols[4].color = Color.Lerp(start.colorKeys[4].color, end.colorKeys[4].color, tim);
			var grad = new Gradient();
			grad.SetKeys(cols, start.alphaKeys);
			arc.colorOptions.startColor = grad;
			yield return new WaitForEndOfFrame();
		}
		arc.colorOptions.startColor = end;
	}
}