﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour
{
	#region Variables
	public static PlayerHUD Instance;
	public static int LevelTimer;
	public static int MovesTaken;
	public static bool TooltipIsActive;

	[Header("Objectives")]
	[SerializeField] private GameObject mainCanvas;
	public int ScoreGoal;
	[SerializeField] protected TextMeshProUGUI[] textComps;
	public int[] desiredTileTypes;
	[SerializeField] protected int[] desiredTileCounts;
	[SerializeField] protected Sprite[] desiredTileSprites;
	protected int[] tileCounts;
	private int offset;
	private int hazardCount;
	[HideInInspector] public LinkedList<Transform> ObjectiveTracerTargetQueue = new LinkedList<Transform>();
	[HideInInspector] public LinkedList<int> ObjectiveTracerCollectionDataQueue = new LinkedList<int>();
	[HideInInspector] public LinkedList<ObjectiveTracerType> ObjectiveTracerTargetTypeQueue = new LinkedList<ObjectiveTracerType>();

	[Header("Time")]
	[SerializeField] private TextMeshProUGUI timerText;
	private float startingTime;
	private ScreenFader faderComp;
	private float fanfareResizeTimer;

	[Header("Debug")]
	public TextMeshProUGUI PossibleMovesText;
	[SerializeField] private TextMeshProUGUI levelNameText;
	#endregion

	#region Setup
	protected virtual void Awake()
	{
		Instance = this;
		tileCounts = new int[desiredTileTypes.Length];
		startingTime = 0f;
		MovesTaken = 0;
		LevelTimer = 0;
		faderComp = GetComponent<ScreenFader>();
		levelNameText.SetText(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
	}

	protected virtual void Start()
	{
		StartCoroutine(WaitingToStart());
	}

	private IEnumerator WaitingToStart()
	{
		yield return new WaitForSeconds(.01f);
		Setup();
		ShowHUD(false);
	}

	protected virtual void Setup()
	{
		for (int i = 0; i < textComps.Length; i++) textComps[i].transform.parent.gameObject.SetActive(false);
		//hazards
		if (GameLevel.Instance.UsesHazards)
		{
			var comp = textComps[1];
			comp.transform.parent.gameObject.SetActive(true);
			hazardCount = FindObjectsOfType<Hazard>().Length;
			comp.SetText(hazardCount.ToString());
			comp.GetComponentInParent<Image>().sprite = desiredTileSprites[0];
			offset = 1;
		}
		//fail by timer
		if(GameLevel.Instance.FailMoverCounter > 0)
		{
			var comp = textComps[1 + offset];
			comp.transform.parent.gameObject.SetActive(true);
			comp.GetComponentInParent<Image>().sprite = desiredTileSprites[offset];
			comp.SetText(Mathf.RoundToInt(GameLevel.Instance.FailMoverCounter).ToString());
			offset += 1;
		}
		//collections
		for (int i = 1 + offset; i < desiredTileTypes.Length + 1 + offset; i++)
		{
			textComps[i].transform.parent.gameObject.SetActive(true);
			textComps[i].SetText(desiredTileCounts[i - (1 + offset)].ToString());
			textComps[i].GetComponentInParent<Image>().sprite = desiredTileSprites[i - 1];
		}
		//adjust UI bend
		var count = 0;
		foreach (var v in textComps)
		{
			if (v.transform.parent.gameObject.activeSelf)
			{
				count += 1;
				v.transform.parent.GetComponent<ScreenFader>().FadeScreen(true, null, null);
			}
		}
		count = Mathf.Max(1, count);
		GetComponentInChildren<CurvedUI.CurvedUISettings>().Angle = Mathf.RoundToInt(22.5f * (count - 1));
	}

	public void StartTimer()
	{
		startingTime = Time.time;
	}
	#endregion

	public void ShowHUD(bool reveal)
	{
		if(reveal) mainCanvas.SetActive(true);
		faderComp.FadeScreen(reveal, mainCanvas, null);
	}

	protected virtual void Update()
	{
		if (!GameLevel.IsFinished && startingTime != 0f)
		{
			LevelTimer = Mathf.RoundToInt(Time.time - startingTime);
			timerText.SetText(LevelTimer.ToString());
		}
	}

	private IEnumerator WaitingToFail()
	{
		while (!GameLevel.IsInteractive) yield return new WaitForEndOfFrame();
		yield return new WaitForSeconds(.5f);
		UpdateFailMoveCounter();
	}

	public void UpdateFailMoveCounter()
	{
		var c = GameLevel.Instance.FailMoverCounter - MovesTaken;
		if (GameLevel.Instance.FailMoverCounter == 0 || c < 0) return;
		var v = 1;
		if (GameLevel.Instance.UsesHazards) v += 1;
		textComps[v].SetText(c.ToString());
		if (c == 0 && !DidPlayerWin() && (!GameLevel.Instance.UsesHazards || 0 != hazardCount)) GameLevel.Instance.LevelFailure();
	}

	#region UFO
	//public void UpdateUFOCounter(GameTile ufo)
	//{
	//	var v = 1;
	//	if (GameLevel.Instance.UsesHazards) v += 1;
	//	if (GameLevel.Instance.FailMoverCounter > 0) v += 1;
	//	if (!GameLevel.Instance.Settings.HideAllVFX)
	//	{
	//		ObjectiveTracerTargetTypeQueue.AddLast(ObjectiveTracerType.UFO);
	//		ObjectiveTracerCollectionDataQueue.AddLast(v);
	//		SpawnTieBetweenLevelAndHUD(ufo.transform, textComps[v].transform);
	//	}
	//	else ExecuteUFOCountUpdate(v);
	//}

	//public void ExecuteUFOCountUpdate(int val)
	//{
	//	GameLevel.SuccessfulUFOs += 1;
	//	textComps[val].SetText((GameLevel.Instance.UFOFailCount - GameLevel.SuccessfulUFOs).ToString());
	//}
	#endregion

	#region Collections
	public virtual void CheckTileCount(GameTile val)
	{
		for (int i = 0; i < desiredTileTypes.Length; i++)
		{
			if (val.Type == desiredTileTypes[i])
			{
				tileCounts[i] += 1;
				if (!GameLevel.Instance.Settings.HideAllVFX)
				{
					var texCom = textComps[i + 1 + offset];
					ObjectiveTracerTargetTypeQueue.AddLast(ObjectiveTracerType.Collection);
					ObjectiveTracerCollectionDataQueue.AddLast(val.Type);
					SpawnTieBetweenLevelAndHUD(val.transform, texCom.transform);
				}
				else UpdateTileCollectionCounter(i);
				break;
			}
		}
	}

	public virtual void ExecuteTileCountUpdate(int val)
	{
		for (int i = 0; i < desiredTileTypes.Length; i++)
		{
			if (val == desiredTileTypes[i])
			{
				UpdateTileCollectionCounter(i);
				break;
			}
		}
	}

	public virtual void UpdateTileCollectionCounter(int i)
	{
		var tex = tileCounts[i] >= desiredTileCounts[i] ? 0 : desiredTileCounts[i] - tileCounts[i];
		var texCom = textComps[i + 1 + offset];
		texCom.SetText(tex.ToString());
	}
	#endregion

	public void SpawnTieBetweenLevelAndHUD(Transform startPt, Transform target)
	{
		var spawna = GameLevel.Instance.Spawner;
		ObjectiveTracerTargetQueue.AddLast(target);
		spawna.SpawnVFX(spawna.TypeObjectiveCompleteTracerVFX, startPt);
	}

	public virtual bool DidPlayerWin()
	{
		if (!GameLevel.Instance.HasCollectionObjective) return false;
		for(int i = 0; i < desiredTileTypes.Length; i++)
		{
			if (tileCounts[i] < desiredTileCounts[i]) return false;
		}
		return true;
	}

	public virtual void IncreaseMoveCounter()
	{
		MovesTaken += 1;
		StartCoroutine(WaitingToFail());
		//UpdateFailMoveCounter();
		//textComps[0].SetText(MovesTaken.ToString());
		GameLevel.Instance.Logic.ResetMatchHelpTooltip();
		if (GameLevel.Instance.UFOSpawnMoveCountIntervals.Length > 0) StartCoroutine(WaitingToSpawnUFO());
	}

	private IEnumerator WaitingToSpawnUFO()
	{
		while (!GameLevel.IsInteractive) yield return new WaitForEndOfFrame();
		foreach (var v in GameLevel.Instance.UFOSpawnMoveCountIntervals)
			if (v == MovesTaken && !GameLevel.IsFinished) GameLevel.Instance.Logic.SpawnUFO();
	}

	#region Hazards
	public virtual void DecreaseHazardCounter(Transform pos)
	{
		if (!GameLevel.Instance.Settings.HideAllVFX)
		{
			hazardCount -= 1;
			var texCom = textComps[1];
			ObjectiveTracerTargetTypeQueue.AddLast(ObjectiveTracerType.Hazard);
			SpawnTieBetweenLevelAndHUD(pos, texCom.transform);
		}
		else UpdateHazardCounter(hazardCount - FindObjectsOfType<Hazard>().Length);
	}

	public void UpdateHazardCounter(int val)
	{
		textComps[1].SetText(hazardCount.ToString());
	}
	#endregion

	public void FanfareResize()
	{
		StartCoroutine(FanfareResizing());
	}

	private IEnumerator FanfareResizing()
	{
		var tim = GameLevel.Instance.Settings.HUDFanfareResizeTime;
		var posNew = new Vector3(mainCanvas.transform.localPosition.x, GameLevel.Instance.Settings.HUDFanfarePosition, mainCanvas.transform.localPosition.z);
		var scalNewX = GameLevel.Instance.Settings.HUDFanfareScale;
		var scalNew = new Vector3(scalNewX, scalNewX, scalNewX);
		var posOld = mainCanvas.transform.localPosition;
		var scalOld = mainCanvas.transform.localScale;
		var faderComps = mainCanvas.GetComponentsInChildren<ScreenFader>();
		foreach (var v in faderComps) v.FadeScreen(false, null, null);
		while(fanfareResizeTimer < tim)
		{
			fanfareResizeTimer += Time.deltaTime;
			var delta = fanfareResizeTimer / tim;
			mainCanvas.transform.localPosition = Vector3.Lerp(posOld, posNew, delta);
			mainCanvas.transform.localScale = Vector3.Lerp(scalOld, scalNew, delta);
			yield return new WaitForEndOfFrame();
		}
	}
}