﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlasterManager : MonoBehaviour
{
	public static BlasterManager Instance;
	public bool IsBlasting;
	public float BlasterDuration = 3f;
	private float timer;
	[HideInInspector] public List<GameTile> SpecialsToBlast = new List<GameTile>();
	private AudioSource blasterAudioSource;

	private ArcReactor_Launcher _blasterBeam;
	public ArcReactor_Launcher BlasterBeam
	{
		get
		{
			if(null == _blasterBeam || !_blasterBeam.gameObject.activeSelf)
			{
				var beams = GetComponentsInChildren<PlayerHand>();
				for(int i = 0; i < beams.Length; i++)
				{
					if (beams[i].gameObject.activeSelf)
					{
						_blasterBeam = beams[i].BlasterBeam;
						blasterAudioSource = _blasterBeam.GetComponent<AudioSource>();
					}
				}
			}
			return _blasterBeam;
		}
	}

	private void Awake()
	{
		Instance = this;
	}

	//private void Start()
	//{
	//	BlasterBeam.LaunchRay();
	//	BlasterBeam.Rays[0].arc.playbackType = ArcReactor_Arc.ArcsPlaybackType.once;
	//	BlasterBeam.Rays[0].arc.elapsedTime = BlasterBeam.Rays[0].arc.lifetime;
	//}

	public void LaunchRay()
	{
		BlasterBeam.LaunchRay();
		SharedGameSettings.SoundEffectInfo s;
		GameLevel.Instance.Settings.GetRandomSound("BlasterBeam", out s);
		blasterAudioSource.PlayOneShot(s.Clip, s.VolumeAdd);
	}

	private void Update()
	{
		if(IsBlasting)
		{
			timer += Time.deltaTime;
			if(timer >= BlasterDuration)
			{
				timer = 0f;
				FinishBlasting();
			}
		}
	}

	private void FinishBlasting()
	{
		IsBlasting = false;
		//trigger specials, or board refill
		if (SpecialsToBlast.Count > 0)
		{
			foreach (GameTile gt in SpecialsToBlast)
			{
				if(gt.TypeOfBomb == BombType.ColorBomb) GameLevel.Instance.Logic.ActivateBomb(gt, false, SwipeDirection.Undefined, GameLevel.Instance.Logic.GetRandomTile());
				else
				{
					var dirs = new SwipeDirection[4] { SwipeDirection.Up, SwipeDirection.Down, SwipeDirection.Left, SwipeDirection.Right };
					var rando = Random.Range(0, dirs.Length);
					var dir = dirs[rando];
					GameLevel.Instance.Logic.ActivateBomb(gt, false, dir);
				}
			}
			SpecialsToBlast.Clear();
		}
		else
		{
			GameLevel.IsInteractive = false;
			GameLevel.Instance.StartCoroutine(GameLevel.Instance.DoAfterSeconds(GameLevel.Instance.Logic.RefillBoard, GameLevel.Instance.Settings.BoardRefillDelay));
		}
		EndBeam();
	}

	private void EndBeam()
	{
		BlasterBeam.Rays[0].arc.playbackType = ArcReactor_Arc.ArcsPlaybackType.once;
	}
}