﻿using UnityEngine;

public class PlayerHandSpawner : MonoBehaviour
{
	[SerializeField] private GameObject PlayerHandPrefab;
	[SerializeField] private bool isLeftHand;

	private void Awake()
	{
		var hud = FindObjectOfType<PlayerHUD_Tutorial_Basics>();
		var prefab = null == hud ? PlayerHandPrefab : hud.PlayerHandOverride;
		GameObject hand = Instantiate(prefab, transform, false);
		OVRTrackedRemote parent = GetComponentInParent<OVRTrackedRemote>();
		parent.m_modelOculusGoController = hand;
		Player player = GetComponentInParent<Player>();
		PlayerHand playerHand = hand.GetComponent<PlayerHand>();
		playerHand.Fader = player.Fader;
		playerHand.IsLevel = player.IsLevel;
		playerHand.IsLeftHand = isLeftHand;
		playerHand.SetInput();
	}
}