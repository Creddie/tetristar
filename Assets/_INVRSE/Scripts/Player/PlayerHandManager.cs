﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandManager : MonoBehaviour
{
	private static PlayerHandManager _Instance;
	public static PlayerHandManager Instance
	{
		get
		{
			if (null == _Instance) _Instance = FindObjectOfType<PlayerHandManager>();
			return _Instance;
		}
	}

	public static bool CanChangeHands = true;
	public static bool IsLeftHanded;

	[SerializeField] private OVRTrackedRemote leftHand;
	[SerializeField] private OVRTrackedRemote rightHand;
	[SerializeField] private GameObject leftGizmo;
	[SerializeField] private GameObject rightGizmo;

	public bool BothHands()
	{
		return ((OVRInput.IsControllerConnected(OVRInput.Controller.LTrackedRemote) && OVRInput.IsControllerConnected(OVRInput.Controller.RTrackedRemote)) ||
			(OVRInput.IsControllerConnected(OVRInput.Controller.LTouch) && OVRInput.IsControllerConnected(OVRInput.Controller.RTouch)));
	}

	private void LateUpdate()
	{
		if (BothHands()) SetHandVisuals();
		if (CanChangeHands) InputHandler();
	}

	private void InputHandler()
	{
		if (BothHands() && 
			(!IsLeftHanded && (OVRInput.GetUp(OVRInput.RawButton.Y) || OVRInput.GetUp(OVRInput.RawButton.X) || OVRInput.GetUp(OVRInput.RawButton.LIndexTrigger))) ||
			(IsLeftHanded && (OVRInput.GetUp(OVRInput.RawButton.A) || OVRInput.GetUp(OVRInput.RawButton.B) || OVRInput.GetUp(OVRInput.RawButton.RIndexTrigger))))
		{
			IsLeftHanded = !IsLeftHanded;
			SetHandVisuals();
		}
	}

	private void SetHandVisuals()
	{
		if (IsLeftHanded)
		{
			leftHand.m_modelOculusGoController.SetActive(true);
			rightGizmo.SetActive(true);
			rightHand.m_modelOculusGoController.SetActive(false);
			leftGizmo.SetActive(false);
		}
		else
		{
			leftHand.m_modelOculusGoController.SetActive(false);
			rightGizmo.SetActive(false);
			rightHand.m_modelOculusGoController.SetActive(true);
			leftGizmo.SetActive(true);
		}
	}
}