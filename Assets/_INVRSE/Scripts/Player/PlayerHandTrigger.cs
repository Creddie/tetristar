﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandTrigger : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		PlayerHandManager.CanChangeHands = true;
	}

	private void OnTriggerExit(Collider other)
	{
		PlayerHandManager.CanChangeHands = false;
	}
}