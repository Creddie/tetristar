﻿using UnityEngine;

public class StaticObjectsHolder : MonoBehaviour
{
	[HideInInspector]
	public GameLevel.SlotInfo[] GridSlots;
	[SerializeField]
	private int SlotW = 0;
	[SerializeField]
	private int SlotH = 0;

	[HideInInspector]
	public Transform GridTransform;
	[HideInInspector]
	public HazardLayer HazardLayer;
	[HideInInspector]
	public Transform BoardMesh;
	[HideInInspector]
	public Transform NullObstacles;

	public void InitGridSlots(int x, int y)
	{
		SlotW = x;
		SlotH = y;
		GridSlots = new GameLevel.SlotInfo[x * y];
	}

	public GameLevel.SlotInfo[,] GetGridSlots()
	{
		GameLevel.SlotInfo[,] slot = new GameLevel.SlotInfo[SlotW, SlotH];
		for (int c = 0; c < SlotW; c++)
			for (int r = 0; r < SlotH; r++)
				slot[c, r] = GridSlots[c + (r * SlotW)];
		return slot;
	}

	public void SetGridSlot(int c, int r, GameLevel.SlotInfo slot)
	{
		GridSlots[c + (r * SlotW)] = slot;
	}
}