﻿using System.IO;
using UnityEditor;

public class OnSceneSave : UnityEditor.AssetModificationProcessor
{
	public static string[] OnWillSaveAssets(string[] paths)
	{
		string sceneName = string.Empty;
		foreach (string path in paths)
		{
			if (path.EndsWith(".unity") && !path.EndsWith("_static.unity"))
				sceneName = Path.GetFileNameWithoutExtension(path);
		}
		if(!LevelList.IsSaving)
			LevelList.SaveLevelStrings();

		if (sceneName.Length == 0)
			return paths;

		LevelBuilder.MakeStale();
		return paths;
	}
}