﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameLevel))]
public class GameLevelInspector : Editor
{
	private const float GRID_CELL_SIZE = 24f;
	public const float MINI_BUTTON_WIDTH = 26f;
	private const float MINI_GRID_BUTTON_WIDTH = 80f;
	private const int LAYER_PADDING = 6;
	private const float LINE_WIDTH = 2f;
	private const int FOLDOUT_SPACING = 14;
	private static GUIStyle HEADER_STYLE
	{
		get
		{
			GUIStyle style = new GUIStyle(EditorStyles.label);
			MakeHeader(style);
			return style;
		}
	}
	private static GUIStyle HEADER_FOLDOUT_STYLE
	{
		get
		{
			GUIStyle style = new GUIStyle(EditorStyles.foldout);
			MakeHeader(style);
			return style;
		}
	}
	public static GUIStyle FOLDOUT_PADDING
	{
		get
		{
			GUIStyle style = new GUIStyle();
			style.margin = new RectOffset(FOLDOUT_SPACING, 0, 0, 0);
			return style;
		}
	}

	private static void MakeHeader(GUIStyle s)
	{
		s.fontStyle = FontStyle.Bold;
		s.fontSize = 12;
	}

	private void OnSceneGUI()
	{
		if (Event.current.type != EventType.Repaint)
			return;

		GameLevel lvl = target as GameLevel;
		if (lvl == null || lvl.Settings == null)
			return;
		Handles.color = lvl.Settings.GridColor;
		Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;
		for (int r = 0; r <= lvl.GridSizeY; r++)
		{
			Vector3 prev = Vector3.zero;
			for (int c = 0; c <= lvl.GridSizeX; c++)
			{
				Vector3 cur = lvl.GetGridPoint(c, r, false, false)[0];
				if (prev != Vector3.zero)
					Handles.DrawAAPolyLine(lvl.Settings.SplineTexture, LINE_WIDTH, prev, cur);
				if (r != lvl.GridSizeY)
					Handles.DrawAAPolyLine(lvl.Settings.SplineTexture, LINE_WIDTH, cur, lvl.GetGridPoint(c, r + 1, false, false)[0]);

				if (r > 0 && c > 0 && lvl.NullTileGrid.Get(c - 1, r - 1))
				{
					Handles.color = lvl.Settings.NullColor;
					Vector3[] rect = new Vector3[4]
					{
						lvl.GetGridPoint(c, r-1, false, false)[0],
						lvl.GetGridPoint(c-1, r-1, false, false)[0],
						prev,
						cur
					};
					Handles.DrawAAConvexPolygon(rect);
					Handles.color = lvl.Settings.GridColor;
				}
				prev = cur;
			}
		}
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		GameLevel lvl = target as GameLevel;
		if (lvl == null || lvl.Settings == null)
			return;

		InspectMeshOverrides(lvl);
		InspectTileSpawnProbability(lvl);
		InspectNullTiles(lvl);
		InspectHazardLayers(lvl);
	}

	private void InspectMeshOverrides(GameLevel lvl)
	{
		EditorGUILayout.Space();
		FieldInfo[] MeshFields = typeof(SharedGameSettings.BoardMeshSettings).GetFields(BindingFlags.Instance | BindingFlags.Public);
		SetMeshArraySize(lvl, MeshFields.Length);
		lvl.ShowMeshOverrideEditor = EditorGUILayout.Foldout(lvl.ShowMeshOverrideEditor, "Grid Mesh Overrides", HEADER_FOLDOUT_STYLE);
		if (lvl.ShowMeshOverrideEditor)
		{
			GUIStyle style = new GUIStyle() { fixedWidth = GRID_CELL_SIZE };
			style.normal.textColor = Color.white;
			object settings = lvl.BoardMeshOverride;
			for (int i = 0; i < MeshFields.Length; i++)
			{
				EditorGUILayout.BeginHorizontal();

				bool isOverriding = lvl.MeshOverrideFields[i];
				EditorGUI.BeginDisabledGroup(!isOverriding);
				FieldInfo field = MeshFields[i];
				EditorGUILayout.LabelField(field.Name);

				string undoText = "Override Mesh Setting: " + field.Name;
				object oldField = field.GetValue(isOverriding ? lvl.BoardMeshOverride : lvl.Settings.DefaultMeshSettings);
				object newField = null;
				if (field.FieldType == typeof(bool))
					newField = EditorGUILayout.Toggle((bool)oldField);
				else if (field.FieldType == typeof(SharedGameSettings.BoardMeshDisplay))
					newField = EditorGUILayout.EnumPopup((SharedGameSettings.BoardMeshDisplay)oldField);
				else if (field.FieldType == typeof(Material))
					newField = EditorGUILayout.ObjectField((Object)oldField, typeof(Material), false);
				else if (field.FieldType == typeof(float))
					newField = EditorGUILayout.FloatField((float)oldField);
				else if (field.FieldType == typeof(int))
					newField = EditorGUILayout.IntField((int)oldField);
				else
				{
					isOverriding = false;
					EditorGUILayout.HelpBox("Please edit GameLevelInspector.InspectMeshOverrides() to support " + field.FieldType.ToString() + " types.", MessageType.Error);
				}

				if (isOverriding && newField != oldField)
				{
					Undo.RecordObject(lvl, undoText);
					field.SetValue(settings, newField);
				}
				EditorGUI.EndDisabledGroup();

				//checkbox
				if (GUILayout.Button(lvl.MeshOverrideFields[i] ? "✔" : "X", style))
				{
					Undo.RecordObject(lvl, "Toggle Override for Mesh Setting: " + field.Name);
					lvl.MeshOverrideFields[i] = !lvl.MeshOverrideFields[i];
				}
				EditorGUILayout.EndHorizontal();
			}
			lvl.BoardMeshOverride = (SharedGameSettings.BoardMeshSettings)settings;
		}
	}

	private void InspectTileSpawnProbability(GameLevel lvl)
	{
		EditorGUILayout.Space();
		SetProbArraySize(lvl);
		lvl.ShowProbabilityEditor = EditorGUILayout.Foldout(lvl.ShowProbabilityEditor, "Tile Probability", HEADER_FOLDOUT_STYLE);
		if (lvl.ShowProbabilityEditor)
		{
			EditorGUILayout.BeginVertical(FOLDOUT_PADDING);

			for (int i = 0; i < lvl.TileProbability.Length; i++)
			{
				if (lvl.Settings.Tiles[i].isSpecial || lvl.Settings.Tiles[i].Name == "UFO_Trail")
					continue;
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField(lvl.Settings.Tiles[i].Name);
				float newProb = EditorGUILayout.Slider(lvl.TileProbability[i], 0, 100);
				if (lvl.TileProbability[i] != newProb)
				{
					Undo.RecordObject(lvl, "Change " + lvl.Settings.Tiles[i].Name + " Tile Probability");
					lvl.TileProbability[i] = newProb;
				}
				EditorGUILayout.EndHorizontal();
			}

			EditorGUILayout.EndVertical();
		}
	}

	private void InspectNullTiles(GameLevel lvl)
	{
		lvl.ShowNullTileGrid = EditorGUILayout.Foldout(lvl.ShowNullTileGrid, "Null Tiles", HEADER_FOLDOUT_STYLE);
		if (lvl.ShowNullTileGrid)
		{
			lvl.NullTileGrid.SetSize(lvl.GridSizeX, lvl.GridSizeY, 1, 1);
			BoolGrid(lvl.NullTileGrid, 1, 1, lvl);
		}
	}

	private void InspectHazardLayers(GameLevel lvl)
	{
		lvl.ShowHazardLayerEditor = EditorGUILayout.Foldout(lvl.ShowHazardLayerEditor, "Hazard Layers", HEADER_FOLDOUT_STYLE);
		if (lvl.ShowHazardLayerEditor)
		{
			EditorGUILayout.BeginVertical(FOLDOUT_PADDING);

			if (lvl.HazardLayers == null)
				lvl.HazardLayers = new HazardLayer.HazardLayerInstance[0];

			int delete = -1;
			int move = -1;
			bool moveUp = false;
			for (int i = 0; i < lvl.HazardLayers.Length; i++)
			{
				EditorGUILayout.BeginHorizontal(new GUIStyle() { margin = new RectOffset(0, 0, LAYER_PADDING, LAYER_PADDING) });
				EditorGUILayout.LabelField("Layer " + (i + 1) + (i == 0 ? " (Top Layer)" : ""), HEADER_STYLE);

				EditorGUI.BeginDisabledGroup(i == 0);
				if (GUILayout.Button("▲", new GUIStyle(EditorStyles.miniButtonLeft) { fixedWidth = MINI_BUTTON_WIDTH }))
				{
					move = i;
					moveUp = true;
				}
				EditorGUI.EndDisabledGroup();

				EditorGUI.BeginDisabledGroup(i == lvl.HazardLayers.Length - 1);
				if (GUILayout.Button("▼", new GUIStyle(EditorStyles.miniButtonMid) { fixedWidth = MINI_BUTTON_WIDTH }))
				{
					move = i;
					moveUp = false;
				}
				EditorGUI.EndDisabledGroup();

				if (GUILayout.Button("X", new GUIStyle(EditorStyles.miniButtonRight) { fixedWidth = MINI_BUTTON_WIDTH }))
					delete = i;
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Template");
				Object obj = EditorGUILayout.ObjectField(lvl.HazardLayers[i].LayerConfigTemplate, typeof(LayerConfig), false);
				if (obj != lvl.HazardLayers[i].LayerConfigTemplate)
				{
					Undo.RecordObject(lvl, "Set HazardLayer Template");
					lvl.HazardLayers[i].LayerConfigTemplate = (LayerConfig)obj;
				}
				EditorGUILayout.EndHorizontal();

				if (lvl.HazardLayers[i].LayerConfigTemplate == null)
				{
					EditorGUILayout.HelpBox("Template is null", MessageType.Warning);
					continue;
				}

				LayerConfig template = lvl.HazardLayers[i].LayerConfigTemplate;

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Tint Type");
				HazardLayer.TintType tt = (HazardLayer.TintType)EditorGUILayout.EnumPopup(lvl.HazardLayers[i].TintType);
				if (tt != lvl.HazardLayers[i].TintType)
				{
					Undo.RecordObject(lvl, "Set HazardLayer Tint Color");
					lvl.HazardLayers[i].TintType = tt;
				}
				EditorGUILayout.EndHorizontal();

				if (lvl.HazardLayers[i].TintType != HazardLayer.TintType.NoTint)
				{
					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField("Tint");
					Color color = EditorGUILayout.ColorField(lvl.HazardLayers[i].TintColor);
					if (color != lvl.HazardLayers[i].TintColor)
					{
						Undo.RecordObject(lvl, "Set HazardLayer Tint Color");
						lvl.HazardLayers[i].TintColor = color;
					}
					EditorGUILayout.EndHorizontal();
				}

				lvl.HazardLayers[i].Grid.SetSize(lvl.GridSizeX, lvl.GridSizeY, template.TileSizeX, template.TileSizeY);
				BoolGrid(lvl.HazardLayers[i].Grid, template.TileSizeX, template.TileSizeY, lvl);
			}

			if (GUILayout.Button("+ Add Layer", new GUIStyle(EditorStyles.miniButton) { fixedWidth = 120f, margin = new RectOffset(0, 0, 20, 20) }))
				AddLayer(lvl);

			EditorGUILayout.EndVertical();
			if (delete != -1)
				DeleteLayer(lvl, delete);
			else if (move != -1)
				MoveLayer(lvl, move, moveUp);
		}
	}

	private void SetProbArraySize(GameLevel lvl)
	{
		int tileCount = lvl.Settings.Tiles.Length;
		if (lvl.TileProbability == null)
		{
			lvl.TileProbability = new float[tileCount];
		}
		else if (lvl.TileProbability.Length != tileCount)
		{
			float[] prev = lvl.TileProbability;
			lvl.TileProbability = new float[tileCount];
			for (int i = 0; i < prev.Length && i < tileCount; i++)
				lvl.TileProbability[i] = prev[i];
		}
	}

	private void SetMeshArraySize(GameLevel lvl, int newSize)
	{
		if (lvl.MeshOverrideFields == null)
		{
			lvl.MeshOverrideFields = new bool[newSize];
		}
		else if (lvl.MeshOverrideFields.Length != newSize)
		{
			bool[] prev = lvl.MeshOverrideFields;
			lvl.MeshOverrideFields = new bool[newSize];
			for (int i = 0; i < prev.Length && i < newSize; i++)
				lvl.MeshOverrideFields[i] = prev[i];
		}
	}

	private void AddLayer(GameLevel lvl)
	{
		Undo.RecordObject(lvl, "Add Hazard Layer");
		HazardLayer.HazardLayerInstance[] old = lvl.HazardLayers;
		lvl.HazardLayers = new HazardLayer.HazardLayerInstance[old.Length + 1];
		for (int i = 0; i < old.Length; i++)
			lvl.HazardLayers[i] = old[i];
		lvl.HazardLayers[lvl.HazardLayers.Length - 1] = new HazardLayer.HazardLayerInstance()
		{
			TintColor = Color.white,
			Grid = new Flat2DArrayBool()
		};
	}

	private void DeleteLayer(GameLevel lvl, int i)
	{
		Undo.RecordObject(lvl, "Remove Hazard Layer");
		HazardLayer.HazardLayerInstance[] old = lvl.HazardLayers;
		lvl.HazardLayers = new HazardLayer.HazardLayerInstance[old.Length - 1];
		for (int j = 0; j < lvl.HazardLayers.Length; j++)
			lvl.HazardLayers[j] = old[j >= i ? j + 1 : j];
	}

	private void MoveLayer(GameLevel lvl, int i, bool up)
	{
		if ((up && i == 0) || (!up && i == lvl.HazardLayers.Length - 1))
			return;
		Undo.RecordObject(lvl, "Move " + (up ? "Up" : "Down") + " Hazard Layer");
		HazardLayer.HazardLayerInstance tmp = lvl.HazardLayers[i];
		int j = up ? i - 1 : i + 1;
		lvl.HazardLayers[i] = lvl.HazardLayers[j];
		lvl.HazardLayers[j] = tmp;
	}

	private void BoolGrid(Flat2DArrayBool grid, int slotSizeX, int slotSizeY, GameLevel lvl)
	{
		RectOffset cellMargin = new RectOffset(-2, -2, -2, -2);
		RectOffset cellPadding = new RectOffset(1, 1, 1, 1);
		grid.Scroll = EditorGUILayout.BeginScrollView(grid.Scroll, GUILayout.Height((GRID_CELL_SIZE + cellMargin.top - 1) * (lvl.GridSizeY + 4) + 6));
		for (int row = -3; row < lvl.GridSizeY; row++)
		{
			EditorGUILayout.BeginHorizontal(new GUIStyle() { padding = new RectOffset(0, 0, 0, 0), margin = cellMargin }, GUILayout.Height(GRID_CELL_SIZE - 4));
			if (row < 0)
			{
				if (row == -3)
				{
					if (GUILayout.Button("All", new GUIStyle(EditorStyles.miniButton) { fixedWidth = GRID_CELL_SIZE * 3 + (cellMargin.left + cellMargin.right) * 0 - 6, margin = new RectOffset(0, 6, 0, 0), padding = cellPadding }))
					{
						Undo.RecordObject(lvl, "Fill Grid");
						grid.SetAll(true);
						SceneView.RepaintAll();
					}
				}
				else if (row == -2)
				{
					if (GUILayout.Button("None", new GUIStyle(EditorStyles.miniButton) { fixedWidth = GRID_CELL_SIZE * 3 + (cellMargin.left + cellMargin.right) * 0 - 6, margin = new RectOffset(0, 6, 0, 0), padding = cellPadding }))
					{
						Undo.RecordObject(lvl, "Clear Grid");
						grid.SetAll(false);
						SceneView.RepaintAll();
					}
				}
				else
				{
					if (slotSizeY != 1 || slotSizeX != 1)
					{
						EditorGUI.BeginDisabledGroup(slotSizeX == 1);
						if (GUILayout.Button("x" + grid.OffSetX, new GUIStyle(EditorStyles.miniButtonLeft) { margin = cellMargin, padding = cellPadding, fixedWidth = GRID_CELL_SIZE }))
						{
							Undo.RecordObject(lvl, "Move Grid Offset");
							grid.OffSetX = (grid.OffSetX + 1) % slotSizeX;
							SceneView.RepaintAll();
						}
						EditorGUI.EndDisabledGroup();
						EditorGUI.BeginDisabledGroup(slotSizeY == 1);
						if (GUILayout.Button("y" + grid.OffSetY, new GUIStyle(EditorStyles.miniButtonRight) { margin = cellMargin, padding = cellPadding, fixedWidth = GRID_CELL_SIZE }))
						{
							Undo.RecordObject(lvl, "Move Grid Offset");
							grid.OffSetY = (grid.OffSetY + 1) % slotSizeY;
							SceneView.RepaintAll();
						}
						EditorGUI.EndDisabledGroup();
						EditorGUILayout.LabelField(" ", new GUIStyle(EditorStyles.miniLabel) { margin = cellMargin, padding = cellPadding }, GUILayout.Width(GRID_CELL_SIZE - 4));
					}
					else
					{
						EditorGUILayout.LabelField(" ", new GUIStyle(EditorStyles.miniLabel) { margin = cellMargin, padding = cellPadding }, GUILayout.Width(GRID_CELL_SIZE * 3 + (cellMargin.left + cellMargin.right) * 0 - 6));
					}
				}
			}
			else
			{
				bool disableRow = grid.OffSetY > row;
				EditorGUI.BeginDisabledGroup(disableRow);
				if (GUILayout.Button("✔", new GUIStyle(EditorStyles.miniButtonLeft) { margin = cellMargin, padding = cellPadding, fixedWidth = GRID_CELL_SIZE }))
				{
					Undo.RecordObject(lvl, "Fill Grid Row");
					grid.SetAllRow(row, true);
					SceneView.RepaintAll();
				}
				if (GUILayout.Button("-", new GUIStyle(EditorStyles.miniButtonRight) { margin = cellMargin, padding = cellPadding, fixedWidth = GRID_CELL_SIZE }))
				{
					Undo.RecordObject(lvl, "Clear Grid Row");
					grid.SetAllRow(row, false);
					SceneView.RepaintAll();
				}
				EditorGUILayout.LabelField("" + (row + 1), new GUIStyle(EditorStyles.miniLabel) { margin = cellMargin, padding = cellPadding, alignment = TextAnchor.UpperCenter }, GUILayout.Width(GRID_CELL_SIZE - 6), GUILayout.Height(GRID_CELL_SIZE - 4));
			}
			for (int col = 0; col < lvl.GridSizeX; col++)
			{
				bool disableRow = grid.OffSetX > col;
				EditorGUI.BeginDisabledGroup(disableRow);
				if (row < -1)
				{
					bool isCheck = row == -3;
					if (GUILayout.Button(isCheck ? "✔" : "-", new GUIStyle(EditorStyles.miniButton) { margin = cellMargin, padding = cellPadding, fixedWidth = GRID_CELL_SIZE }))
					{
						Undo.RecordObject(lvl, (isCheck ? "Fill" : "Clear") + " Grid Column");
						grid.SetAllColumn(col, isCheck);
						SceneView.RepaintAll();
					}
				}
				else if (row == -1)
					EditorGUILayout.LabelField("" + (col + 1), new GUIStyle(EditorStyles.miniLabel) { margin = cellMargin, padding = cellPadding, alignment = TextAnchor.UpperCenter }, GUILayout.Width(GRID_CELL_SIZE - 6), GUILayout.Height(GRID_CELL_SIZE - 4));
				else
				{
					bool val = grid.Get(col, row);
					Color prv = GUI.backgroundColor;
					GUI.backgroundColor = val ? Color.black : Color.white;
					if (GUILayout.Button(val ? "✔" : " ", new GUIStyle(EditorStyles.toolbarButton) { margin = cellMargin, padding = cellPadding, fixedWidth = GRID_CELL_SIZE, fixedHeight = GRID_CELL_SIZE }))
					{
						Undo.RecordObject(lvl, "Configure Grid");
						grid.Set(col, row, !val);
						SceneView.RepaintAll();
					}
					GUI.backgroundColor = prv;
				}
				EditorGUI.EndDisabledGroup();
			}
			if (row >= 0)
				EditorGUI.EndDisabledGroup();
			EditorGUILayout.EndHorizontal();
		}
		EditorGUILayout.EndScrollView();
	}
}