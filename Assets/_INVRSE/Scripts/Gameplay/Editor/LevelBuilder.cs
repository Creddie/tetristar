﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

[InitializeOnLoad]
public class LevelBuilder
{
	private static readonly char SLASH = '/';// System.IO.Path.DirectorySeparatorChar;
	private static readonly string ROOT = "static_root";
	private static readonly string SceneFolderPath = "Assets" + SLASH + "_INVRSE" + SLASH + "_Scenes";
	private static readonly string LevelFolderPath = SceneFolderPath + SLASH + "Levels";
	private static readonly string StoryFolderPath = SceneFolderPath + SLASH + "Stories";
	private static readonly string StaticFolderName = "static";
	private static readonly string StaticFolderPath = LevelFolderPath + SLASH + StaticFolderName;

	private static bool IsStale = true;
	public static void MakeStale() { IsStale = true; }

	private static readonly List<string> LevelsToGenerate = new List<string>();
	private static bool IsBusy = false;
	private static Scene levelScene;
	private static Scene staticScene;
	private static bool BothScenesOpen = false;
	private static GameLevel Level;

	private static string[] OpenScenes;

	static LevelBuilder()
	{
		if (!AssetDatabase.IsValidFolder(LevelFolderPath))
			Debug.LogError("Could not find levels folder: " + LevelFolderPath +
				"\nPlease fix either this script or your Asset folder structure");
		else EditorApplication.update += Update;
	}

	private static void Update()
	{
		if (IsBusy || EditorApplication.isPlaying)
			return;

		if (DoFolderCheck())
		{
			if (HaveScenesChanged()) DoLevelCheck();
			if (Level != null && IsStale) BuildLevel(Level);
		}
	}

	private static bool DoFolderCheck()
	{
		if (!AssetDatabase.IsValidFolder(StaticFolderPath))
		{
			Print("No static folder found. Generating now.");
			AssetDatabase.CreateFolder(LevelFolderPath, StaticFolderName);
			RegenerateAllLevels();
			return false;
		}
		return true;
	}

	private static bool HaveScenesChanged()
	{
		if (OpenScenes == null) return true;
		if (OpenScenes.Length != EditorSceneManager.sceneCount) return true;
		for (int i = 0; i < EditorSceneManager.sceneCount; i++)
			if (OpenScenes[i] != EditorSceneManager.GetSceneAt(i).path)
				return true;
		return false;
	}

	private static void DoLevelCheck()
	{
		BothScenesOpen = false;
		int count = EditorSceneManager.sceneCount;
		OpenScenes = new string[count];
		for (int i = 0; i < count; i++)
		{
			Scene s = EditorSceneManager.GetSceneAt(i);
			OpenScenes[i] = s.path;
			if (IsLevelScene(s.path)) CheckLevel(s, false);
		}
	}

	private static bool IsLevelScene(string path)
	{
		return path.Substring(0, path.LastIndexOf('/')) == LevelFolderPath;
	}

	private static void CheckLevel(Scene lvl, bool forceRefresh)
	{
		string sceneName = lvl.name + "_static";
		string scenePath = StaticFolderPath + SLASH + sceneName + ".unity";
		Level = Object.FindObjectOfType<GameLevel>();

		for (int i = 0; i < EditorSceneManager.sceneCount; i++)
		{
			Scene s = EditorSceneManager.GetSceneAt(i);
			if (s.path == scenePath)
			{
				//is open already
				levelScene = lvl;
				staticScene = s;
				BothScenesOpen = true;

				if (forceRefresh)
					BuildLevel(Level);
				return;
			}
		}

		Scene scene;
		try
		{
			scene = EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Additive);
		}
		catch (System.ArgumentException)
		{
			//scene does not exist
			scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Additive);
			if (!EditorSceneManager.SaveScene(scene, scenePath)) Print("Failed to create static scene: " + scenePath);
			else forceRefresh = true;
		}
		//scene exists and is open
		levelScene = lvl;
		staticScene = scene;
		BothScenesOpen = true;
		if (forceRefresh) BuildLevel(Level);
	}

	[MenuItem("Invrse/Regenerate THIS Level")]
	private static void RegenerateCurrentLevel()
	{
		if (Level != null) BuildLevel(Level);
		else Debug.LogError("[LevelBuilder] This scene is not a level.");
	}

	[MenuItem("Invrse/Regenerate All Levels")]
	private static void RegenerateAllLevels()
	{
		Print("Regenerating levels...");
		IsBusy = true;

		string CurrentScene = EditorSceneManager.GetActiveScene().path;
		bool wasValid = EditorSceneManager.GetActiveScene().IsValid();
		LevelsToGenerate.Clear();
		foreach (string s in AssetDatabase.FindAssets("t:scene", new string[1] { LevelFolderPath }))
		{
			string path = AssetDatabase.GUIDToAssetPath(s);
			if (IsLevelScene(path)) LevelsToGenerate.Add(path);
		}

		string levelpath = "";
		for (int i = 0; i < LevelsToGenerate.Count; i++)
		{
			levelpath = LevelsToGenerate[i];
			EditorUtility.DisplayProgressBar("Generating Static Objects for Levels", levelpath, i / (float)LevelsToGenerate.Count);
			Scene s = EditorSceneManager.OpenScene(levelpath, OpenSceneMode.Single);
			CheckLevel(s, true);
		}
		EditorUtility.DisplayProgressBar("Generating Static Objects for Levels", CurrentScene, 1f);
		if (CurrentScene == "") CurrentScene = levelpath;
		EditorUtility.ClearProgressBar();

		IsBusy = false;
		Print("Done.");
		if(wasValid && CurrentScene != "") EditorSceneManager.OpenScene(CurrentScene, OpenSceneMode.Single);

	}

	[MenuItem("Invrse/Add All Levels To Build")]
	private static void AddLevelsToBuild()
	{
		List<EditorBuildSettingsScene> build = new List<EditorBuildSettingsScene>();
		EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;
		foreach (EditorBuildSettingsScene s in scenes)
			if (!s.path.Contains(LevelFolderPath) && !s.path.Contains(StoryFolderPath))
				build.Add(s);
		foreach (string guid in AssetDatabase.FindAssets("t:scene", new string[2] { LevelFolderPath, StoryFolderPath }))
		{
			string path = AssetDatabase.GUIDToAssetPath(guid);
			EditorBuildSettingsScene s = new EditorBuildSettingsScene(path, true);
			build.Add(s);
		}
		EditorBuildSettings.scenes = build.ToArray();
	}

	private static void Print(string s)
	{
		Debug.Log("[LevelBuilder] " + s);
	}

	private static void BuildLevel(GameLevel lvl)
	{
		if (!BothScenesOpen || lvl == null || lvl.Settings == null) return;

		IsStale = false;
		Print("Rebuilding " + levelScene.name + " :: " + System.DateTime.Now.TimeOfDay.ToString());

		var root = GameObject.Find(ROOT);
		if (root != null) Object.DestroyImmediate(root);
		root = new GameObject(ROOT) { isStatic = true };
		var store = root.AddComponent<StaticObjectsHolder>();
		EditorSceneManager.MoveGameObjectToScene(root, staticScene);

		root.AddComponent<StargazerPivot>();

		BuildGridSlots(lvl, store);
		BuildNullObstacles(lvl, store);
		GameBoardMeshBuilder.Build(lvl, store);

		if (lvl.UsesHazards)
		{
			store.HazardLayer = BuildHazardLayer(lvl, lvl.HazardLayers);
			store.HazardLayer.transform.SetParent(root.transform, false);
		}

		EditorSceneManager.SaveScene(staticScene);
	}

	private static void BuildGridSlots(GameLevel lvl, StaticObjectsHolder root)
	{
		root.GridTransform = new GameObject("Grid").transform;
		root.GridTransform.gameObject.isStatic = true;
		root.GridTransform.SetParent(root.transform, false);
		root.GridTransform.localPosition = Vector3.zero;
		root.GridTransform.SetSiblingIndex(0);
		root.GridTransform.gameObject.isStatic = true;

		root.InitGridSlots(lvl.GridSizeX, lvl.GridSizeY);

		for (int c = 0; c < lvl.GridSizeX; c++)
		{
			for (int r = 0; r < lvl.GridSizeY; r++)
			{
				GameLevel.SlotInfo slot = new GameLevel.SlotInfo();
				Vector3[] info = lvl.GetGridPoint(c, r);

				Transform tile = new GameObject(c + "x" + r).transform;
				tile.gameObject.isStatic = true;
				tile.SetParent(root.GridTransform, false);
				tile.localPosition = info[0];
				tile.localEulerAngles = info[1];
				slot.x = c;
				slot.y = r;
				slot.transform = tile;

				AudioSource a = tile.gameObject.AddComponent<AudioSource>();
				slot.audio = a;
				a.loop = false;
				a.playOnAwake = false;
				a.spatialBlend = 1f;
				a.minDistance = lvl.Settings.MinMaxSoundDistance.x;
				a.maxDistance = lvl.Settings.MinMaxSoundDistance.y;

				if(!lvl.NullTileGrid.Get(c, r))
				{
					if (lvl.Settings.ShowLineRenders)
					{
						LineRenderer l = tile.gameObject.AddComponent<LineRenderer>();
						l.loop = false;
						l.useWorldSpace = false;
						Vector3[] pos = new Vector3[2]
						{
						Vector3.zero,
						new Vector3(0, 0, -lvl.GridRadius)
						};
						l.SetPositions(pos);
						l.startColor = lvl.Settings.LineColorStart;
						l.endColor = lvl.Settings.LineColorEnd;
						l.startWidth = l.endWidth = lvl.Settings.LineWidth;
						l.numCapVertices = 8;
						l.receiveShadows = false;
						l.materials = new Material[1] { lvl.Settings.LineMaterial };
						l.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
					}
					if (lvl.Settings.ShowLineMesh)
					{
						GameObject l = Object.Instantiate(lvl.Settings.LinePrefab);
						l.isStatic = true;
						l.SetActive(false);
						l.name = "Line";
						l.transform.localPosition = new Vector3(0, 0, -lvl.Settings.LineLength);
						l.transform.localScale = new Vector3(lvl.Settings.LineThickness, lvl.Settings.LineLength, lvl.Settings.LineThickness);
						l.transform.SetParent(tile, false);
						slot.line = l;
					}
				}

				root.SetGridSlot(c, r, slot);
			}
		}
	}

	private static void BuildNullObstacles(GameLevel lvl, StaticObjectsHolder root)
	{
		root.NullObstacles = new GameObject("Obstacles").transform;
		root.NullObstacles.gameObject.isStatic = true;
		root.NullObstacles.SetParent(root.transform, false);

		if (lvl.NullObsticlePrefabs == null || lvl.NullObsticlePrefabs.Length == 0)
			return;
		for (int r = 0; r < lvl.GridSizeY; r++)
			for (int c = 0; c < lvl.GridSizeX; c++)
				if(lvl.NullTileGrid.Get(c, r))
				{
					int i = Random.Range(0, lvl.NullObsticlePrefabs.Length);
					Transform ob = Object.Instantiate(lvl.NullObsticlePrefabs[i], root.NullObstacles, false).transform;
					ob.gameObject.isStatic = true;
					ob.rotation = Quaternion.Euler(lvl.GetGridPoint(c, r, true, false)[1]) * Quaternion.AngleAxis(90f, Vector3.right);
					ob.position = ob.up * lvl.PlanetRadius;
					ob.localScale = ob.lossyScale * lvl.Settings.ObstacleScale;
				}
	}

	private static HazardLayer BuildHazardLayer(GameLevel lvl, HazardLayer.HazardLayerInstance[] configs, int i = 0)
	{
		HazardLayer.HazardLayerInstance h = configs[i];

		HazardLayer layer = new GameObject("Hazard Layer #" + (i + 1)) { isStatic = true }.AddComponent<HazardLayer>();
		layer.transform.localScale = Vector3.one * (h.LayerConfigTemplate.ScaleOverride != 0 ? h.LayerConfigTemplate.ScaleOverride : lvl.Settings.HazardLayerScale);
		layer.DisappearVFX = h.LayerConfigTemplate.DisappearVFX;
		layer.HazardsParent = new GameObject("Hazards") { isStatic = true }.transform;
		layer.HazardsParent.SetParent(layer.transform, false);

		Material[] sharedMats = null;

		foreach (HazardLayer.SpawnInfo s in h.Grid)
		{
			bool hasValidCoord = false;
			foreach (Coords c in s.coords)
				if (!lvl.NullTileGrid.Get(c.x, c.y))
					hasValidCoord = true;
			if (!hasValidCoord)
				continue;

			Hazard haz = h.LayerConfigTemplate.InstantiatePrefab(s);
			Vector3[] res = lvl.GetGridPoint(s.avgCoord.x, s.avgCoord.y, true, true, -10f);
			haz.transform.SetParent(layer.HazardsParent, false);
			haz.transform.localPosition = res[0];
			haz.gameObject.isStatic = true;
			for (int j = 0; j < haz.transform.childCount; j++)
				haz.transform.GetChild(j).gameObject.isStatic = true;
			switch (h.LayerConfigTemplate.Align)
			{
				case LayerConfig.AlignType.AlignNormals:
					haz.transform.localEulerAngles += res[1];
					break;
				case LayerConfig.AlignType.RotateYOnly:
					haz.transform.localEulerAngles += new Vector3(0, res[1].y, 0);
					break;
			}
			if (h.TintType == HazardLayer.TintType.TintAllRenderers)
			{
				Renderer[] rs = haz.GetComponentsInChildren<Renderer>(true);
				if (sharedMats == null)
				{
					sharedMats = new Material[rs.Length];
					for (int m = 0; m < sharedMats.Length; m++)
					{
						sharedMats[m] = new Material(rs[m].sharedMaterial);
						sharedMats[m].color = h.TintColor;
					}
				}
				for (int r = 0; r < rs.Length; r++)
					rs[r].sharedMaterial = sharedMats[r];
			}
			else if (h.TintType == HazardLayer.TintType.TintFirstRenderer)
			{
				Renderer r = haz.GetComponentInChildren<Renderer>(true);
				if (sharedMats == null)
					sharedMats = new Material[1] {
						new Material(r.sharedMaterial) { color = h.TintColor }
					};
				r.sharedMaterial = sharedMats[0];
			}
		}

		i++;
		if (i < configs.Length)
		{
			layer.Next = BuildHazardLayer(lvl, configs, i);
			layer.Next.transform.SetParent(layer.transform, false);
		}

		return layer;
	}
}