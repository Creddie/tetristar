﻿using System.Collections.Generic;
using UnityEngine;

public class GameBoardMeshBuilder
{
	public static void Build(GameLevel lvl, StaticObjectsHolder root)
	{
		new GameBoardMeshBuilder(lvl, root);
	}

	private struct Point
	{
		public float x, y;
		public float u, v;
	}
	private struct Square
	{
		public Point TopLeft;
		public Point TopRight;
		public Point BottomLeft;
		public Point BottomRight;
	}

	private enum RenderType { Face, Border, Line };

	private GameLevel lvl;
	private SharedGameSettings.BoardMeshSettings Settings;
	private List<Vector3> verts;
	private List<Vector3> norms;
	private List<Vector2> uv;
	private List<int> trisFaces;
	private List<int> trisBorder;

	private GameBoardMeshBuilder(GameLevel lvl, StaticObjectsHolder root)
	{
		this.lvl = lvl;
		Settings = lvl.BoardMeshSettings;

		Transform board = new GameObject("Board") { isStatic = true }.transform;
		board.SetParent(root.transform, false);
		root.BoardMesh = board;

		if (!Settings.HideAllMesh)
		{
			board.gameObject.AddComponent<MeshFilter>().mesh = BuildMesh();
			MeshRenderer rend = board.gameObject.AddComponent<MeshRenderer>();
			if (Settings.ShowGridFace != SharedGameSettings.BoardMeshDisplay.HIDE)
				rend.materials = new Material[2] { Settings.BoardMatFaces, Settings.BoardMatBorder };
			else
				rend.material = Settings.BoardMatBorder;
		}
		board.gameObject.SetActive(!Settings.HideAllMesh);
	}

	private Mesh BuildMesh()
	{
		verts = new List<Vector3>();
		norms = new List<Vector3>();
		uv = new List<Vector2>();
		trisFaces = new List<int>();
		trisBorder = new List<int>();

		int numDivs = Settings.MeshDivisionsPerTile;
		float BorderWidth = Settings.GridBorderWidth;
		float LineWidth = Settings.ShowGridLines != SharedGameSettings.BoardMeshDisplay.HIDE ? Settings.GridLineWidth : 0;
		for (int x = 0; x < lvl.GridSizeX; x++)
		{
			for (int y = 0; y < lvl.GridSizeY; y++)
			{
				if (!ToShowTile(x, y))
					continue;

				for (int dx = 0; dx < numDivs; dx++)
				{
					for (int dy = 0; dy < numDivs; dy++)
					{
						bool HasLeftBorder = dx == 0 && ((x != 0 && !ToShowTile(x - 1, y)) || (x == 0 && !ToShowTile(lvl.GridSizeX - 1, y)));
						bool HasRightBorder = dx == numDivs - 1 && ((x != lvl.GridSizeX - 1 && !ToShowTile(x + 1, y)) || (x == lvl.GridSizeX - 1 && !ToShowTile(0, y)));
						bool HasTopBorder = dy == 0 && (y == 0 || !ToShowTile(x, y - 1));
						bool HasBottomBorder = dy == numDivs - 1 && (y == lvl.GridSizeY - 1 || !ToShowTile(x, y + 1));

						float dLeft = dx / (float)numDivs;
						float dRight = (dx + 1) / (float)numDivs;
						float dTop = dy / (float)numDivs;
						float dBottom = (dy + 1) / (float)numDivs;

						//borders
						if (HasLeftBorder && HasTopBorder)
							BuildSquare(-BorderWidth, 0, -BorderWidth, 0, x, y, Settings.ShowGridBorder, RenderType.Border);
						if (HasTopBorder)
							BuildSquare(dLeft, dRight, -BorderWidth, 0, x, y, Settings.ShowGridBorder, RenderType.Border);
						if (HasLeftBorder)
							BuildSquare(-BorderWidth, 0, dTop, dBottom, x, y, Settings.ShowGridBorder, RenderType.Border);
						if (HasBottomBorder && HasLeftBorder)
							BuildSquare(-BorderWidth, 0, 1, 1 + BorderWidth, x, y, Settings.ShowGridBorder, RenderType.Border);

						//main square
						BuildSquare(dLeft + (HasLeftBorder ? 0 : LineWidth), dRight - (HasRightBorder ? 0 : LineWidth), dTop + (HasTopBorder ? 0 : LineWidth), dBottom - (HasBottomBorder ? 0 : LineWidth), x, y, Settings.ShowGridFace, RenderType.Face);

						//borders
						if (HasTopBorder && HasRightBorder)
							BuildSquare(1, 1 + BorderWidth, -BorderWidth, 0, x, y, Settings.ShowGridBorder, RenderType.Border);
						if (HasRightBorder)
							BuildSquare(1, 1 + BorderWidth, dTop, dBottom, x, y, Settings.ShowGridBorder, RenderType.Border);
						if (HasBottomBorder)
							BuildSquare(dLeft, dRight, 1, 1 + BorderWidth, x, y, Settings.ShowGridBorder, RenderType.Border);
						if (HasRightBorder && HasBottomBorder)
							BuildSquare(1, 1 + BorderWidth, 1, 1 + BorderWidth, x, y, Settings.ShowGridBorder, RenderType.Border);

						//grid lines
						if (LineWidth > 0)
						{
							if (!HasBottomBorder)
								BuildSquare(dLeft, dRight, 1 - LineWidth, 1 + LineWidth, x, y, Settings.ShowGridLines, RenderType.Line);
							if (!HasRightBorder)
								BuildSquare(1 - LineWidth, 1 + LineWidth, dTop, dBottom, x, y, Settings.ShowGridLines, RenderType.Line);
						}
					}
				}
			}
		}

		Mesh mesh = new Mesh() { name = "Game Board" };
		mesh.vertices = verts.ToArray();
		if (Settings.ShowGridFace != SharedGameSettings.BoardMeshDisplay.HIDE)
		{
			mesh.subMeshCount = 2;
			mesh.SetTriangles(trisFaces.ToArray(), 0);
			mesh.SetTriangles(trisBorder.ToArray(), 1);
		}
		else
		{
			mesh.triangles = trisBorder.ToArray();
		}
		mesh.normals = norms.ToArray();
		mesh.uv = uv.ToArray();

		return mesh;
	}

	private bool ToShowTile(int x, int y)
	{
		bool isNull = lvl.NullTileGrid.Get(x, y);
		bool flip = Settings.FaceCoversNullTilesInstead;
		return (!isNull && !flip) || (isNull && flip);
	}

	private void BuildSquare(float dLeft, float dRight, float dTop, float dBottom, int x, int y, SharedGameSettings.BoardMeshDisplay display, RenderType type)
	{
		if (display == SharedGameSettings.BoardMeshDisplay.HIDE)
			return;
		bool isBorder = type == RenderType.Border || type == RenderType.Line;
		BuildSquare(new Square()
		{
			TopLeft = new Point()
			{
				x = x + dLeft,
				y = y + dTop,
				u = isBorder ? 0 : dLeft,
				v = isBorder ? 0 : dTop
			},
			TopRight = new Point()
			{
				x = x + dRight,
				y = y + dTop,
				u = isBorder ? 1 : dRight,
				v = isBorder ? 0 : dTop
			},
			BottomLeft = new Point()
			{
				x = x + dLeft,
				y = y + dBottom,
				u = isBorder ? 0 : dLeft,
				v = isBorder ? 1 : dBottom
			},
			BottomRight = new Point()
			{
				x = x + dRight,
				y = y + dBottom,
				u = isBorder ? 1 : dRight,
				v = isBorder ? 1 : dBottom
			}
		}, display, type);
	}

	private void BuildSquare(Square square, SharedGameSettings.BoardMeshDisplay display, RenderType type)
	{
		int topLeft = BuildPoint(square.TopLeft);
		int topRight = BuildPoint(square.TopRight);
		int bottomLeft = BuildPoint(square.BottomLeft);
		int bottomRight = BuildPoint(square.BottomRight);

		List<int> tris = type == RenderType.Border || type == RenderType.Line ? trisBorder : trisFaces;

		tris.Add(topLeft);
		tris.Add(topRight);
		tris.Add(bottomRight);

		tris.Add(topLeft);
		tris.Add(bottomRight);
		tris.Add(bottomLeft);

		//back-facing only for border
		if (display == SharedGameSettings.BoardMeshDisplay.SHOW_DOUBLE_SIDED)
		{
			tris.Add(bottomRight);
			tris.Add(topRight);
			tris.Add(topLeft);

			tris.Add(bottomLeft);
			tris.Add(bottomRight);
			tris.Add(topLeft);
		}
	}

	//private int NewPoint(int x, int y, float dx, float dy)
	private int BuildPoint(Point p)
	{
		Vector3[] v = lvl.GetGridPoint(p.x, p.y, false, true, Settings.RadiusOffset);

		verts.Add(v[0]);
		norms.Add(Quaternion.Euler(v[1]) * Vector3.up);
		uv.Add(new Vector2(p.u, p.v));

		return verts.Count - 1;
	}
}