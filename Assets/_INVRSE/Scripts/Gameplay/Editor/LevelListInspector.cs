﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;


[CustomEditor(typeof(LevelList))]
public class LevelListInspector : Editor
{
	private ReorderableList lvlList;
	private ReorderableList storyList;

	private void OnEnable()
	{
		lvlList = new ReorderableList(serializedObject, serializedObject.FindProperty("Levels"), true, true, true, true);
		lvlList.drawHeaderCallback = (Rect rect) =>
		{
			EditorGUI.LabelField(rect, "Level List");
		};
		lvlList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
		{
			var element = lvlList.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			EditorGUI.ObjectField(
				new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
				element, GUIContent.none
			);
		};

		storyList = new ReorderableList(serializedObject, serializedObject.FindProperty("Stories"), true, true, true, true);
		storyList.drawHeaderCallback = (Rect rect) =>
		{
			EditorGUI.LabelField(rect, "Story List");
		};
		storyList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
		{
			var element = storyList.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			EditorGUI.ObjectField(
				new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
				element, GUIContent.none
			);
		};
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		EditorGUILayout.BeginHorizontal();

		EditorGUILayout.BeginVertical();
		lvlList.DoLayoutList();
		EditorGUILayout.EndVertical();

		EditorGUILayout.BeginVertical();
		storyList.DoLayoutList();
		EditorGUILayout.EndVertical();

		EditorGUILayout.EndHorizontal();
		serializedObject.ApplyModifiedProperties();
	}
}