﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTileUFO : GameTile
{
	[HideInInspector] public int TargetType;
	//private int movementDistance = 1;
	private int trailTileType = 26;
	private GameTile target;
	private float moveIntoPlaceTime = 2f;
	private float scaleTime = .5f;
	private float timer;
	private Vector3 originalScale;

	private void Awake()
	{
		originalScale = transform.localScale;
	}

	public override void ActivateSpecial(bool chain, SwipeDirection swipeDir, int type = -1)
	{
		if(!HasActivated)
		{
			HasActivated = true;
			GameLevel.Instance.Spawner.SpawnVFX(Type, transform.parent);
			GameLevel.Instance.PlaySound(X, Y, "Explode");
			ActivateTime = Time.time;
			ActivateLength = 0f;
		}
	}

	#region Look Directions
	private bool TryLeft()
	{
		var newX = X == 0 ? GameLevel.Instance.GridSizeX : X - 1;
		if (!GameLevel.Instance.NullTileGrid.Get(newX, Y) && !GameLevel.Instance.TempNullTileGrid.Get(newX, Y))
		{
			var til = GameLevel.Instance.GameGrid[newX, Y];
			if(!til.MarkedForDespawn && til.TypeOfBomb != BombType.UFO)
			{
				target = til;
				return true;
			}
		}
		return false;
	}
	private bool TryRight()
	{
		var newX = X == GameLevel.Instance.GridSizeX ? 0 : X + 1;
		if (!GameLevel.Instance.NullTileGrid.Get(newX, Y) && !GameLevel.Instance.TempNullTileGrid.Get(newX, Y))
		{
			var til = GameLevel.Instance.GameGrid[newX, Y];
			if (!til.MarkedForDespawn && til.TypeOfBomb != BombType.UFO)
			{
				target = til;
				return true;
			}
		}
		return false;
	}
	private bool TryUp()
	{
		if (Y == 0) return false;
		if (!GameLevel.Instance.NullTileGrid.Get(X, Y - 1) && !GameLevel.Instance.TempNullTileGrid.Get(X, Y - 1))
		{
			var til = GameLevel.Instance.GameGrid[X, Y - 1];
			if (!til.MarkedForDespawn && til.TypeOfBomb != BombType.UFO)
			{
				target = til;
				return true;
			}
		}
		return false;
	}
	private bool TryDown()
	{
		if (Y == GameLevel.Instance.GridSizeY - 1) return false;
		if (!GameLevel.Instance.NullTileGrid.Get(X, Y + 1) && !GameLevel.Instance.TempNullTileGrid.Get(X, Y + 1) && !GameLevel.Instance.GameGrid[X, Y + 1].MarkedForDespawn)
		{
			var til = GameLevel.Instance.GameGrid[X, Y + 1];
			if (!til.MarkedForDespawn && til.TypeOfBomb != BombType.UFO)
			{
				target = til;
				return true;
			}
		}
		return false;
	}
	#endregion

	public void MoveTowardTarget(WaitForAll waiter)
	{
		if (!HasInit) return;
		target = null;
		var tilesAway = int.MaxValue;
		GameTile closest = null;
		foreach (var v in GameLevel.Instance.Logic.GetAllTilesOfType(TargetType))
		{
			var xDist = 0;
			var yDist = 0;
			if (Y > v.Y) yDist = Y - v.Y;
			else if (v.Y > Y) yDist = v.Y - Y;
			if (X > v.X) xDist = X - v.X;
			else if (v.X > X) xDist = v.X - X;
			if(xDist + yDist < tilesAway)
			{
				tilesAway = xDist + yDist;
				closest = v;
			}
		}
		if(null != closest)
		{
			bool takeTarget = false;
			var horiz = 0;
			var vert = 0;
			if (closest.Y > Y) vert = closest.Y - Y;
			else if (Y > closest.Y) vert = -(Y - closest.Y);
			if (closest.X > X) horiz = closest.X - X;
			else if (X > closest.X) horiz = -(X - closest.X);
			//try move up/down
			if (Mathf.Abs(vert) >= Mathf.Abs(horiz))
			{
				//try up
				if (vert > 0)
				{
					if (horiz < 0)
					{
						if (!TryDown() && !TryLeft() && !TryRight() && !TryUp())
							return;
					}
					else if (!TryDown() && !TryRight() && !TryLeft() && !TryUp()) return;
				}
				//try down
				else
				{
					if (horiz < 0)
					{
						if (!TryUp() && !TryLeft() && !TryRight() && !TryDown())
							return;
					}
					else if (!TryUp() && !TryRight() && !TryLeft() && !TryDown()) return;
				}
			}
			//try move left/right
			else
			{
				//left
				if (horiz < 0)
				{
					if (vert > 0)
					{
						if (!TryLeft() && !TryDown() && !TryUp() && !TryRight())
							return;
					}
					else if (!TryLeft() && !TryUp() && !TryDown() && !TryRight()) return;
				}
				//right
				else if(horiz > 0)
				{
					if (vert > 0)
					{
						if (!TryRight() && !TryDown() && !TryUp() && !TryLeft())
							return;
					}
					else if (!TryRight() && !TryUp() && !TryDown() && !TryLeft()) return;
				}
			}
			//target reached
			if ((vert == 0 && Mathf.Abs(horiz) == 1) || (horiz == 0 && Mathf.Abs(vert) == 1))
			{
				target = closest;
				takeTarget = true;
				//PlayerHUD.Instance.UpdateUFOCounter(this);
			}
			SwapTiles(target, waiter);
			GameLevel.Instance.GameGrid[X, Y] = this;
			GameLevel.Instance.GameGrid[target.X, target.Y] = target;
			Coords myPos = new Coords() { x = target.X, y = target.Y };
			Coords otherPos = new Coords() { x = X, y = Y };
			if (Settings.UFOLeavesTrail || takeTarget) LeaveTrail(myPos, otherPos);
		}
	}

	private void LeaveTrail(Coords myPos, Coords otherPos)
	{
		target.MarkedForDespawn = true;
		GameLevel.Instance.Spawner.DespawnTile(target.gameObject, target.Type);
		target.CheckToDisableMoveWaiter();
		var spawned = GameLevel.Instance.SetGridTile(target.X, target.Y, trailTileType);
		if(Settings.UFOLeavesTrail) GameLevel.Instance.TempNullTileGrid.Set(spawned.X, spawned.Y, true);
	}

	public void MoveIntoPlaceOnSpawn(GameTile location)
	{
		target = location;
		StartCoroutine(MovingIntoPlace());
	}

	private IEnumerator MovingIntoPlace()
	{
		transform.Find("SpecialGlowUFO(Clone)").GetChild(0).gameObject.SetActive(false);
		GameLevel.IsInteractive = false;
		timer = 0f;
		transform.localScale = Vector3.zero;
		var pos = transform.position;
		while(timer < scaleTime)
		{
			timer += Time.deltaTime;
			transform.localScale = Vector3.Lerp(Vector3.zero, originalScale, timer / scaleTime);
			transform.position = Vector3.Lerp(pos, target.transform.position, timer / moveIntoPlaceTime);
			yield return new WaitForEndOfFrame();
		}
		transform.localScale = originalScale;
		while(timer < moveIntoPlaceTime)
		{
			timer += Time.deltaTime;
			transform.position = Vector3.Lerp(pos, target.transform.position, timer / moveIntoPlaceTime);
			yield return new WaitForEndOfFrame();
		}
		//transform.position = target.transform.position;
		transform.position = GameLevel.Instance.GetGridPoint(target.X, target.Y)[0];
		GameLevel.Instance.Spawner.DespawnTile(target.gameObject, target.Type);
		var spawned = GameLevel.Instance.SetGridTile(target.X, target.Y, 21);
		//GameLevel.Instance.GameGrid[target.X, target.Y] = spawned;
		GameLevel.IsInteractive = true;
		GameLevel.Instance.Spawner.SpawnVFX(GameLevel.Instance.Spawner.TypeUFOLand, spawned.transform);
		GameLevel.Instance.Spawner.DespawnTile(gameObject, 21);

		var ufo = FindObjectOfType<PlayerHUD_Tutorial_UFO>();
		if (null != ufo) ufo.StartTooltips();
	}

	protected override void OnDisable()
	{
		var ufo = FindObjectOfType<PlayerHUD_Tutorial_UFO>();
		if (null != ufo) ufo.StopTooltips();
		base.OnDisable();
	}
}