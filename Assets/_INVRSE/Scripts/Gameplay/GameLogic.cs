﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogic
{
	#region Variables
	private enum SpawnType { ColumnAndHemisphere, ColumnAndRow }

	public class TileMatch
	{
		public GameTile[] TilesMatched;
		public int Score;
		public bool IsBomb;
		public bool IsSquare;
		public bool WasMerged;
		public bool IsVert;
		public bool IsHoriz;
	}

	private delegate bool MatchCheckerFunction(GameTile[,] board, int col, int row, out TileMatch match, bool checkOnly, bool bothWays);
	private readonly MatchCheckerFunction[] MatchCheckers;
	public static int ConsecutiveFailedMatches;
	public static int ConsecutiveBoardShuffles;

	public GameLevel Level { get; private set; }
	private SharedGameSettings Settings;
	private SharedGameSettings.GameTileInfo[] AllTiles;
	private GameTile swapOriginTile;

	[HideInInspector] public int CascadeComboCount = 1;
	private readonly float ProbSum;
	private int BombChain = 0;
	private int possibleMatchesCount;
	private int cascadeChordStartingPoint;
	[HideInInspector] public bool PlayerTookAction;

	private EventCSVLine evt;
	private MatchCSVLine[] mtc;
	#endregion

	public GameLogic(GameLevel lvl)
	{
		Level = lvl;
		Settings = lvl.Settings;
		AllTiles = Level.Settings.Tiles;
		ConsecutiveFailedMatches = 0;
		ConsecutiveBoardShuffles = 0;

		ProbSum = 0;
		for (int i = 0; i < lvl.TileProbability.Length; i++)
			if (!Settings.Tiles[i].isSpecial)
				ProbSum += lvl.TileProbability[i];

		MatchCheckers = new MatchCheckerFunction[3]
		{
			CheckHorizontalMatch,
			CheckVerticalMatch,
			CheckSquareMatch
		};
		//SaveLoadManager.Init(Settings.LevelList);
		Object.FindObjectOfType<Player>().Fader.gameObject.SetActive(SaveLoadManager.SafetyMode());
	}

	public void InitBoard(bool reshuffle)
	{
		for (int c = 0; c < Level.GridSizeX; c++)
		{
			for (int r = 0; r < Level.GridSizeY; r++)
			{
				if (Level.NullTileGrid.Get(c, r) || Level.TempNullTileGrid.Get(c,r))
					continue;

				bool[] ignore = new bool[Level.Settings.Tiles.Length];
				//check two up
				if (r >= 2)
				{
					int up1 = Level.GetGridTileType(c, r - 1);
					int up2 = Level.GetGridTileType(c, r - 2);
					if (up1 == up2 && up1 != -1)
						ignore[up1] = true;
				}
				//check two left
				if (c >= 2)
				{
					int left1 = Level.GetGridTileType(c - 1, r);
					int left2 = Level.GetGridTileType(c - 2, r);
					if (left1 == left2 && left1 != -1)
						ignore[left1] = true;
					//check wrap-around left and right
					if (c == Level.GridSizeX - 1)
					{
						int right1 = Level.GetGridTileType(0, r);
						if (right1 == left1 && right1 != -1)
							ignore[left1] = true;
						//check wrap-around two right
						else
						{
							int right2 = Level.GetGridTileType(1, r);
							if (right1 == right2 && right1 != -1)
								ignore[right1] = true;
						}
					}
				}
				//check square
				if (r > 0 && c > 0)
				{
					int s1 = Level.GetGridTileType(c - 1, r - 1);
					int s2 = Level.GetGridTileType(c, r - 1);
					if (s1 == s2 && s1 != -1)
					{
						int s3 = Level.GetGridTileType(c - 1, r);
						if (s1 == s3)
							ignore[s1] = true;
					}

					//check square wraparound
					if (c == Level.GridSizeX - 1)
					{
						s1 = Level.GetGridTileType(0, r - 1);
						if (s1 == s2 && s1 != -1)
						{
							int s3 = Level.GetGridTileType(0, r);
							if (s1 == s3)
								ignore[s1] = true;
						}
					}
				}
				Level.SetGridTile(c, r, GetRandomTile(ignore));
				var reveal = false;
				if (reshuffle /*|| null != Object.FindObjectOfType<PlayerHUD_Tutorial_Intro>()*/) reveal = true;
				else if (!StoryManager.IsActive) reveal = false;
				Level.GameGrid[c, r].ShowHideTile(reveal);
			}
		}
		CheckForPossibleMatches(false);
	}

	public int GetRandomTile(bool[] ignore = null)
	{
		float totalProb = ProbSum;
		if (ignore != null)
			for (int i = 0; i < ignore.Length; i++)
				if (ignore[i])
					totalProb -= Level.TileProbability[i];

		float rand = Random.Range(0f, 1f) * totalProb;
		float sum = 0;
		for (int i = 0; i < AllTiles.Length; i++)
		{
			float prob = Level.TileProbability[i];
			if (prob > 0 && (ignore == null || (ignore != null && !ignore[i])) && !Settings.Tiles[i].isSpecial)
			{
				sum += prob;
				if (rand <= sum)
					return i;
			}
		}
		return 0;
	}

	public int GetStandardPoints(int gemsInCombo)
	{
		return gemsInCombo * Settings.PointsPerTile * (1 + CascadeComboCount);
	}

	public void Swipe(GameTile tile, SwipeDirection dir, bool justChecking, bool checkingForHint)
	{
		bool failed = false;
		int x = tile.X;
		int y = tile.Y;
		switch (dir)
		{
			case SwipeDirection.Up:
				failed = (tile.Y == 0);
				y--;
				break;
			case SwipeDirection.Down:
				failed = (tile.Y >= Level.GridSizeY - 1);
				y++;
				break;
			case SwipeDirection.Left:
				x = (x == 0 ? Level.GridSizeX - 1 : x - 1);
				break;
			case SwipeDirection.Right:
				x = (x + 1) % Level.GridSizeX;
				break;
			default:
				failed = true;
				break;
		}

		failed = failed || Level.NullTileGrid.Get(x, y) || Level.TempNullTileGrid.Get(x, y);

		if (!failed)
		{
			swapOriginTile = tile;
			GameTile other = Level.GetGridTile(x, y);
			if (tile.TypeOfBomb == BombType.ColorBomb && !justChecking && other.Type != 10 && other.Type != 0) //no color-bombing collection tiles
			{
				ActivateBomb(tile, false, dir, other.Type);
				PlayerTookAction = true;
				PlayerHUD.Instance.IncreaseMoveCounter();
			}
			else if (tile.CanSwipe && other.CanSwipe)
			{
				GameTile[,] tempLevel = new GameTile[Level.GridSizeX, Level.GridSizeY];
				for (int c = 0; c < Level.GridSizeX; c++)
				{
					for (int r = 0; r < Level.GridSizeY; r++)
					{
						if (c == tile.X && r == tile.Y)
							tempLevel[c, r] = other;
						else if (c == other.X && r == other.Y)
							tempLevel[c, r] = tile;
						else
							tempLevel[c, r] = Level.GameGrid[c, r];
					}
				}

				if (justChecking)
					CheckSomeMatches(tempLevel, tile, other, checkingForHint);
				else if (CheckAllMatches(tempLevel, true))
				{
					//yes, can swipe
					evt = new EventCSVLine(EventCSVLine.EventType.PlayerMove);
					evt.PlayerMoveType = EventCSVLine.MoveType.SwipeAndMatch;

					GameLevel.IsInteractive = false;
					PlayerTookAction = true;
					WaitForAll waiter = new WaitForAll(ProcessAllMatches);
					tile.SwapTiles(other, waiter);
					Level.GameGrid[tile.X, tile.Y] = tile;
					Level.GameGrid[other.X, other.Y] = other;
					PlayerHUD.Instance.IncreaseMoveCounter();
					Level.PlaySound(tile.X, tile.Y, "Swipe");
					return;
				}
			}
		}

		//no, can't swipe
		if (!justChecking && !checkingForHint)
		{
			tile.FailedSwapAttempt();
			ConsecutiveFailedMatches += 1;
			if (ConsecutiveFailedMatches >= Settings.HelpMatchThreshold)
			{
				DynamicTooltipManager.Instance.TryShowTooltip(BombType.NOT_A_BOMB, true, true);
				PlayerHUD.TooltipIsActive = true;
			}
		}
	}

	public void ResetMatchHelpTooltip()
	{
		if (ConsecutiveFailedMatches >= Settings.HelpMatchThreshold)
		{
			DynamicTooltipManager.Instance.TryShowTooltip(BombType.NOT_A_BOMB, false, true);
			PlayerHUD.TooltipIsActive = false;
		}
		ConsecutiveFailedMatches = 0;
	}

	private void MatchHint(TileMatch match)
	{
		//TODO: hint sfx/vfx
		foreach (GameTile gt in match.TilesMatched) gt.ShakeTile();
	}

	#region Special Gem Logic
	public List<GameTile> GetNearbyTiles(GameTile tile, int dist = 1, bool crossOnly = false)
	{
		List<GameTile> neighbors = new List<GameTile>();
		int leftStart;
		if (tile.X < dist)
			leftStart = Level.GridSizeX - dist + tile.X;
		else
			leftStart = tile.X - dist;

		for (int steps = 0, c = leftStart; steps < 2 * dist + 1; c = (c + 1) % Level.GridSizeX, steps++)
		{
			for (int r = Mathf.Max(0, tile.Y - dist); r <= Mathf.Min(Level.GridSizeY - 1, tile.Y + dist); r++)
			{
				if (crossOnly && c != tile.X && r != tile.Y)
					continue;
				GameTile neighbor = Level.GameGrid[c, r];
				if (neighbor != null)
					neighbors.Add(neighbor);
			}
		}
		return neighbors;
	}

	private List<GameTile> GetCannonBlast(int col, int row, SwipeDirection dir, int range)
	{
		if(dir == SwipeDirection.Undefined)
		{
			var rando = Random.Range(0, 4);
			switch(rando)
			{
				case 0: dir = SwipeDirection.Down; break;
				case 1: dir = SwipeDirection.Left; break;
				case 2: dir = SwipeDirection.Up; break;
				case 3: dir = SwipeDirection.Right; break;
			}
		}
		//const int range = 4 + 1;//the +1 is counting the bomb itself
		int increment = dir == SwipeDirection.Right || dir == SwipeDirection.Down ? 1 : -1;
		List<GameTile> tiles = new List<GameTile>();
		if (dir == SwipeDirection.Right || dir == SwipeDirection.Left)
			for (int steps = 0, c = col; steps < range; c = (c == 0 && increment == -1 ? Level.GridSizeX - 1 : (c + increment) % Level.GridSizeX), steps++)
			{
				GameTile t = Level.GameGrid[c, row];
				if (Level.Settings.CannonIsBlockedByNullTiles && Level.NullTileGrid.Get(c, row))
					break;
				if (t != null)
					tiles.Add(t);
			}
		else
			for (int steps = 0, r = row; steps < range && r >= 0 && r < Level.GridSizeY; r += increment, steps++)
			{
				GameTile t = Level.GameGrid[col, r];
				if (Level.Settings.CannonIsBlockedByNullTiles && Level.NullTileGrid.Get(col, r))
					break;
				if (t != null)
					tiles.Add(t);
			}
		return tiles;
	}

	public List<GameTile> GetConeBlast(int col, int row, SwipeDirection dir, int dist = 3)
	{
		int increment = dir == SwipeDirection.Right || dir == SwipeDirection.Down ? 1 : -1;
		List<GameTile> tiles = new List<GameTile>();
		//right/left
		if (dir == SwipeDirection.Right || dir == SwipeDirection.Left)
		{
			for (int steps = 0, c = col; steps <= dist; c = (c == 0 && increment == -1 ? Level.GridSizeX - 1 : (c + increment) % Level.GridSizeX), steps++)
			{
				if (Level.NullTileGrid.Get(c, row))
				{
					if (Level.Settings.CannonIsBlockedByNullTiles) break;
					else continue;
				}
				var t = Level.GameGrid[c, row];
				if (null != t) tiles.Add(t);
				for(int i = 1; i < steps; i++)
				{
					//+
					if(row + i < Level.GridSizeY)
					{
						if (Level.NullTileGrid.Get(c, row + i))
						{
							if (Level.Settings.CannonIsBlockedByNullTiles) break;
							else continue;
						}
						t = Level.GameGrid[c, row + i];
						if (null != t) tiles.Add(t);
					}
					//-
					if(row - i >= 0)
					{
						if (Level.NullTileGrid.Get(c, row - i))
						{
							if (Level.Settings.CannonIsBlockedByNullTiles) break;
							else continue;
						}
						t = Level.GameGrid[c, row - i];
						if (null != t)
							tiles.Add(t);
					}
				}
			}
		}
		//up/down
		else
		{
			for (int steps = 0, r = row; steps <= dist && r >= 0 && r < Level.GridSizeY; r += increment, steps++)
			{
				if(Level.NullTileGrid.Get(col, r))
				{
					if (Level.Settings.CannonIsBlockedByNullTiles) break;
					else continue;
				}
				var t = Level.GameGrid[col, r];
				if (null != t) tiles.Add(t);
				for (int i = 0; i < steps; i++)
				{
					//+
					var v = col + i;
					if(v >= Level.GridSizeX) v -= Level.GridSizeX;
					if (Level.NullTileGrid.Get(v, r))
					{
						if (Level.Settings.CannonIsBlockedByNullTiles) break;
						else continue;
					}
					t = Level.GameGrid[v, r];
					if (null != t) tiles.Add(t);
					//-
					v = col - i;
					if (v < 0) v = Level.GridSizeX + (col - i);
					if (Level.NullTileGrid.Get(v, r))
					{
						if (Level.Settings.CannonIsBlockedByNullTiles) break;
						else continue;
					}
					t = Level.GameGrid[v, r];
					if (null != t) tiles.Add(t);
				}
			}
		}
		return tiles;
	}

	public List<GameTile> GetAllTilesInRow(int row)
	{
		List<GameTile> tiles = new List<GameTile>();
		for (int i = 0; i < Level.GridSizeX; i++)
		{
			GameTile t = Level.GameGrid[i, row];
			if (t != null)
				tiles.Add(t);
		}
		return tiles;
	}

	public List<GameTile> GetAllTilesInColumn(int col)
	{
		List<GameTile> tiles = new List<GameTile>();
		for (int i = 0; i < Level.GridSizeY; i++)
		{
			GameTile t = Level.GameGrid[col, i];
			if (t != null)
				tiles.Add(t);
		}
		return tiles;
	}

	public List<GameTile> GetVerticalRing(int col)
	{
		int otherCol = (col + (Level.GridSizeX / 2)) % Level.GridSizeX;
		List<GameTile> tiles = GetAllTilesInColumn(col);
		tiles.AddRange(GetAllTilesInColumn(otherCol));
		return tiles;
	}

	public List<GameTile> GetAllTilesOfType(int type)
	{
		List<GameTile> tiles = new List<GameTile>();
		foreach (GameTile t in Level.GameGrid)
			if (t != null && t.Type == type)
				tiles.Add(t);
		return tiles;
	}

	public void ActivateBomb(GameTile tile, bool combo, SwipeDirection swipeDir, int type = -1, bool playerInitiated = false)
	{
		if(playerInitiated)
		{
			evt = new EventCSVLine(EventCSVLine.EventType.PlayerMove);
			evt.PlayerMoveType = EventCSVLine.MoveType.ActivateSpecial;
		}
		else
		{
			evt = new EventCSVLine(EventCSVLine.EventType.ChainSpecial);
			evt.ChainDepthCount = BombChain.ToString();
			evt.ChainStartEventID = TelemetryManager.LastMoveID;
			evt.PlayerMoveType = EventCSVLine.MoveType.Chain;
		}
		BombChain++;
		GameLevel.IsInteractive = false;
		tile.ActivateSpecial(combo, swipeDir, type);
	}

	public virtual void DetonateBomb(GameTile bomb, SwipeDirection dir, int type = -1)
	{
		if (!bomb.IsBomb)
		{
			Debug.LogError("tile is not bomb");
			return;
		}
		var tilesFound = new List<GameTile>();
		TileMatch m = new TileMatch
		{
			IsBomb = true
		};
		//get tiles in range
		switch (bomb.TypeOfBomb)
		{
			case BombType.Neighbors3x3:
				tilesFound = GetNearbyTiles(bomb);
				break;
			case BombType.Neighbors5x5:
				tilesFound = GetNearbyTiles(bomb, 2);
				break;
			case BombType.Cross5x5:
				tilesFound = GetNearbyTiles(bomb, 2, true);
				break;
			case BombType.Cannon:
				tilesFound = GetCannonBlast(bomb.X, bomb.Y, dir, 5);
				break;
			case BombType.HalfCross:
				if (dir == SwipeDirection.Left || dir == SwipeDirection.Right) tilesFound = GetAllTilesInRow(bomb.Y);
				else tilesFound = GetVerticalRing(bomb.X);
				break;
			case BombType.FullCross:
				tilesFound = GetAllTilesInRow(bomb.Y);
				tilesFound.AddRange(GetVerticalRing(bomb.X));
				break;
			case BombType.ColorBomb:
				if (type == -1 || Settings.Tiles[type].isSpecial || Settings.Tiles[type].BombType != BombType.NOT_A_BOMB)
				{
					bomb.FailedSwapAttempt();
					return;
				}
				tilesFound = GetAllTilesOfType(type);
				tilesFound.Add(bomb);
				break;
			case BombType.BlasterAuto:
				StartBlasting(bomb);
				return;
			case BombType.ShuffleBoard:
				Level.Spawner.SpawnScore(GetStandardPoints(1), bomb.transform.parent);
				ShuffleBoard(true);
				return;
			case BombType.ConeBomb3x:
				tilesFound = GetConeBlast(bomb.X, bomb.Y, dir, 3);
				tilesFound.Add(bomb);
				break;
			case BombType.UFO:
				tilesFound.Add(bomb);
				m.TilesMatched = new GameTile[1] { bomb };
				break;
			default:
				Debug.LogError("Bomb Type " + bomb.TypeOfBomb + " isn't defined in GameLogic yet.");
				return;
		}

		//process bombs in range
		var tilesToBlow = new List<GameTile>();
		tilesToBlow.AddRange(tilesFound);
		foreach (GameTile t in tilesFound)
		{
			if (t.IsBomb && !t.HasActivated)
			{
				if (t.TypeOfBomb == BombType.BlasterAuto || t.TypeOfBomb == BombType.ShuffleBoard)
				{
					tilesToBlow.Remove(t);
					continue;
				}
				BombHitOtherBomb(bomb, t);
			}
		}

		m.TilesMatched = tilesToBlow.ToArray();
		m.Score = GetStandardPoints(m.TilesMatched.Length);
		Level.SpawnExplosionEffects(m.TilesMatched);
		Level.StartCoroutine(OnExplosion(m));
		TelemetryManager.LogEvent(evt, new MatchCSVLine[1] 
		{
			new MatchCSVLine(evt.EventID, "Bomb_" + bomb.TypeOfBomb.ToString(), tilesFound.Count, m.Score)
		});
		//TODO: ripple anims on gems near those destroyed
	}

	private void StartBlasting(GameTile bomb)
	{
		//TODO: added delay/warmup before actually blasting
		BlasterManager.Instance.IsBlasting = true;
		BlasterManager.Instance.LaunchRay();
		BlasterManager.Instance.BlasterBeam.Rays[0].arc.playbackType = ArcReactor_Arc.ArcsPlaybackType.loop;
		Level.KillTile(bomb);
	}

	private void BombHitOtherBomb(GameTile first, GameTile second)
	{
		SwipeDirection direc = SwipeDirection.Undefined;
		switch (second.TypeOfBomb)
		{
			case BombType.HalfCross:
				if (first.TypeOfBomb == BombType.HalfCross || first.TypeOfBomb == BombType.FullCross)
					second.TypeOfBomb = BombType.FullCross;
				else
					direc = Random.Range(0, 2) == 0 ? SwipeDirection.Right : SwipeDirection.Up;
				break;

			case BombType.Cannon:
				int midRow = GetFirstRowOfBottomHalf();
				bool coinToss = Random.Range(0, 2) == 0;
				if (first.TypeOfBomb == BombType.Neighbors3x3 || first.TypeOfBomb == BombType.Neighbors5x5 || first.TypeOfBomb == BombType.Cross5x5)
				{
					if (second.Y >= midRow && second.Y < first.Y && coinToss)
						direc = SwipeDirection.Up;
					else if (second.Y < midRow && second.Y > first.Y && coinToss)
						direc = SwipeDirection.Down;
					else
					{
						SwipeDirection comp = CompareHorizontalPosition(second, first);
						if (comp == SwipeDirection.Right)
							direc = SwipeDirection.Left;
						else if (comp == SwipeDirection.Left)
							direc = SwipeDirection.Right;
						else
							direc = coinToss ? SwipeDirection.Right : SwipeDirection.Left;
					}
				}
				else if (first.TypeOfBomb == BombType.HalfCross || first.TypeOfBomb == BombType.FullCross)
				{
					if (second.Y == first.Y)
						direc = coinToss ? SwipeDirection.Left : SwipeDirection.Right;
					else if (second.Y < midRow)
						direc = SwipeDirection.Down;
					else
						direc = SwipeDirection.Up;
				}
				else if (first.TypeOfBomb == BombType.Cannon)
				{
					if (second.Y == first.Y)
						direc = second.Y >= midRow ? SwipeDirection.Up : SwipeDirection.Down;
					else
						direc = coinToss ? SwipeDirection.Left : SwipeDirection.Right;
				}
				else
					direc = (SwipeDirection)Random.Range(1, 5);
				break;

			case BombType.ColorBomb:
				ActivateBomb(second, true, direc, GetRandomTile());
				return;
		}
		ActivateBomb(second, true, direc);
	}

	/**
	 * Avoids wraparound bugs for when you want to know if a tile is to
	 * the right or left of another tile
	 * 
	 * returns either Right, Left, or Undefined (if equal)
	 */
	private SwipeDirection CompareHorizontalPosition(GameTile baseTile, GameTile otherTile)
	{
		if (baseTile.X == otherTile.X)
			return SwipeDirection.Undefined;
		float rightDistance;
		if (baseTile.X > otherTile.X)
			rightDistance = Level.GridSizeX - baseTile.X + otherTile.X;
		else
			rightDistance = otherTile.X - baseTile.X;

		float leftDistance;
		if (baseTile.X > otherTile.X)
			leftDistance = baseTile.X - otherTile.X;
		else
			leftDistance = baseTile.X + (Level.GridSizeX - otherTile.X);

		return rightDistance < leftDistance ? SwipeDirection.Right : SwipeDirection.Left;
	}

	private IEnumerator OnExplosion(TileMatch m)
	{
		yield return new WaitForSecondsRealtime(Settings.BombExplosionLength);
		//TODO: secondary bomb sounds
		DeactivateTiles(new List<TileMatch>() { m });
	}
	#endregion

	#region Check Match Logic
	private void ProcessAllMatches()
	{
		CheckAllMatches(Level.GameGrid, false);
		TelemetryManager.LogEvent(evt, mtc);
	}

	public bool CheckForPossibleMatches(GameTile tile)
	{
		possibleMatchesCount = 0;
		Swipe(tile, SwipeDirection.Down, true, false);
		Swipe(tile, SwipeDirection.Up, true, false);
		Swipe(tile, SwipeDirection.Left, true, false);
		Swipe(tile, SwipeDirection.Right, true, false);
		if (0 == possibleMatchesCount) return false;
		else return true;
	}

	public void CheckForPossibleMatches(bool checkingForHint)
	{
		Level.HintTimer = 0f;
		if (BlasterManager.Instance.IsBlasting)
			return;
		possibleMatchesCount = 0;
		var specials = 0;
		for (int c = 0; c < Level.GridSizeX; c++)
		{
			for (int r = 0; r < Level.GridSizeY; r++)
			{
				if (Level.NullTileGrid.Get(c, r) || Level.TempNullTileGrid.Get(c, r))
					continue;
				var gt = Level.GameGrid[c, r];
				if (null == gt)
					continue;
				if (gt.IsBomb)
				{
					if (checkingForHint)
						return;
					specials += 1;
					continue;
				}
				Swipe(gt, SwipeDirection.Down, true, checkingForHint);
				Swipe(gt, SwipeDirection.Up, true, checkingForHint);
				Swipe(gt, SwipeDirection.Left, true, checkingForHint);
				Swipe(gt, SwipeDirection.Right, true, checkingForHint);
				if (checkingForHint && possibleMatchesCount > 0)
					return;
			}
		}

		if (!checkingForHint)
		{
			possibleMatchesCount += specials;
			PlayerHUD.Instance.PossibleMovesText.SetText(possibleMatchesCount.ToString());
			if (0 == possibleMatchesCount)
			{
				ConsecutiveBoardShuffles += 1;
				if (ConsecutiveBoardShuffles >= 10) Level.LevelFailure();
				else ShuffleBoard(false);
			}
			else ConsecutiveBoardShuffles = 0;
		}
	}

	private void ShuffleBoard(bool allowMatches)
	{
		TelemetryManager.LogEvent(new EventCSVLine(EventCSVLine.EventType.ShuffleBoard));
		//TODO: fx + smooth transition, instead of just instant switch
		for (int c = 0; c < Level.GridSizeX; c++)
		{
			for (int r = 0; r < Level.GridSizeY; r++)
			{
				if (Level.NullTileGrid.Get(c, r) || (Level.GetGridTile(c, r).IsBomb && !Level.GetGridTile(c, r).HasActivated) || Level.TempNullTileGrid.Get(c, r))
					continue;
				var til = Level.GetGridTile(c, r);
				Level.GameGrid[c, r] = null;
				Level.Spawner.DespawnTile(til.gameObject, til.Type);
			}
		}
		if(allowMatches)
		{
			for (int c = 0; c < Level.GridSizeX; c++)
			{
				for (int r = 0; r < Level.GridSizeY; r++)
				{
					if (Level.NullTileGrid.Get(c, r) || (null != Level.GameGrid[c, r] && Level.GetGridTile(c, r).IsBomb) || Level.TempNullTileGrid.Get(c, r))
						continue;
					Level.SetGridTile(c, r, GetRandomTile());
				}
			}
			CheckAllMatches(Level.GameGrid, false);
		}
		else InitBoard(true);
	}

	private void CheckSomeMatches(GameTile[,] board, GameTile tile, GameTile other, bool checkingForHint)
	{
		var doneHint = false;
		TileMatch match;
		var cycles = 0;
		GameTile til = tile;
		while (cycles < 2)
		{
			foreach (MatchCheckerFunction check in MatchCheckers)
			{
				if (check(board, til.X, til.Y, out match, true, true))
				{
					possibleMatchesCount += 1;
					if (checkingForHint && !doneHint)
					{
						doneHint = true;
						MatchHint(match);
					}
				}
			}
			til = other;
			cycles += 1;
		}
	}

	private bool CheckAllMatches(GameTile[,] board, bool checkOnly)
	{
		List<TileMatch> matches = null;
		if (!checkOnly)
			matches = new List<TileMatch>();
		for (int col = 0; col < Level.GridSizeX; col++)
		{
			for (int row = 0; row < Level.GridSizeY; row++)
			{
				if (Level.NullTileGrid.Get(col, row) || null == board[col,row] || !board[col, row].CanSwipe || board[col, row].IsBomb || Level.TempNullTileGrid.Get(col, row))
					continue;

				TileMatch match;
				foreach (MatchCheckerFunction check in MatchCheckers)
				{
					if (check(board, col, row, out match, checkOnly, false))
						if (checkOnly)
							return true;
						else
							matches.Add(match);
				}
			}
		}

#if UNITY_EDITOR
		//PrintMatches(matches);
#endif
		if (checkOnly || matches.Count == 0) return false;
		else
		{
			mtc = new MatchCSVLine[matches.Count];
			for (int i = 0; i < mtc.Length; i++)
			{
				var matchLength = matches[i].TilesMatched.Length;
				mtc[i] = new MatchCSVLine(evt.EventID, Settings.Tiles[matches[i].TilesMatched[0].Type].Name, matchLength, GetStandardPoints(matchLength));
			}
			DeactivateTiles(matches);
			return true;
		}
	}

#if UNITY_EDITOR
	private void PrintMatches(List<TileMatch> matches)
	{
		if (matches.Count > 0)
		{
			string result = "matches found: " + matches.Count + "\n";
			foreach (TileMatch m in matches)
			{
				result += m.Score + ": {\n";
				foreach (GameTile t in m.TilesMatched)
					result += "	" + t.Type + ": [" + t.X + "," + t.Y + "]\n";
				result += "}\n";
			}
			Debug.Log(result);
		}
	}
#endif

	private bool CheckHorizontalMatch(GameTile[,] board, int col, int row, out TileMatch match, bool checkOnly, bool bothWays)
	{
		match = new TileMatch();
		int tile = board[col, row].Type;
		//var b = (col == 0 && (Level.NullTileGrid.Get(Level.GridSizeX - 1, row) || Level.TempNullTileGrid.Get(Level.GridSizeX - 1, row) || (null != board[Level.GridSizeX - 1, row] && tile != board[Level.GridSizeX - 1, row].Type))) || 
			//(col > 0 && (Level.NullTileGrid.Get(col - 1, row) || Level.TempNullTileGrid.Get(col - 1, row) || (null != board[col - 1, row] && tile != board[col - 1, row].Type)));
		//if (bothWays || b)
		//{
			var tilesFound = new List<GameTile> { board[col, row] };
			//walk right until we either (1) find a non-matching tile, or (2) loop back to where we started
			for (int i = (col + 1) % Level.GridSizeX; i != col; i = (i + 1) % Level.GridSizeX)
			{
				GameTile next = board[i, row];
				if (!Level.NullTileGrid.Get(i, row) && null != next && next.Type == tile)
					tilesFound.Add(next);
				else break;
			}
			//walk left
			if(bothWays)
			{
				for(int i = 1; i < Level.GridSizeX; i++)
				{
					var step = col - i;
					if (step < 0) step += Level.GridSizeX;
					GameTile next = board[step, row];
					if (!Level.NullTileGrid.Get(step, row) && null != next && next.Type == tile)
						tilesFound.Add(next);
					else break;
				}
			}
			//match found
			if (tilesFound.Count >= 3)
			{
				match.IsHoriz = true;
				match.TilesMatched = tilesFound.ToArray();
				match.Score = GetStandardPoints(tilesFound.Count);
				if (!checkOnly)
					match = MergeMatch(match);
				return true;
			}
		//}
		return false;
	}

	private bool CheckVerticalMatch(GameTile[,] board, int col, int row, out TileMatch match, bool checkOnly, bool bothWays)
	{
		match = new TileMatch();
		int tile = board[col, row].Type;
		var b = (row == 0 || (row > 0 && row < Level.GridSizeY - 2));// && null != board[col, row - 1] && (Level.NullTileGrid.Get(col, row - 1) || Level.TempNullTileGrid.Get(col, row - 1) || tile != board[col, row - 1].Type)));
		if (bothWays || b)
		{
			var tilesFound = new List<GameTile> { board[col, row] };
			//walk down until we either (1) find a non-matching tile, or (2) run out of grid
			int i = row + 1;
			while (i < Level.GridSizeY)
			{
				GameTile next = board[col, i];
				if (!Level.NullTileGrid.Get(col, i) && null != next && next.Type == tile)
					tilesFound.Add(next);
				else break;
				i++;
			}
			//walk up
			if(bothWays && row > 0)
			{
				i = row - 1;
				while(i >= 0)
				{
					GameTile next = board[col, i];
					if (!Level.NullTileGrid.Get(col, i) && null != next && next.Type == tile)
						tilesFound.Add(next);
					else break;
					i--;
				}
			}
			//match found
			if (tilesFound.Count >= 3)
			{
				match.IsVert = true;
				match.TilesMatched = tilesFound.ToArray();
				match.Score = GetStandardPoints(tilesFound.Count);
				if (!checkOnly)
					match = MergeMatch(match);
				return true;
			}
		}
		return false;
	}

	private bool CheckSquareMatch(GameTile[,] board, int col, int row, out TileMatch match, bool checkOnly, bool allWays)
	{
		match = new TileMatch();
		int tile = board[col, row].Type;
		var tilesFound = new List<GameTile> { board[col, row] };
		if (allWays)
		{
			var horiz = 0;
			var vert = 0;
			for(int j = 0; j < 4; j++)
			{
				switch(j)
				{
					//right up
					case 0:
						if(row > 0)
						{
							horiz = col + 1 > Level.GridSizeX - 1 ? 0 : col + 1;
							vert = row - 1;
						}
						else continue;
						break;
					//right down
					case 1:
						if (row < Level.GridSizeY - 1)
						{
							horiz = col + 1 > Level.GridSizeX - 1 ? 0 : col + 1;
							vert = row + 1;
						}
						else continue;
						break;
					//left up
					case 2:
						if(row > 0)
						{
							horiz = col - 1 < 0 ? Level.GridSizeX - 1 : col - 1;
							vert = row - 1;
						}
						else continue;
						break;
					//left down
					case 3:
						if (row < Level.GridSizeY - 1)
						{
							horiz = col - 1 < 0 ? Level.GridSizeX - 1 : col - 1;
							vert = row + 1;
						}
						else continue;
						break;
				}
				//check three corners
				for (int i = 0; i < 3; i++)
				{
					GameTile other;
					bool isNull;
					switch (i)
					{
						case 0: other = board[horiz, row]; isNull = Level.NullTileGrid.Get(horiz, row); break;
						case 1: other = board[col, vert]; isNull = Level.NullTileGrid.Get(col, vert); break;
						default: other = board[horiz, vert]; isNull = Level.NullTileGrid.Get(horiz, vert); break;
					}
					if (!isNull && null != other && other.Type == tile)
						tilesFound.Add(other);
				}
				//match found
				if (tilesFound.Count == 4)
				{
					match.TilesMatched = tilesFound.ToArray();
					match.Score = GetStandardPoints(tilesFound.Count);
					match.IsSquare = true;
					if (!checkOnly)
						match = MergeMatch(match);
					return true;
				}
				else tilesFound.Clear();
			}
		}
		else if (row < Level.GridSizeY - 1)
		{
			int right = (col + 1) % Level.GridSizeX;
			int down = row + 1;

			//check three corners
			for (int i = 0; i < 3; i++)
			{
				GameTile other;
				bool isNull;
				switch (i)
				{
					case 0: other = board[right, row]; isNull = Level.NullTileGrid.Get(right, row); break;
					case 1: other = board[col, down]; isNull = Level.NullTileGrid.Get(col, down); break;
					default: other = board[right, down]; isNull = Level.NullTileGrid.Get(right, down); break;
				}
				if (!isNull && null != other && other.Type == tile)
					tilesFound.Add(other);
			}
			//match found
			if (tilesFound.Count == 4)
			{
				match.TilesMatched = tilesFound.ToArray();
				match.Score = GetStandardPoints(tilesFound.Count);
				match.IsSquare = true;
				if (!checkOnly)
					match = MergeMatch(match);
				return true;
			}
		}
		return false;
	}

	private TileMatch MergeMatch(TileMatch match)
	{
		List<TileMatch> otherMatches = null;
		foreach (GameTile tile in match.TilesMatched)
		{
			if (tile.MyMatch == null)
				tile.MyMatch = match;
			else
			{
				if (otherMatches == null)
					otherMatches = new List<TileMatch>();
				if (!otherMatches.Contains(tile.MyMatch))
					otherMatches.Add(tile.MyMatch);
			}
		}
		if (otherMatches == null)
			return match;

		otherMatches.Add(match);
		TileMatch merged = new TileMatch();
		List<GameTile> tiles = new List<GameTile>();
		foreach (TileMatch m in otherMatches)
		{
			m.WasMerged = true;
			if (m.IsSquare)
				merged.IsSquare = true;
			if (m.IsVert)
				merged.IsVert = true;
			if (m.IsHoriz)
				merged.IsHoriz = true;
			foreach (GameTile t in m.TilesMatched)
			{
				if (!tiles.Contains(t))
				{
					tiles.Add(t);
					t.MyMatch = merged;
				}
			}
		}
		merged.TilesMatched = tiles.ToArray();
		merged.Score = GetStandardPoints(tiles.Count);
		merged.WasMerged = false;
		return merged;
	}
	#endregion

	#region Match Aftermath
	///called by blaster
	public void DeativateTile(GameTile tile)
	{
		if (Level.HasCollectionObjective) PlayerHUD.Instance.CheckTileCount(tile);
		Level.PlaySound(tile.X, tile.Y, "TileSpawn");
		Level.KillTile(tile);
	}

	private void DeactivateTiles(List<TileMatch> matches)
	{
		var soundSpot = matches[0].TilesMatched[0];
		Level.PlaySound(soundSpot.X, soundSpot.Y, "Cascade");
		foreach (TileMatch m in matches)
		{
			if (m.WasMerged) continue;
			BombType BombToSpawn = GetBombToSpawn(m);
			if (BombToSpawn == BombType.NOT_A_BOMB) ClearTiles(m);
			else
			{
				TelemetryManager.LogEvent(evt, new MatchCSVLine[1]
				{
					new MatchCSVLine(evt.EventID, "Bomb_" + BombToSpawn.ToString(), 0, 0)
				});
				GameTile target = null;
				if (new List<GameTile>(m.TilesMatched).Contains(swapOriginTile))
				{
					target = swapOriginTile;
					swapOriginTile = null;
				}
				else
				{
					var rando = Random.Range(0, m.TilesMatched.Length);
					target = m.TilesMatched[rando];
					if (target.AlreadyChosen)
					{
						var i = rando == 0 ? rando + 1 : rando - 1;
						target = m.TilesMatched[i];
					}
				}
				//clear after determing bomb type, to determine T or L
				ClearTiles(m);
				target.AlreadyChosen = true;
				GameTile tile = Level.SetGridTile(target.X, target.Y, (int)BombToSpawn);
				Vector3[] p = Level.GetGridPoint(target.X, target.Y, true, false);
				Level.PlaySound(target.X, target.Y, "CreateSpecial");
				tile.transform.position = p[0];
				tile.transform.eulerAngles = p[1];
			}
		}
		swapOriginTile = null;
		if (BombChain > 0)
		{
			BombChain--;
			if (BombChain != 0) return;
		}
		if (!BlasterManager.Instance.IsBlasting) Level.StartCoroutine(Level.DoAfterSeconds(RefillBoard, Settings.BoardRefillDelay));
	}

	protected virtual BombType GetBombToSpawn(TileMatch m)
	{
		///only player moves make specials in tutorial
		///pay attention to level name index, may change
		//if (CascadeComboCount > 1 && inTutorialSpecial)
		//	return BombType.NOT_A_BOMB;
		///pizza
		//if (m.TilesMatched[0].Type == 10)
		//	return Level.BombForPizza;
		///not a bomb
		if (m.IsBomb || m.TilesMatched.Length <= 3)
			return BombType.NOT_A_BOMB;
		///square
		if (m.IsSquare)
		{
			///P shape
			if (m.IsVert || m.IsHoriz) return Level.BombForP;
			else return Level.BombForSquareMatches;
		}
		///T or L shape
		if (m.IsVert && m.IsHoriz)
		{
			int counter = 0;
			var tempList = new List<GameTile>();
			tempList.AddRange(m.TilesMatched);
			foreach (GameTile gt in m.TilesMatched)
			{
				counter = 0;
				if (gt.Y > 0 && tempList.Contains(Level.GameGrid[gt.X, gt.Y - 1]))
					counter++;
				if (gt.Y < Level.GridSizeY - 1 && tempList.Contains(Level.GameGrid[gt.X, gt.Y + 1]))
					counter++;
				if (gt.X > 0 && tempList.Contains(Level.GameGrid[gt.X - 1, gt.Y]))
					counter++;
				else if (gt.X == 0 && tempList.Contains(Level.GameGrid[Level.GridSizeX - 1, gt.Y]))
					counter++;
				if (gt.X < Level.GridSizeX - 1 && tempList.Contains(Level.GameGrid[gt.X + 1, gt.Y]))
					counter++;
				else if (gt.X == Level.GridSizeX - 1 && tempList.Contains(Level.GameGrid[0, gt.Y]))
					counter++;
				if (counter > 2)
					return Level.BombForT;
			}
			return Level.BombForL;
		}

		switch (m.TilesMatched.Length)
		{
			case 4: return Level.BombForMatch4;
			case 5: return Level.BombForMatch5;
			//case 6: return Level.BombForMatch6;	//probably never reached
			//default: return Level.BombForBiggestMatches;	//probably never reached
			default: return Level.BombForMatch5;
		}
	}

	//TODO: make score toasts not overlap (maybe spawn on random spot instead of closest, or track which are active and use an inactive one)
	protected virtual void ClearTiles(TileMatch m)
	{
		Transform closestSlot = null;
		foreach (GameTile t in m.TilesMatched)
		{
			if (closestSlot == null || Vector3.Distance(t.transform.parent.position, Level.Player.position) < Vector3.Distance(closestSlot.position, Level.Player.position))
				closestSlot = t.transform.parent;

			if (!t.gameObject.activeSelf) continue;
			if (Level.HasCollectionObjective) PlayerHUD.Instance.CheckTileCount(t);

			t.MyMatch = null;
			Level.KillTile(t);
		}

		if (m.Score != 0) Level.Spawner.SpawnScore(m.Score, closestSlot);
	}
	#endregion

	#region Refill Logic
	public void RefillBoard()
	{
		WaitForAll waiter = new WaitForAll(OnRefillComplete);
		waiter.Lock();
		for (int col = 0; col < Level.GridSizeX; col++)
		{
			RefillColumnHalf(col, waiter, true);
			if (!Level.NoMidRow)
				RefillColumnHalf(col, waiter, false);
		}
		waiter.Unlock(); //in case we somehow don't refill anything
	}

	private int GetFirstRowOfBottomHalf()
	{
		return Level.GridSizeY / 2 + Level.GridSizeY % 2;
	}

	private void RefillColumnHalf(int col, WaitForAll waiter, bool isTopHalf)
	{
		var odd = Level.GridSizeY % 2 > 0 && !Level.NoMidRow;
		int midRow = Level.NoMidRow ? Level.GridSizeY : GetFirstRowOfBottomHalf();
		midRow = isTopHalf && odd ? Level.GridSizeY / 2 : midRow;
		int startRow = isTopHalf ? midRow - 1 : midRow;
		int endRow = isTopHalf ? 0 : Level.GridSizeY - 1;
		int dir = isTopHalf ? -1 : 1;
		if (Level.ReverseGravity)
		{
			int swapRow = startRow;
			startRow = endRow;
			endRow = swapRow;
			dir *= -1;
		}

		//move tiles down the column
		LinkedList<GameLevel.SlotInfo> slots = null;
		GameTile Tile = null;
		for (int row = startRow; dir == 1 ? row <= endRow : row >= endRow; row += dir)
		{
			Tile = Level.GameGrid[col, row];
			if (slots == null && Tile == null && !Level.NullTileGrid.Get(col, row) && !Level.TempNullTileGrid.Get(col, row))
				slots = new LinkedList<GameLevel.SlotInfo>();
			if (slots != null && !Level.NullTileGrid.Get(col, row) && !Level.TempNullTileGrid.Get(col, row))
			{
				if (Tile != null)
				{
					Tile.AlreadyChosen = false;
					GameLevel.SlotInfo target = slots.Last.Value;
					Level.GameGrid[Tile.X, Tile.Y] = null;
					Coords oldPos = new Coords() { x = Tile.X, y = Tile.Y };
					Coords newPos = new Coords() { x = target.x, y = target.y };
					Tile.MoveBetweenSlots(oldPos, newPos, waiter);
					Level.GameGrid[target.x, target.y] = Tile;
					slots.RemoveLast();
				}
				slots.AddFirst(Level.GridSlots[col, row]);
			}
		}

		//spawn new tiles in remaining empty slots
		if (slots != null)
		{
			int numSpawned = 0;
			while (slots.Count > 0)
			{
				GameLevel.SlotInfo target = slots.Last.Value;
				int type = GetRandomTile();
				int y = endRow + dir * (1 + numSpawned);
				Coords startPos = new Coords() { x = target.x, y = y };
				Coords endPos = new Coords() { x = target.x, y = target.y };
				GameTile tile = Level.SetGridTile(endPos.x, endPos.y, type);
				Vector3[] p = Level.GetGridPoint(startPos.x, startPos.y, true, false);
				tile.transform.position = p[0];
				tile.transform.eulerAngles = p[1];
				tile.MoveBetweenSlots(startPos, endPos, waiter);
				tile.HideTile(endRow, dir < 0);
				slots.RemoveLast();
				if (GameTile.selected == null)
					GameTile.selected = tile;
				Tile = tile;
				numSpawned++;
			}
		}
	}

	private Transform[] ToArray(LinkedList<GameLevel.SlotInfo> slots)
	{
		Transform[] result = new Transform[slots.Count];
		int i = 0;
		foreach (GameLevel.SlotInfo s in slots)
		{
			result[i] = s.transform;
			i++;
		}
		return result;
	}

	private void OnRefillComplete()
	{
		if (CheckAllMatches(Level.GameGrid, true))
		{
			CascadeComboCount++;
			evt = new EventCSVLine(EventCSVLine.EventType.ChainMatch);
			evt.ChainDepthCount = CascadeComboCount.ToString();
			evt.ChainStartEventID = TelemetryManager.LastMoveID;
			evt.PlayerMoveType = EventCSVLine.MoveType.Chain;
			Level.StartCoroutine(Level.DoAfterSeconds(ProcessAllMatches, Settings.ComboDelayTime));
		}
		else
		{
			//end level fanfare cycle
			if(GameLevel.DidWin)
			{
				GameLevel.Instance.ClearSpecials();
				return;
			}
			if (Level.CheckIfWon()) ExecuteWinState();
			else CheckForPossibleMatches(false);
			CascadeComboCount = 1;
			if (GameLevel.IsFinished) GameLevel.IsInteractive = true;
			else
			{
				var ufos = Object.FindObjectsOfType<GameTileUFO>();
				if (PlayerTookAction && ufos.Length > 0)
				{
					PlayerTookAction = false;
					var waiter = new WaitForAll(FinishMovingUFO);
					foreach (var v in ufos) v.MoveTowardTarget(waiter);
				}
				else GameLevel.IsInteractive = true;
			}
		}
	}
	#endregion

	#region UFO
	//public void SpawnUFOInit()
	//{
	//	for (int i = 0; i < Level.NumberOfUFO; i++)
	//	{
	//		var rando = GetUFOStartTile();
	//		Level.Spawner.DespawnTile(rando.gameObject, rando.Type);
	//		var til = Level.SetGridTile(rando.X, rando.Y, 21);
	//		til.ShowHideTile(false);
	//	}
	//}
	public void SpawnUFO()
	{
		var targetTile = GetUFOStartTile();
		var spawnedTile = GameLevel.Instance.Spawner.SpawnTile(21);
		spawnedTile.transform.position = targetTile.transform.position;
		spawnedTile.transform.rotation = targetTile.transform.rotation;
		spawnedTile.transform.Translate(Vector3.forward * 2500);
		spawnedTile.GetComponent<GameTileUFO>().MoveIntoPlaceOnSpawn(targetTile);
	}
	public GameTile GetUFOStartTile()
	{
		var lis = new List<GameTile>();
		for (int c = 0; c < Level.GridSizeX; c++)
		{
			for (int r = 0; r < Level.GridSizeY; r++)
			{
				if (Level.NullTileGrid.Get(c, r) || Level.GetGridTile(c, r).IsBomb) continue;
				lis.Add(Level.GetGridTile(c, r));
			}
		}
		var til = lis[Random.Range(0, lis.Count)];
		return til;
	}
	public void FinishMovingUFO()
	{
		if (!CheckAllMatches(Level.GameGrid, false))
			GameLevel.IsInteractive = true;
		TelemetryManager.LogEvent(evt, mtc);
		//GameLevel.Instance.CheckIfLostByUFO();
	}
	#endregion

	public void ExecuteWinState()
	{
		if (GameLevel.IsFinished) return;
		GameLevel.IsFinished = true;
		GameLevel.DidWin = true;
		MenuScreen.AcceptsInput = false;
		if (EndScreen.Instance == null) Debug.LogError("There is no EndScreen prefab in the level!");
		else Level.EndLevelFanfare();
	}
}