﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLevel : MonoBehaviour
{
	#region Variables
	public static GameLevel Instance;
	//public static int SuccessfulUFOs;
	public static bool IsInteractive;
	public static bool IsFinished;
	public static bool DidWin;

	[System.Serializable]
	public struct SlotInfo
	{
		public int x;
		public int y;
		public Transform transform;
		public AudioSource audio;
		public GameObject line;
	}

	public SharedGameSettings Settings;

	[Header("Basics"), Range(4, 30)]
	public int GridSizeX = 20;
	[Range(2, 14)]
	public int GridSizeY = 6;
	[Range(10, 90)]
	public float PolarCapsDegrees = 50f;
	public float GridRadius = 250;
	public float PlanetRadius;
	public float TileSize = 1f;
	//public AudioClip LevelSongIntro;
	//public AudioClip LevelSongLoop;
	//public int LevelSongIndex;
	public bool HasCollectionObjective;
	public bool HasScoreObjective;
	public int[] UFOSpawnMoveCountIntervals;	//for spawning UFO mid-round after player makes X moves
	//public int NumberOfUFO;
	//public int UFOFailCount;
	//public float FailLevelTime;
	public int FailMoverCounter;
	public int ScoreBonusMoveCounterOffset;
	public GameObject[] NullObsticlePrefabs;

	[Header("Gravity")]
	public bool ReverseGravity;
	public bool NoMidRow;

	[Header("Scoring")]
	public int Score3Stars = 20000;
	public int Score2Stars = 15000;

	[Header("Bomb Spawn Rules")]
	public BombType BombForSquareMatches = BombType.Cross5x5;
	public BombType BombForMatch4 = BombType.HalfCross;
	public BombType BombForMatch5 = BombType.ColorBomb;
	//public BombType BombForMatch6 = BombType.BlasterAuto;
	//public BombType BombForBiggestMatches = BombType.BlasterAuto;
	public BombType BombForP = BombType.ConeBomb3x;
	public BombType BombForL = BombType.BlasterAuto;
	public BombType BombForT = BombType.Neighbors5x5;
	public BombType BombForPizza = BombType.ShuffleBoard;

	[HideInInspector] public SharedGameSettings.BoardMeshSettings BoardMeshOverride;
	[HideInInspector] public bool[] MeshOverrideFields;
	public bool ShowMeshOverrideEditor { get; set; }
	public SharedGameSettings.BoardMeshSettings BoardMeshSettings
	{
		get
		{
			object s = Settings.DefaultMeshSettings;
			if (MeshOverrideFields != null && MeshOverrideFields.Length > 0)
			{
				FieldInfo[] fields = typeof(SharedGameSettings.BoardMeshSettings).GetFields(BindingFlags.Instance | BindingFlags.Public);
				for (int i = 0; i < MeshOverrideFields.Length && i < fields.Length; i++)
					if (MeshOverrideFields[i])
						fields[i].SetValue(s, fields[i].GetValue(BoardMeshOverride));
			}
			return (SharedGameSettings.BoardMeshSettings)s;
		}
	}

	[HideInInspector] public float[] TileProbability;
	public bool ShowProbabilityEditor { get; set; }
	[HideInInspector] public Flat2DArrayBool NullTileGrid;
	[HideInInspector] public Flat2DArrayBool TempNullTileGrid;
	public bool ShowNullTileGrid { get; set; }
	[HideInInspector] public HazardLayer.HazardLayerInstance[] HazardLayers;
	public bool ShowHazardLayerEditor { get; set; }

	public GameTile[,] GameGrid { get; private set; }
	public SlotInfo[,] GridSlots { get; private set; }

	[HideInInspector] public GameLogic Logic;
	private HazardLayer HazardLayer;
	public SpawnPools Spawner { get; private set; }
	public Transform Player { get; private set; }
	[HideInInspector] public int CurrentSelectionSoundID;

	private bool HasInit = false;
	private int cascadeChordStartingPoint;
	[HideInInspector] public float HintTimer;
	private bool didEndLevel;
	private bool didClearSpecials;
	private bool canPlaySound = true;

	public GameLevel()
	{
		ShowProbabilityEditor = true;
		ShowNullTileGrid = true;
		ShowHazardLayerEditor = true;
	}
	#endregion

	private void Awake()
	{
		Instance = this;
		IsInteractive = false;
		DidWin = false;
		IsFinished = false;
	}

	private void Start()
	{
		if (Settings == null)
		{
			Debug.LogError("Unable to find SharedGameSettings. " +
				"Create them by going to Create > Tetristar > Shared Game Settings, " +
				"and then configuring the 'settings' field for the GameLevel behaviour.");
		}
		else
		{
			if (GridSizeX % 2 == 1) Debug.LogError("Please don't make levels with an odd number of columns.");
			Scene s;
			if (IsStaticSceneReady(out s)) Init(s);
		}
	}

	public bool UsesHazards
	{
		get { return HazardLayers.Length > 0; }
	}

	private bool IsStaticSceneReady(out Scene s)
	{
		string myName = SceneManager.GetActiveScene().name;
		string staticName = myName + "_static";
		s = SceneManager.GetSceneByName(staticName);

		if (s.IsValid()) return s.GetRootGameObjects().Length > 0;

		SceneManager.LoadScene(staticName, LoadSceneMode.Additive);
		s = SceneManager.GetSceneByName(staticName);
		return s.IsValid() && s.GetRootGameObjects().Length > 0;
	}

	private void Update()
	{
		if (HasInit)
		{
			if (IsInteractive)
			{
				HintTimer += Time.deltaTime;
				if (HintTimer >= Settings.HintInterval) Logic.CheckForPossibleMatches(true);
			}
			//if(0f != FailLevelTime && PlayerHUD.LevelTimer >= FailLevelTime) LevelFailure();
			return;
		}

		Scene s;
		if (IsStaticSceneReady(out s)) Init(s);
	}

	private void Init(Scene staticScene)
	{
		var story = Settings.LevelList.GetCurrentStory();
		if (!string.IsNullOrEmpty(story)) SceneManager.LoadScene(story, LoadSceneMode.Additive);

		HasInit = true;

		Player = GameObject.FindWithTag("Player").transform;
		var bucket = new GameObject("Spawn Pool").transform;
		bucket.SetParent(transform, false);
		bucket.SetSiblingIndex(0);
		Spawner = new SpawnPools(Settings, bucket);

		var coll = gameObject.AddComponent<SphereCollider>();
		coll.radius = GridRadius;
		coll.isTrigger = true;

		if (null != PlayerHUD_Tutorial_Specials.InstanceSpecial) Logic = new GameLogic_Balloons(this);
		else Logic = new GameLogic(this);

		GameGrid = new GameTile[GridSizeX, GridSizeY];
		InitStaticObjects(staticScene);

		TempNullTileGrid.SetSize(NullTileGrid.Width, NullTileGrid.Height);

		Logic.InitBoard(false);
		//Logic.SpawnUFOInit();
	}

	public void EndLevelFanfare()
	{
		if (didEndLevel) return;
		didEndLevel = true;
		PlayerHUD.Instance.FanfareResize();
		MusicManager.Instance.FadeMusic(true, EndScreen.Instance.FanfareIntro);
		MusicManager.Instance.SetupNextSong(EndScreen.Instance.FanfareIntro.length, EndScreen.Instance.FanfareLoop);
		ClearSpecials();
	}

	public void ClearSpecials()
	{
		var specials = new List<GameTile>();
		foreach(var gt in GameGrid)
			if(null != gt && gt.IsBomb && gt.TypeOfBomb != BombType.BlasterAuto && gt.TypeOfBomb != BombType.UFO)
				specials.Add(gt);
		if (0 == specials.Count)
		{
			if (!didClearSpecials)
			{
				didClearSpecials = true;
				RevealBoard(false);
			}
		}
		else foreach (var gt in specials) gt.ActivateSpecial(true, SwipeDirection.Undefined, Logic.GetRandomTile());
	}

	public void RevealBoard(bool reveal)
	{
		StartCoroutine(RevealingBoard(reveal));
	}

	private IEnumerator RevealingBoard(bool revealing)
	{
		var cycles = 0;
		var lis = new List<GameTile>();
		foreach (GameTile gt in GameGrid) lis.Add(gt);
		while (lis.Count > 0)
		{
			var rando = Random.Range(0, lis.Count);
			var til = lis[rando];
			if (til == null) lis.RemoveAt(rando);
			else
			{
				lis.Remove(til);
				til.ShowHideTile(revealing);
				cycles += 1;
				PlaySound(til.X, til.Y, "TileSpawn");
				Spawner.SpawnVFX(Spawner.TypeInitVFX, GridSlots[til.X, til.Y].transform);
				yield return new WaitForSeconds(Settings.TileRevealDelay / cycles);
			}
		}
		if (!revealing) EndScreen.Instance.Activate();
		IsInteractive = true;
	}

	private void InitStaticObjects(Scene s)
	{
		var store = s.GetRootGameObjects()[0].GetComponent<StaticObjectsHolder>();

		//store.GridTransform.SetParent(transform, false);
		store.GridTransform.localPosition = Vector3.zero;
		GridSlots = store.GetGridSlots();

		store.BoardMesh.SetParent(transform, false);

		if (UsesHazards)
		{
			HazardLayer = store.HazardLayer;
			HazardLayer.transform.SetParent(transform, false);
			HazardLayer.transform.localPosition = Vector3.zero;
			HazardLayer.Init(this);
		}
	}

	/**
	 * Returns [position, rotation] for arbitrary coordinates on spherical grid
	 */
	public Vector3[] GetGridPoint(float x, float y, bool centerTile = true, bool localSpace = true, float radiusOffset = 0)
	{
		float degreePerX = 360f / GridSizeX;
		float degreePerY = PolarCapsDegrees * 2f / GridSizeY;

		float myDegreeX = -90f - degreePerX * x - (centerTile ? degreePerX / 2 : 0);
		float myDegreeY = -PolarCapsDegrees + degreePerY * y + (centerTile ? degreePerY / 2 : 0);

		Vector3 p = Vector3.forward * (GridRadius + radiusOffset);
		p = Quaternion.AngleAxis(myDegreeX, Vector3.up) * Quaternion.AngleAxis(myDegreeY, Vector3.right) * p;

		Vector3[] result = new Vector3[2];
		result[0] = localSpace ? p : transform.TransformPoint(p);
		result[1] = ((localSpace ? Quaternion.identity : transform.rotation) * Quaternion.Euler(myDegreeY, myDegreeX, 0)).eulerAngles;
		return result;
	}

	public GameTile GetGridTile(int x, int y)
	{
		GameTile tile = GameGrid[x, y];
		if (tile == null)
			Debug.LogError("Found a null slot in game grid at [" + x + ", " + y + "]");
		return tile;
	}

	public int GetGridTileType(int x, int y)
	{
		GameTile tile = GameGrid[x, y];
		return tile == null ? -1 : tile.Type;
	}

	public GameTile SetGridTile(int x, int y, int tileType)
	{
		Transform tileTransform = GridSlots[x, y].transform;
		GameTile tile = Spawner.SpawnTile(tileType);
		tile.Init(x, y, tileType, Settings, Logic);
		//tile.InitScaling();
		tile.transform.SetParent(tileTransform, false);
		if (Settings.TilesStandUpStraight)
			tile.transform.localEulerAngles = new Vector3(-tileTransform.localEulerAngles.x, 0, 0);
		GameGrid[x, y] = tile;
		return tile;
	}

	public void KillTile(GameTile t, bool hitHazrds = true)
	{
		if(t.TypeOfBomb == BombType.NOT_A_BOMB) Spawner.SpawnVFX(t.Type, GridSlots[t.X, t.Y].transform);
		GameGrid[t.X, t.Y] = null;
		Spawner.DespawnTile(t.gameObject, t.Type);

		if (UsesHazards) HazardLayer.BreakHazard(t.X, t.Y);
		if (TempNullTileGrid.Get(t.X, t.Y)) TempNullTileGrid.Set(t.X, t.Y, false);
	}

#if UNITY_EDITOR
	public void SwapTile(GameTile before, int after)
	{
		int x = before.X;
		int y = before.Y;
		KillTile(before);

		SetGridTile(x, y, after);
	}
#endif

	public void SpawnExplosionEffects(GameTile[] tiles)
	{
		foreach (GameTile t in tiles)
		{
			var slot = GridSlots[t.X, t.Y].transform;
			Spawner.SpawnVFXExplosion(slot);
		}
	}

	public delegate void SimpleCallback();
	public IEnumerator DoAfterSeconds(SimpleCallback callback, float time)
	{
		yield return new WaitForSecondsRealtime(time);
		callback();
	}

	public void PlaySound(int x, int y, string name)
	{
		SharedGameSettings.SoundEffectInfo s;	
		if (name == "Selection")
		{
			s = Settings.Sounds[0].Files[CurrentSelectionSoundID];
			GridSlots[x, y].audio.PlayOneShot(s.Clip, s.VolumeAdd + Settings.DefaultSoundVolume);
			CurrentSelectionSoundID = CurrentSelectionSoundID == Settings.Sounds[0].Files.Length - 1 ? 0 : CurrentSelectionSoundID + 1;
		}
		else if (name == "Cascade")
		{
			if (!canPlaySound) return;
			StartCoroutine(SoundCooldown());
	
			cascadeChordStartingPoint = Logic.CascadeComboCount == 1 ? Random.Range(0, Settings.CascadeChordStartingPoints.Length) : cascadeChordStartingPoint;
			var val = cascadeChordStartingPoint + Logic.CascadeComboCount - 1;
			val = val > Settings.Sounds[5].Files.Length - 1 ? 0 : val;
			s = Settings.Sounds[5].Files[val];
			GridSlots[x, y].audio.PlayOneShot(s.Clip, s.VolumeAdd + Settings.DefaultSoundVolume);
		}
		else if (Settings.GetRandomSound(name, out s))
		{
			if (name == "RemoveObstruction")
			{
				if (!canPlaySound) return;
				StartCoroutine(SoundCooldown());
			}
			GridSlots[x, y].audio.PlayOneShot(s.Clip, s.VolumeAdd);
		}
	}

	private IEnumerator SoundCooldown()
	{
		canPlaySound = false;
		yield return new WaitForEndOfFrame();
		canPlaySound = true;
	}

	public bool CheckIfWon()
	{
		if(HasCollectionObjective)
		{
			if(UsesHazards)
			{
				//score + hazards + collection
				if(HasScoreObjective)
				{
					var t1 = HazardLayer.CheckIfPlayerWon();
					var t2 = PlayerHUD.Instance.DidPlayerWin();
					var t3 = (Scoreboard.LevelScore >= PlayerHUD.Instance.ScoreGoal);
					return (t1 && t2 && t3);
				}
				//collection + hazards
				else
				{
					var t1 = HazardLayer.CheckIfPlayerWon();
					var t2 = PlayerHUD.Instance.DidPlayerWin();
					return (t1 && t2);
				}
			}
			//collection + score
			else if(HasScoreObjective)
			{
				var t1 = PlayerHUD.Instance.DidPlayerWin();
				var t2 = (Scoreboard.LevelScore >= PlayerHUD.Instance.ScoreGoal);
				return (t1 && t2);
			}
			//collection
			else return PlayerHUD.Instance.DidPlayerWin();
		}
		else if(HasScoreObjective)
		{
			//score + hazards
			if(UsesHazards)
			{
				var t1 = HazardLayer.CheckIfPlayerWon();
				var t2 = (Scoreboard.LevelScore >= PlayerHUD.Instance.ScoreGoal);
				return (t1 && t2);
			}
			//score
			else return (Scoreboard.LevelScore >= PlayerHUD.Instance.ScoreGoal);
		}
		else if(null != PlayerHUD_Tutorial_Specials.InstanceSpecial)
		{
			if (PlayerHUD_Tutorial_Specials.InstanceSpecial.CurrentStage >= 2) return true;
			return false;
		}
		//hazards
		else return HazardLayer.CheckIfPlayerWon();
	}

	public void LevelFailure()
	{
		if (IsFinished) return;
		IsFinished = true;
		RevealBoard(false);
		//PlayerHUD.Instance.UpdateFailMoveCounter();
		MusicManager.Instance.FadeMusic(true, EndScreen.Instance.LoserIntro);
		MusicManager.Instance.SetupNextSong(EndScreen.Instance.LoserIntro.length, EndScreen.Instance.LoserLoop);
		StoryManager.Instance.ToggleTutorial(false);
	}
}