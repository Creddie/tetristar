﻿using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "LevelList", menuName = "Tetristar/Level List")]
public class LevelList : ScriptableObject
{

#if UNITY_EDITOR
	private static LevelList me = null;
	public static bool IsSaving { get; private set; }

	[MenuItem("Invrse/Save Level List")]
	public static void SaveLevelStrings()
	{
		IsSaving = true;
		if (me != null)
		{
			//update string list of names
			if (me.LevelNames.Length != me.Levels.Length)
				me.LevelNames = new string[me.Levels.Length];
			if (me.StoryNames.Length != me.Stories.Length)
				me.StoryNames = new string[me.Stories.Length];
			for (int i = 0; i < me.Levels.Length; i++)
				me.LevelNames[i] = me.Levels[i] == null ? string.Empty : me.Levels[i].name;
			for (int i = 0; i < me.Stories.Length; i++)
				me.StoryNames[i] = me.Stories[i] == null ? string.Empty : me.Stories[i].name;

			//force myself to serialize to disk
			string[] results = AssetDatabase.FindAssets("t:LevelList");
			for (int i = 0; i < results.Length; i++)
				results[i] = AssetDatabase.GUIDToAssetPath(results[i]);
			AssetDatabase.ForceReserializeAssets(results, ForceReserializeAssetsOptions.ReserializeAssets);
		}
		IsSaving = false;
	}

	public LevelList()
	{
		me = this;
	}
#endif

	public Object[] Levels;
	public Object[] Stories;
	[HideInInspector]
	public string[] LevelNames;
	public string[] StoryNames;

	public string GetNextLevel()
	{
		string current = SceneManager.GetActiveScene().name;
		for (int i = 0; i < LevelNames.Length; i++)
			if (LevelNames[i] == current)
				return LevelNames[(i + 1) % LevelNames.Length];
		return current;
	}

	public string GetCurrentStory()
	{
		string current = SceneManager.GetActiveScene().name;
		for (int i = 0; i < LevelNames.Length; i++)
			if (LevelNames[i] == current)
				return StoryNames[i];
		return string.Empty;
	}
}