﻿/*
 * Each bomb type's value must be the same as the bomb's index in the array SharedGameSettings.Tiles
 */
public enum BombType
{
	NOT_A_BOMB = 0,
	Neighbors3x3 = 5,
	HalfCross = 6,
	FullCross = 7,
	ColorBomb = 8,
	BlasterAuto = 11,
	Cannon = 13,
	Cross5x5 = 12,
	Neighbors5x5 = 14,
	ShuffleBoard = 15,
	ConeBomb3x = 16,
	UFO = 17
}