﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTile : MonoBehaviour, VRInteractable
{
	#region Variables
	public static GameTile selected = null;

	public int X { get; private set; }
	public int Y { get; private set; }
	public int Type { get; private set; }
	public bool IsImmovable { get; protected set; }
	public bool CanSwipe { get { return !NoSwiping && !IsImmovable; } }
	public BombType TypeOfBomb { get; set; }
	public bool IsBomb { get { return TypeOfBomb != BombType.NOT_A_BOMB; } }
	public bool HasActivated { get; protected set; }

	private bool NoSwiping;
	protected bool HasInit = false;
	protected SharedGameSettings Settings;
	private GameLogic Logic;

	public bool AlreadyChosen { get; set; }
	public GameLogic.TileMatch MyMatch { get; set; }

	//animation
	private Vector3 StartSize;
	//private Vector3 TargetSize;
	//private Vector3 TargetSizeDrag;
	private Animation ThisAnim;

	private Vector3 DragDistance = Vector3.zero;
	private bool DragThresholdHit = false;
	private List<GameTile> specialHighlightTiles;
	private bool DragHighlightThresholdHit = false;

	//private float ScaleTime = 0;
	//private float InitScaleTime = -1f;
	private Vector3 ScaleStart;
	private Vector3 ScaleEnd;
	private bool canScale;
	private SwipeDirection specialDir;

	private Vector2 MoveDirction = Vector2.zero;
	private Vector2 VirtualPosition;
	private Vector2 EndPosition;
	private bool IsHidden = false;
	private int HidingRow;
	private bool HidingGoingUp;
	private WaitForAll MoveWaiter;
	[HideInInspector] public bool MarkedForDespawn;

	protected float ActivateTime = 0;
	protected float ActivateLength = 0;

	private Quaternion CannonBaseRotation;

	private int ColorBombTargetType;
	public ParticleSystem SpecialHighlightVFX { get; set; }
	public ParticleSystem GrabTileVFX { get; set; }
	#endregion

	public void Init(int x, int y, int type, SharedGameSettings settings, GameLogic logic)
	{
		X = x;
		Y = y;
		Type = type;
		Logic = logic;
		Settings = settings;

		HasActivated = false;
		//ScaleTime = 0;
		ActivateTime = 0;

		if (HasInit) return;
		HasInit = true;

		NoSwiping = Settings.Tiles[type].NoSwiping;
		IsImmovable = Settings.Tiles[type].Immovable;
		TypeOfBomb = Settings.Tiles[type].BombType;
		ResetSpecialFacingDirection();

		BoxCollider coll = gameObject.AddComponent<BoxCollider>();
		coll.size = settings.colliderSize;
		gameObject.layer = settings.TileLayer;

		StartSize = transform.localScale;
		//TargetSize = Vector3.one * Settings.HighlightSize;
		//TargetSizeDrag = Vector3.one * Settings.DragHighlightSize;
		ThisAnim = GetComponentInChildren<Animation>();
	}

#if UNITY_EDITOR
	public void EditorSwapTileUI()
	{
		Transform ui = Instantiate(Settings.SwapUIPrefab, transform, false).transform;
		Vector3 dir = (FindObjectOfType<OVRManager>().transform.position - transform.position).normalized;
		ui.position = transform.position + dir * Settings.SwapUIDistance;
		ui.GetComponentInChildren<SwapTileUI>().ToSwap = this;

		transform.localScale = StartSize;
	}

	public void EditorSwapTile(int type)
	{
		Logic.Level.SwapTile(this, type);
	}
#endif

	protected virtual void OnDisable()
	{
		transform.localScale = StartSize;
		MarkedForDespawn = false;
		DragThresholdHit = false;
		DragHighlightThresholdHit = false;
		///for when half-crosses are triggered by full-crosses and temporarily become full-crosses
		if (null != Settings) TypeOfBomb = Settings.Tiles[Type].BombType;
		ResetSpecialFacingDirection();
	}

	//public void InitScaling()
	//{
	//	InitScaleTime = Time.time;
	//	//Debug.Log(InitScaleTime);
	//	//StartSize = transform.localScale;
	//	//Debug.Log(StartSize);
	//	transform.localScale = Vector3.zero;
	//	canScale = false;
	//}

	#region Scaling
	private IEnumerator scalingCoro;
	
	public void ChangeScale(AnimationCurve curve)
	{
		if (null != scalingCoro) StopCoroutine(scalingCoro);
		scalingCoro = Scaling(curve);
		StartCoroutine(scalingCoro);
	}

	private IEnumerator Scaling(AnimationCurve curve)
	{
		var timer = 0f;
		//var curve = 
		//var start = transform.localScale;
		//var end = Vector3.one * target;
		while (timer < Settings.SizeChangeTime)
		{
			timer += Time.deltaTime;
			transform.localScale = curve.Evaluate(timer / Settings.SizeChangeTime) * StartSize;
			//transform.localScale = Vector3.Lerp(start, end, timer / Settings.SizeChangeTime);
			yield return new WaitForEndOfFrame();
		}
		//transform.localScale = end;
	}
	#endregion

	private void FixedUpdate()
	{
		UpdateMove();
		//if (TypeOfBomb == BombType.HalfCross || TypeOfBomb == BombType.ConeBomb3x) UpdateCannon();
		//if (ScaleTime != 0)
		//{
		//	float diff = (Time.time - ScaleTime) / Settings.SizeChangeTime;
		//	if (diff >= 1)
		//	{
		//		transform.localScale = ScaleEnd;
		//		ScaleTime = 0;
		//	}
		//	else
		//		transform.localScale = Vector3.Lerp(ScaleStart, ScaleEnd, diff);
		//}

		//else if (InitScaleTime != -1f)
		//{
		//	float diff = (Time.time - InitScaleTime) / Settings.InitSizeChangeTime;
		//	//Debug.Log("scaling " + diff);
		//	if (diff >= 1f)
		//	{
		//		transform.localScale = StartSize;
		//		InitScaleTime = -1f;
		//		canScale = true;
		//	}
		//	else
		//		transform.localScale = Vector3.Lerp(Vector3.zero, StartSize, diff);
		//}

		if (ActivateTime != 0)
		{
			if (Time.time - ActivateTime >= ActivateLength)
			{
				ActivateTime = 0;
				if(TypeOfBomb == BombType.ColorBomb) Logic.DetonateBomb(this, specialDir, ColorBombTargetType);
				else Logic.DetonateBomb(this, specialDir);
			}
		}
	}

	//private float TimeResetStarted = 0;
	//private Quaternion RotStarted = Quaternion.identity;

	private void UpdateCannon()
	{
		//if (IsDragging || HasActivated) return;
		//Transform cannon = transform.GetChild(0);
		//if (cannon.localRotation == CannonBaseRotation) return;
		//if (TimeResetStarted == 0)
		//{
		//	TimeResetStarted = Time.time;
		//	RotStarted = cannon.localRotation;
		//}
		//float i = (Time.time - TimeResetStarted) / Settings.SizeChangeTime;
		//if (i >= 1)
		//{
		//	cannon.localRotation = CannonBaseRotation;
		//	TimeResetStarted = 0;
		//	RotStarted = Quaternion.identity;
		//}
		//else cannon.localRotation = Quaternion.Lerp(RotStarted, CannonBaseRotation, i);
	}

	private void UpdateMove()
	{
		if (MoveDirction == Vector2.zero)
			return;

		VirtualPosition += MoveDirction * Settings.MoveSpeed * Time.deltaTime;

		if (IsHidden && (HidingGoingUp ? VirtualPosition.y >= HidingRow - 1 : VirtualPosition.y <= HidingRow + 1))
			ShowHideTile(true);

		bool passedX = (MoveDirction.x == 1 && VirtualPosition.x >= EndPosition.x) || (MoveDirction.x == -1 && VirtualPosition.x <= EndPosition.x);
		bool passedY = (MoveDirction.y == 1 && VirtualPosition.y >= EndPosition.y) || (MoveDirction.y == -1 && VirtualPosition.y <= EndPosition.y);

		if (passedX || passedY)
		{
			#region Bounce/Shake
			var anim = "";
			var direc = 0f;
			if (MoveDirction.y == 1f || MoveDirction.y == -1f)
			{
				//anim = MoveDirction.y == 1f ?  "Shimmy_Up" : "Shimmy_Down";
				anim = "GemBounce";
				direc = MoveDirction.y;
				ThisAnim[anim].speed = direc;
				var tim = direc == 1 ? 0f : ThisAnim[anim].length;
				ThisAnim[anim].time = tim;
				ThisAnim.Play(anim);
			}
			//else if(MoveDirction.x == 1f || MoveDirction.x == -1)
			//{
			//	anim = "GemShake";
			//	direc = -MoveDirction.x;

			//	//ThisAnim[anim].speed = direc;
			//	//var tim = direc == 1 ? 0f : ThisAnim[anim].length;
			//	//ThisAnim[anim].time = tim;
			//}
			//ThisAnim[anim].speed = direc;
			//var tim = direc == 1 ? 0f : ThisAnim[anim].length;
			//ThisAnim[anim].time = tim;
			//ThisAnim.Play(anim);
			#endregion

			//finished
			MoveDirction = Vector2.zero;
			transform.localPosition = Vector3.zero;
			transform.localEulerAngles = Vector3.zero;

			//TODO: this can play too many times on top of each other, gets too loud/distorted
			//Logic.Level.PlaySound(X, Y, "Cascade");
			CheckToDisableMoveWaiter();
		}
		else
		{
			Vector3[] p = Logic.Level.GetGridPoint(VirtualPosition.x, VirtualPosition.y, true, false);
			transform.position = p[0];
			transform.eulerAngles = p[1];
		}
	}

	public void CheckToDisableMoveWaiter()
	{
		if (null != MoveWaiter)
		{
			MoveWaiter.Unlock();
			MoveWaiter = null;
		}
	}

	public void HideTile(int appearInRow, bool goingUp)
	{
		HidingRow = appearInRow;
		HidingGoingUp = goingUp;
		ShowHideTile(false);
	}

	public void ShowHideTile(bool show)
	{
		IsHidden = !show;
		foreach (Renderer r in GetComponentsInChildren<Renderer>(true))
			r.enabled = show;
	}

	public virtual void ActivateSpecial(bool chain, SwipeDirection swipeDir, int type = -1)
	{
		if (IsBomb && !HasActivated)
		{
			SaveLoadManager.TryShowSpecialTooltip(TypeOfBomb, true);
			//GameLevel.IsInteractive = false;
			HasActivated = true;
			Logic.Level.Spawner.SpawnVFX(Type, transform.parent);
			Logic.Level.PlaySound(X, Y, "Explode");
			specialDir = swipeDir;
			ColorBombTargetType = type;
			ActivateTime = Time.time;
			ActivateLength = chain ? Settings.SpecialActivateChainTime : Settings.SpecialActivateTime;

			if (TypeOfBomb == BombType.HalfCross || TypeOfBomb == BombType.ConeBomb3x)
			{
				if (swipeDir == SwipeDirection.Undefined)
				{
					var rando = Random.Range(0, 4);
					switch (rando)
					{
						case 0: swipeDir = SwipeDirection.Up; break;
						case 1: swipeDir = SwipeDirection.Down; break;
						case 2: swipeDir = SwipeDirection.Left; break;
						case 3: swipeDir = SwipeDirection.Right; break;
					}
					specialDir = swipeDir;
				}
				SetSpecialFacingDirection(swipeDir, false);
				if (TypeOfBomb == BombType.HalfCross) GetComponentInChildren<UnicornScaler>().GetComponent<Animator>().SetTrigger("Run");
			}
		}
	}

	private void ResetSpecialFacingDirection()
	{
		if (TypeOfBomb == BombType.HalfCross || TypeOfBomb == BombType.ConeBomb3x)
		{
			var kid = transform.GetChild(0);
			if (TypeOfBomb == BombType.HalfCross) kid.localRotation = Quaternion.Euler(new Vector3(0f, 90f, 0f));
			else if (TypeOfBomb == BombType.ConeBomb3x) kid.localRotation = Quaternion.Euler(Vector3.zero);
			CannonBaseRotation = kid.localRotation;
		}
	}

	private void SetSpecialFacingDirection(SwipeDirection swipeDir, bool reset)
	{
		var rot = Vector3.zero;
		if (reset) rot = CannonBaseRotation.eulerAngles;
		else
		{
			switch (TypeOfBomb)
			{
				case BombType.HalfCross:
					switch (swipeDir)
					{
						case SwipeDirection.Down: rot = new Vector3(90f, -90f, 0f); break;
						case SwipeDirection.Up: rot = new Vector3(-90f, 90f, 0f); break;
						case SwipeDirection.Right: rot = new Vector3(0f, -90f, 0f); break;
						case SwipeDirection.Left: rot = CannonBaseRotation.eulerAngles; break;
					}
					break;
				case BombType.ConeBomb3x:
					switch (swipeDir)
					{
						case SwipeDirection.Down: rot = new Vector3(0f, 0f, 180f); break;
						case SwipeDirection.Up: rot = CannonBaseRotation.eulerAngles; break;
						case SwipeDirection.Right: rot = new Vector3(0f, 0f, 90f); break;
						case SwipeDirection.Left: rot = new Vector3(0f, 0f, -90f); break;
					}
					break;
			}
		}
		transform.GetChild(0).localEulerAngles = rot;
	}

	public void OnPointerEnter()
	{
		if (IsHidden) return;
		Highlight(true, true, false);
	}

	public void OnPointerExit()
	{
		if (IsHidden) return;
		Highlight(false, false, false);
	}

	private void Highlight(bool getLit, bool makeSound, bool withVFX)
	{
		if (!gameObject.activeInHierarchy || !StoryManager.IsDone || (getLit && MenuScreen.IsPaused))// || !canScale)
			return;

		//ScaleTime = Time.time;
		//ScaleStart = transform.localScale;
		//ScaleEnd = getLit ? TargetSize : StartSize;
		ChangeScale(getLit ? Settings.SizeCurveTileHighlightGrow : Settings.SizeCurveTileHighlightShrink);

		if (TypeOfBomb != BombType.NOT_A_BOMB) DynamicTooltipManager.Instance.TryShowTooltip(TypeOfBomb, getLit, false);
		if (withVFX) AdjustSpecialHighlightVFX(getLit);
		if (!getLit && PlayerHUD_Tutorial_Basics.CanSwipe) PlayerHand.Instance.ChangePointerColor(true);
		else if (makeSound)
		{
			if (!Settings.SelectionIsRandom)
				Logic.Level.PlaySound(X, Y, "Selection");
			else
			{
				SharedGameSettings.SoundEffectInfo s;
				if (Settings.GetRandomSound("Selection", out s))
				{
					var clip = s.Clip;
					foreach (SharedGameSettings.GameTileInfo gti in Settings.Tiles)
					{
						if (gti.Name + "(Clone)" == name)
						{
							//var rando = Random.Range(0, gti.SoundEffect.Length);
							//clip = gti.SoundEffect[rando];
							break;
						}
					}
					Logic.Level.GridSlots[X, Y].audio.PlayOneShot(clip, s.VolumeAdd);
				}
			}
		}
	}

	private void AdjustSpecialHighlightVFX(bool getLit)
	{
		if (null == SpecialHighlightVFX) return;
		SpecialHighlightVFX.gameObject.SetActive(getLit);
	}

	public void SwapTiles(GameTile other, WaitForAll waiter)
	{
		Coords myPos = new Coords() { x = X, y = Y };
		Coords otherPos = new Coords() { x = other.X, y = other.Y };

		this.MoveBetweenSlots(myPos, otherPos, waiter);
		other.MoveBetweenSlots(otherPos, myPos, waiter);
	}

	public void MoveBetweenSlots(Coords oldPos, Coords newPos, WaitForAll waiter)
	{
		waiter.Lock();
		MoveWaiter = waiter;

		//set game state for when animation is done
		X = newPos.x;
		Y = newPos.y;
		transform.SetParent(Logic.Level.GridSlots[newPos.x, newPos.y].transform, true);

		//wraparound
		if (oldPos.x == 0 && newPos.x == Logic.Level.GridSizeX - 1)
			newPos.x = -1;
		if (newPos.x == 0 && oldPos.x == Logic.Level.GridSizeX - 1)
			oldPos.x = -1;

		//init animation variables
		VirtualPosition = new Vector2(oldPos.x, oldPos.y);
		EndPosition = new Vector2(newPos.x, newPos.y);
		MoveDirction = new Vector2();

		//set direction
		if (VirtualPosition.x < EndPosition.x)
			MoveDirction.x = 1;
		else if (VirtualPosition.x > EndPosition.x)
			MoveDirction.x = -1;
		else
			MoveDirction.x = 0;
		if (VirtualPosition.y < EndPosition.y)
			MoveDirction.y = 1;
		else if (VirtualPosition.y > EndPosition.y)
			MoveDirction.y = -1;
		else
			MoveDirction.y = 0;
	}

	public void FailedSwapAttempt()
	{
		ShakeTile();
		Logic.Level.PlaySound(X, Y, "SwipeFailed");
		var evt = new EventCSVLine(EventCSVLine.EventType.PlayerMove);
		evt.PlayerMoveType = EventCSVLine.MoveType.FailedSwap;
		TelemetryManager.LogEvent(evt);
	}

	public void ShakeTile()
	{
		ThisAnim.Play("GemShake");
	}

	private void CheckToClearSpecialHighlight()
	{
		DragThresholdHit = false;
		if (DragHighlightThresholdHit)
		{
			DragHighlightThresholdHit = false;
			if (null != specialHighlightTiles)
			{
				for (int i = 0; i < specialHighlightTiles.Count; i++)
					specialHighlightTiles[i].Highlight(false, false, true);
				specialHighlightTiles = null;
			}
		}
	}

	private bool IsDragging { get { return DragDistance != Vector3.zero; } }

	public void OnDrag(Vector3 delta)
	{
		if (DragThresholdHit && TypeOfBomb == BombType.NOT_A_BOMB) return;

		if(!IsDragging)
		{
			//GrabTileVFX.Play();
			//Logic.Level.PlaySound(X, Y, "GrabTile");
			ChangeScale(Settings.SizeCurveTileDragGrow);
		}

		DragDistance += delta;
		float horiz = Vector3.Dot(-transform.right, DragDistance);
		float vert = Vector3.Dot(transform.up, DragDistance);
		float absH = Mathf.Abs(horiz);
		float absV = Mathf.Abs(vert);

		//if (TypeOfBomb == BombType.HalfCross || TypeOfBomb == BombType.ConeBomb3x)
		//{
		//	Transform cannon = transform.GetChild(0);
		//	Vector3 direction = absH > absV ? Vector3.up : Vector3.right;
		//	float axis = absH > absV ? horiz : vert;
		//	float angle = Mathf.Min(Mathf.Max(axis / (Settings.SwipeDragMagnitude), -1), 1) * -90f;
		//	cannon.localRotation = Quaternion.AngleAxis(angle, direction) * CannonBaseRotation;
		//}
		if (absH > Settings.SwipeDragMagnitude || absV > Settings.SwipeDragMagnitude)
		{
			SwipeDirection dir;
			if (absH > absV)
			{
				if (horiz > 0) dir = SwipeDirection.Right;
				else dir = SwipeDirection.Left;
			}
			else if (vert > 0) dir = SwipeDirection.Up;
			else dir = SwipeDirection.Down;

			//if (IsBomb && !DragHighlightThresholdHit)
			if (IsBomb && dir != specialDir)
			{
				DragHighlightThresholdHit = true;

				if (null != specialHighlightTiles)
					for (int i = 0; i < specialHighlightTiles.Count; i++)
						specialHighlightTiles[i].Highlight(false, false, true);

				switch (TypeOfBomb)
				{
					case BombType.ColorBomb:
						bool failed = false;
						int x = X;
						int y = Y;
						switch (dir)
						{
							case SwipeDirection.Up: failed = Y == 0; y--; break;
							case SwipeDirection.Down: failed = (Y >= GameLevel.Instance.GridSizeY - 1); y++; break;
							case SwipeDirection.Left:
								x = (x == 0 ? GameLevel.Instance.GridSizeX - 1 : x - 1);
								break;
							case SwipeDirection.Right:
								x = (x + 1) % GameLevel.Instance.GridSizeX;
								break;
							default: break;
						}
						failed = failed || GameLevel.Instance.NullTileGrid.Get(x, y) || GameLevel.Instance.TempNullTileGrid.Get(x, y) || GameLevel.Instance.GetGridTile(x, y).IsBomb;
						var typ = GameLevel.Instance.GetGridTile(x, y).Type;
						/// no color-bombing pizza, may need to adjust for other collections
						if (failed || typ == 10) break;
						specialHighlightTiles = Logic.GetAllTilesOfType(typ);
						break;
					case BombType.ConeBomb3x:
						specialHighlightTiles = Logic.GetConeBlast(X, Y, dir);
						SetSpecialFacingDirection(dir, false);
						break;
					case BombType.FullCross:
						specialHighlightTiles = Logic.GetAllTilesInRow(Y);
						specialHighlightTiles.AddRange(Logic.GetVerticalRing(X));
						break;
					case BombType.HalfCross:
						if (dir == SwipeDirection.Left || dir == SwipeDirection.Right) specialHighlightTiles = Logic.GetAllTilesInRow(Y);
						else specialHighlightTiles = Logic.GetVerticalRing(X);
						SetSpecialFacingDirection(dir, false);
						break;
					case BombType.Neighbors5x5:
						specialHighlightTiles = Logic.GetNearbyTiles(this, 2);
						break;
					default: break;
				}

				if (null != specialHighlightTiles)
					for (int i = 0; i < specialHighlightTiles.Count; i++)
						specialHighlightTiles[i].Highlight(true, false, true);
			}
			//if (DragThresholdHit) return;
			DragThresholdHit = true;
			specialDir = dir;
			if (TypeOfBomb == BombType.NOT_A_BOMB) Logic.Swipe(this, dir, false, false);
		}
		else if (DragThresholdHit)
		{
			DragThresholdHit = false;
			CheckToClearSpecialHighlight();
		}
	}

	public void ExecuteDrag()
	{
		//if (!DragThresholdHit) return;
		//DragThresholdHit = false;
		CheckToClearSpecialHighlight();
		if (/*TypeOfBomb != BombType.NOT_A_BOMB &&*/ TypeOfBomb != BombType.ColorBomb && TypeOfBomb != BombType.UFO)
		{
			if (TypeOfBomb == BombType.BlasterAuto) ActivateSpecial(false, specialDir);
			else Logic.ActivateBomb(this, false, specialDir, -1, true);
			PlayerHUD.Instance.IncreaseMoveCounter();
			Logic.PlayerTookAction = true;
		}
		else Logic.Swipe(this, specialDir, false, false);
	}

	public bool OnDragEnd()
	{
		bool wasDrag = DragThresholdHit;
		DragThresholdHit = false;
		DragDistance = Vector3.zero;
		CheckToClearSpecialHighlight();
		if (IsBomb && wasDrag)
			ExecuteDrag();
		else if (!wasDrag && (TypeOfBomb == BombType.ConeBomb3x || TypeOfBomb == BombType.HalfCross))
			SetSpecialFacingDirection(SwipeDirection.Undefined, true);
		else if(!IsBomb || !wasDrag)
			ChangeScale(Settings.SizeCurveTileDragShrink);
		return wasDrag;
	}

	public bool IsDraggable() { return true; }
	public void OnTouchDrag(Vector2 delta) { }
	public void OnTouchDragEnd() { }
	public bool IsTouchDraggable() { return false; }
	public void OnPointerClick()
	{
		//if (IsBomb && !HasActivated)
		//{
		//	//if (DragThresholdHit) ExecuteDrag();
		//	//else
		//	//{
		//	//some bombs need direction
		//	if (TypeOfBomb == BombType.ColorBomb || TypeOfBomb == BombType.HalfCross ||
		//		TypeOfBomb == BombType.Cannon || TypeOfBomb == BombType.ConeBomb3x || TypeOfBomb == BombType.UFO)
		//	{
		//		FailedSwapAttempt();
		//		return;
		//	}

		//	PlayerHUD.Instance.IncreaseMoveCounter();
		//	if (TypeOfBomb == BombType.BlasterAuto) ActivateSpecial(false, SwipeDirection.Undefined, -1);
		//	else Logic.ActivateBomb(this, false, SwipeDirection.Undefined, -1, true);
		//	//}
		//}
	}
}
