﻿using UnityEngine;

[CreateAssetMenu(fileName = "SharedGameSettings", menuName = "Tetristar/Shared Game Settings")]
public class SharedGameSettings : ScriptableObject
{
	[System.Serializable]
	public struct GameTileInfo
	{
		public string Name;
		public bool isSpecial;
		public GameObject TilePrefab;
		public GameObject VFXPrefabOverride;
		public bool NoSwiping;
		public bool Immovable;
		public BombType BombType;
		//public AudioClip[] SoundEffect;
	}

	[System.Serializable]
	public struct SoundEffectInfo
	{
		public AudioClip Clip;
		public float VolumeAdd;
	}

	[System.Serializable]
	public struct SoundEffect
	{
		public string Name;
		public SoundEffectInfo[] Files;
	}

	public enum BoardMeshDisplay { HIDE, SHOW_ONE_SIDE, SHOW_DOUBLE_SIDED }

	[System.Serializable]
	public struct BoardMeshSettings
	{
		public bool HideAllMesh;
		public BoardMeshDisplay ShowGridFace;
		public BoardMeshDisplay ShowGridBorder;
		public BoardMeshDisplay ShowGridLines;
		public Material BoardMatFaces;
		public Material BoardMatBorder;
		public float GridBorderWidth;
		public float GridLineWidth;
		public bool FaceCoversNullTilesInstead;
		public float RadiusOffset;
		public int MeshDivisionsPerTile;
	}

	public LevelList LevelList;

	[Header("Player Settings")]
	public Vector3 StoryOffset;
	public Vector3 TutorialWindowOffset;
	public Vector3 TutorialWindowOffsetHigh;
	//public Vector3 SurveyOffset;
	//public Vector3 HUDOffset;
	//public Vector3 EndScreenOffset;

	[Header("Board Time Settings")]
	public float ComboDelayTime = 0.25f;
	public float BoardRefillDelay = 0.1f;
	public float GemSpawnInterval = 0.1f;
	public float TileRevealDelay = .01f;
	public float HintInterval = 10f;

	[Header("Null Obstacles")]
	public float ObstacleScale;

	[Header("Board Mesh Settings")]
	public BoardMeshSettings DefaultMeshSettings;

	[Header("Board Line Renderer Settings")]
	public bool ShowLineRenders = true;
	public Material LineMaterial;
	public Color LineColorStart = Color.white;
	public Color LineColorEnd = Color.white;
	public float LineWidth = 1f;

	[Header("Board Line Mesh Settings")]
	public bool ShowLineMesh;
	public GameObject LinePrefab;
	public float LineThickness;
	public float LineLength;

	[Header("Tile Settings")]
	public bool TilesStandUpStraight;
	public int PointsPerTile = 50;
	public Vector3 colliderSize;
	public int TileLayer = 8;
	public bool CannonIsBlockedByNullTiles = false;

	[Header("Tile Animations")]
	public AnimationClip[] TileAnimations;

	[Header("Tile Controls")]
	public float SwipeDragMagnitude;
	public float TouchDragThreshold = 0.5f;
	public float TouchDragMagnitude = 25f;
	public float HighlightSize = 1.5f;
	public float DragHighlightSize = 1.75f;
	public float SizeChangeTime = 0.1f;
	//public float InitSizeChangeTime = .5f;
	public float MoveSpeed;

	[Header("Special Tiles")]
	public float SpecialActivateTime = 0.75f;
	public float SpecialActivateChainTime = 0.5f;
	public float BombExplosionLength = 0.75f;
	public bool UFOLeavesTrail;

	[Header("VFX")]
	public GameObject SpecialHighlightVFX;
	public GameObject UFOHighlightVFX;
	public GameObject ObjectiveCompleteTracerVFX;
	public GameObject SpecialRangeHighlightVFX;
	public GameObject GrabTileVFX;
	public GameObject NormalComboVFX;
	public GameObject NormalExplosionVFX;
	public GameObject InitTileSpawnFX;
	public GameObject UFOLandVFX;
	public Gradient PointerColorNormal;
	public Gradient PointerColorMatch;

	[Header("Sprites")]
	public Sprite ActiveMenuButtonSprite;
	public Sprite InactiveMenuButtonSprite;

	[Header("Fanfare Settings")]
	public float HUDFanfareResizeTime = 1f;
	public float HUDFanfarePosition = -35f;
	public float HUDFanfareScale = .025f;

	[Header("Tutorial Settings")]
	public int HelpMatchThreshold = 8;

	[Header("Debug")]
	public bool HideAllVFX;
	public bool DoSurvey;

#if UNITY_EDITOR
	[Header("Swap Tiles")]
	public GameObject SwapUIPrefab;
	public float SwapUIDistance;
	public Color SwapSpecialTint;
#endif

	[Header("Hazard Layers")]
	public float HazardLayerScale = 0.85f;

	[Header("Animation Curves")]
	public AnimationCurve SizeCurveTooltipGrow;
	public AnimationCurve SizeCurveTooltipShrink;
	public AnimationCurve SizeCurveButtonGrow;
	public AnimationCurve SizeCurveButtonShrink;
	public AnimationCurve SizeCurveHUD;
	public AnimationCurve SizeCurveTileHighlightGrow;
	public AnimationCurve SizeCurveTileHighlightShrink;
	public AnimationCurve SizeCurveTileDragGrow;
	public AnimationCurve SizeCurveTileDragShrink;

	[Header("Tile Prefabs")]
	public GameObject ScoreCardPrefab;
	public GameTileInfo[] Tiles;

	[Header("Editor Settings")]
	public Color GridColor = Color.white;
	public Color NullColor = Color.white;
	public Texture2D SplineTexture;

	[Header("Sounds")]
	public float DefaultSoundVolume = 1;
	public Vector2 MinMaxSoundDistance;
	public bool SelectionIsRandom;
	public SoundEffect[] Sounds;
	public int[] CascadeChordStartingPoints;
	public AudioClip[] LevelSongsIntro;
	public AudioClip[] LevelSongsLoop;

	public bool GetRandomSound(string Name, out SoundEffectInfo info)
	{
		foreach (SoundEffect e in Sounds)
		{
			if (e.Name.ToLower().Equals(Name.ToLower()))
			{
				int len = e.Files.Length;
				if (len == 0)
				{
					info = new SoundEffectInfo();
					return false;
				}
				else
				{
					info = e.Files[Random.Range(0, len)];
					info.VolumeAdd += DefaultSoundVolume;
					return true;
				}
			}
		}
		info = new SoundEffectInfo();
		return false;
	}
}