﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenderSelectionButton : ButtonTemplate, VRInteractable
{
	[Header("Gender Settings")]
	[SerializeField] private bool isMale;

	[Header("Gender References")]
	[SerializeField] private WordBubbleScaler rings;
	[SerializeField] private Transform ringSpot;
	[SerializeField] private AudioClip sound;
	private IEnumerator ringsCoro;

	public bool IsDraggable() { return false; }
	public bool IsTouchDraggable() { return false; }
	public void OnDrag(Vector3 delta) { }
	public bool OnDragEnd() { return false; }
	public void OnTouchDrag(Vector2 delta) { }
	public void OnTouchDragEnd() { }

	private void Start()
	{
		StartCoroutine(WaitingToSetup());
	}

	private IEnumerator WaitingToSetup()
	{
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		if (!isMale && !SaveLoadManager.HeroIsMale)
		{
			rings.transform.SetParent(scalePivot);
			rings.transform.position = ringSpot.position;
		}
	}

	public void OnPointerClick()
	{
		PlaySound("ButtonClick");
		//thisAudioSource.PlayOneShot(sound, thisAudioSource.volume * SaveLoadManager.VolumeSFX());
		if (SaveLoadManager.HeroIsMale == isMale) return;
		SaveLoadManager.HeroIsMale = isMale;
		//save data
		var val = isMale ? 100 : 0;
		var data = new SaveDataCSV(SaveLoadManager.OptionsKeys[6], val);
		SaveLoadManager.SaveOptionsData(data);
		//do rings
		if (null != ringsCoro) StopCoroutine(ringsCoro);
		ringsCoro = WaitingForRings();
		StartCoroutine(ringsCoro);
	}

	public void OnPointerEnter()
	{
		if (MenuScreen.IsPaused) return;
		Highlight(true, scalePivot);
	}

	public void OnPointerExit()
	{
		if (!isLit && MenuScreen.IsPaused) return;
		Highlight(false, scalePivot);
	}

	private IEnumerator WaitingForRings()
	{
		rings.ChangeState(false);
		yield return new WaitForSeconds(rings.ScaleTime);
		rings.transform.SetParent(scalePivot);
		rings.transform.position = ringSpot.position;
		rings.gameObject.SetActive(true);
		rings.ChangeState(true);
	}
}