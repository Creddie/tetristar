﻿using System.Collections;
using UnityEngine;

public class ButtonTemplate : MonoBehaviour
{
	[Header("Button Settings")]
	[SerializeField] protected SharedGameSettings settings;
	[SerializeField] protected float changeTime = .1f;
	[SerializeField] protected float volAdd;

	[Header("Button References")]
	[SerializeField] protected Transform scalePivot;

	[HideInInspector] public bool isLit;

	protected float timer;
	protected IEnumerator scaleCoro;
	protected Vector3 normalScale;
	protected AudioSource thisAudioSource;
	private bool isHUD;
	private bool canPlaySound;

	protected virtual void Awake()
	{
		canPlaySound = true;
		thisAudioSource = GetComponent<AudioSource>();
		if (null == thisAudioSource) thisAudioSource = GetComponentInParent<AudioSource>();
		isHUD = GetComponentInParent<PlayerHUD>() ? true : false;
		if (null == scalePivot) scalePivot = transform;
		normalScale = scalePivot.localScale;
	}

	public virtual void Highlight(bool getLit, Transform toChange)
	{
		isLit = getLit;
		if (null != scaleCoro) StopCoroutine(scaleCoro);
		scaleCoro = ChangingSize(toChange);
		StartCoroutine(scaleCoro);
		//if (OVRScreenFade.IsFading) return;
		if (isLit)
		{
			PlaySound("ButtonSelect");
			PlayerHand.Instance.ChangePointerColor(false);
		}
		else PlayerHand.Instance.ChangePointerColor(true);
	}

	private IEnumerator ChangingSize(Transform toChange)
	{
		timer = 0f;
		var curve = isLit ? settings.SizeCurveButtonGrow : settings.SizeCurveButtonShrink;
		curve = isHUD ? settings.SizeCurveHUD : curve;
		while (timer < changeTime)
		{
			timer += Time.unscaledDeltaTime;
			toChange.transform.localScale = curve.Evaluate(timer / changeTime) * normalScale;
			yield return new WaitForEndOfFrame();
		}
	}

	public void PlaySound(string name)
	{
		if (null == thisAudioSource || !canPlaySound) return;
		StartCoroutine(SoundCooldown());
		SharedGameSettings.SoundEffectInfo s;
		if (settings.GetRandomSound(name, out s))
			thisAudioSource.PlayOneShot(s.Clip, (s.VolumeAdd + volAdd) * SaveLoadManager.VolumeSFX());
	}

	private IEnumerator SoundCooldown()
	{
		canPlaySound = false;
		yield return new WaitForSecondsRealtime(.01f);
		canPlaySound = true;
	}
}