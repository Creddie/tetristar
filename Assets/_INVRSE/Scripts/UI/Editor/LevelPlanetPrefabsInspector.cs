﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(LevelPlanetPrefabs))]
public class LevelPlanetPrefabsInspector : Editor
{
	private ReorderableList planetPrefabList;

	private void OnEnable()
	{
		planetPrefabList = new ReorderableList(serializedObject, serializedObject.FindProperty("PlanetPrefabs"), true, true, true, true);
		planetPrefabList.drawHeaderCallback = (Rect rect) =>
		{
			EditorGUI.LabelField(rect, "Planet Prefabs");
		};
		planetPrefabList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
		{
			var element = planetPrefabList.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			EditorGUI.ObjectField(
				new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
				element, GUIContent.none
			);
		};
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.BeginVertical();
		planetPrefabList.DoLayoutList();
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();
		serializedObject.ApplyModifiedProperties();
	}
}