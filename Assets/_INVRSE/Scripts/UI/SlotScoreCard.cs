﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SlotScoreCard : MonoBehaviour
{
	[SerializeField]
	private float moveTime;
	[SerializeField]
	private float moveDistance;
	[SerializeField]
	private float offset;

	private float timer;
	private TextMeshProUGUI thisText;
	private Vector3 originPoint;
	private Vector3 targetVec;

	[SerializeField] private AnimationCurve sizeCurve;
	private Vector3 normalScale;

	public SpawnPools.DespawnScoreCallback Despawn;

	private void Awake()
	{
		thisText = GetComponentInChildren<TextMeshProUGUI>();
		originPoint = transform.localPosition;
		originPoint = new Vector3(originPoint.x, originPoint.y, originPoint.z + offset);
		targetVec = originPoint + Vector3.forward * moveDistance;
		normalScale = transform.localScale;
	}

	private void OnDisable()
	{
		transform.localPosition = originPoint;
		transform.localScale = normalScale;
	}

	public void UpdateScore(int score)
	{
		thisText.SetText(score.ToString());
		timer = 0;
		StartCoroutine(Moving());
		Scoreboard.Instance.UpdateScore(score);
	}

	private IEnumerator Moving()
	{
		while(timer < moveTime)
		{
			transform.localScale = sizeCurve.Evaluate(timer / moveTime) * normalScale;
			transform.localPosition = Vector3.Lerp(originPoint, targetVec, timer / moveTime);
			timer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		Despawn(this);
	}
}