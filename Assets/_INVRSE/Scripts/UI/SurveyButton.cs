﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SurveyButton : ButtonTemplate, VRInteractable
{
	private Image thisImage;

	public void OnDrag(Vector3 delta) { }
	public bool OnDragEnd() { return false; }
	public bool IsDraggable() { return false; }
	public void OnTouchDrag(Vector2 delta) { }
	public void OnTouchDragEnd() { }
	public bool IsTouchDraggable() { return false; }

	protected override void Awake()
	{
		base.Awake();
		thisImage = GetComponent<Image>();
	}

	public void OnPointerEnter()
	{
		if (EndLevelSurvey.HasChosen)
			return;
		Highlight(true, scalePivot);
		EndLevelSurvey.Instance.UpdateStars(thisImage, true, false);
	}

	public void OnPointerExit()
	{
		if (EndLevelSurvey.HasChosen && !isLit)
			return;
		Highlight(false, scalePivot);
		EndLevelSurvey.Instance.UpdateStars(thisImage, false, false);
	}

	public void OnPointerClick()
	{
		EndLevelSurvey.Instance.UpdateStars(thisImage, true, true);
	}
}