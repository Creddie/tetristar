﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum MenuSliderType
{
	VolumeOverall,
	VolumeMusic,
	VolumeSFX
}

public class MenuSlider : ButtonTemplate, VRInteractable
{
	[Header("Slider")]
	[SerializeField] private Vector2 minMaxRange;
	[SerializeField] private MenuSliderType sliderType;
	[SerializeField] private Image fillImage;
	[SerializeField] private float scaleFactor = .25f;
	[SerializeField] private TextMeshProUGUI textComp;
	private float currentValue;
	private Vector3 lastPos;
	private float lastValue;
	private bool didDrag;

	private void Start()
	{
		///let data load before accessing
		StartCoroutine(WaitingToStart());
	}

	private IEnumerator WaitingToStart()
	{
		yield return new WaitForSecondsRealtime(.1f);
		switch (sliderType)
		{
			case MenuSliderType.VolumeOverall:
				currentValue = SaveLoadManager.VolumeOverall();
				AudioListener.volume = currentValue;
				break;
			case MenuSliderType.VolumeMusic:
				currentValue = SaveLoadManager.VolumeMusic();
				MusicManager.Instance.ThisAudioSource.volume = MusicManager.Instance.volOrig * currentValue;
				break;
			case MenuSliderType.VolumeSFX:
				currentValue = SaveLoadManager.VolumeSFX();
				settings.DefaultSoundVolume = currentValue;
				break;
			default: Debug.Log("ERROR: improper slider type"); break;
		}
		fillImage.fillAmount = (currentValue - minMaxRange.x) / (minMaxRange.y - minMaxRange.x);
		//textComp.SetText(currentValue.ToString());
	}

	#region Interactable Interface
	public void OnDrag(Vector3 delta)
	{
		if (0f == lastPos.magnitude) lastPos = delta;
		var change = delta.y - lastPos.y;
		if (Mathf.Abs(change) >= .1f) didDrag = true;
		else return;
		//lastPos = delta;
		//didDrag = true;
		currentValue += change * scaleFactor;
		currentValue = Mathf.Max(minMaxRange.x, currentValue);
		currentValue = Mathf.Min(minMaxRange.y, currentValue);
		ExecuteValueChange();
		//textComp.SetText(currentValue.ToString());
	}

	private void ExecuteValueChange()
	{
		fillImage.fillAmount = (currentValue - minMaxRange.x) / (minMaxRange.y - minMaxRange.x);
		var index = 0;
		switch (sliderType)
		{
			case MenuSliderType.VolumeOverall:
				index = 3;
				AudioListener.volume = currentValue;
				break;
			case MenuSliderType.VolumeMusic:
				index = 4;
				MusicManager.Instance.ThisAudioSource.volume = MusicManager.Instance.volOrig * currentValue;
				break;
			case MenuSliderType.VolumeSFX:
				index = 5;
				settings.DefaultSoundVolume = currentValue;
				break;
			default: Debug.Log("ERROR: improper slider type"); break;
		}
		var key = SaveLoadManager.OptionsKeys[index];
		var data = new SaveDataCSV(key, Mathf.RoundToInt(currentValue * 100));
		SaveLoadManager.SaveOptionsData(data);
	}

	public void OnPointerEnter()
	{
		Highlight(true, scalePivot);
	}

	public void OnPointerExit()
	{
		Highlight(false, scalePivot);
	}

	public bool OnDragEnd()
	{
		lastPos = Vector3.zero;
		return false;
	}

	public void OnPointerClick()
	{
		if(didDrag)
		{
			didDrag = false;
			return;
		}
		PlaySound("ButtonClick");
		if (currentValue > 0f) lastValue = currentValue;
		currentValue = currentValue == 0f ? lastValue : 0f;
		ExecuteValueChange();
	}

	public bool IsDraggable() { return true; }
	public bool IsTouchDraggable() { return false; }
	public void OnTouchDrag(Vector2 delta) { }
	public void OnTouchDragEnd() { }
	#endregion
}