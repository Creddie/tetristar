﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum MenuButtonTypes
{
	Resume,
	Options,
	Quit,
	Restart,
	ExitOptions,
	ExitOptionsAudio,
	InvertControls,
	SafetyMode,
	StargazerMode,
	SoundMenu,
	NextLevel,
	LevelSelect,
	NewGame,
	CancelConfirm
}

public class MenuScreen : MonoBehaviour
{
	public static MenuScreen Instance;
	public static bool IsPaused;
	public static bool AcceptsInput;

	[SerializeField] private SharedGameSettings settings;

	[SerializeField] private GameObject debugHUD;
	[SerializeField] protected ScreenFader mainCanvas;
	[SerializeField] private ScreenFader optionsCanvas;
	[SerializeField] private ScreenFader optionsAudioCanvas;
	[SerializeField] protected Collider[] buttonCollidersMain;
	[SerializeField] private Collider[] buttonCollidersOptions;
	[SerializeField] private Collider[] buttonCollidersAudio;

	private MenuButtonTypes activeScreenType;
	private StargazerPivot[] stargazers;

	private void Awake()
	{
		Instance = this;
		AcceptsInput = true;
		IsPaused = false;
		activeScreenType = MenuButtonTypes.Resume;
	}

	private void Start()
	{
		//transform.position = Player.Instance.transform.position + settings.EndScreenOffset;
		if (!Player.Instance.ShowFramerate && null != debugHUD) debugHUD.SetActive(false);
	}

	private void Update()
	{
		if (AcceptsInput) InputHandler();
	}

	private void InputHandler()
	{
		if (OVRInput.GetUp(OVRInput.Button.Back) ||
			(!OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) && 
			((OVRInput.GetUp(OVRInput.RawButton.B) && !PlayerHandManager.IsLeftHanded) || (OVRInput.GetUp(OVRInput.RawButton.Y) && PlayerHandManager.IsLeftHanded))))
			ChangePauseMenuState();
#if UNITY_EDITOR
		else if (Input.GetKeyUp(KeyCode.T)) ChangePauseMenuState();
#endif
	}

	public virtual void ChangePauseMenuState()
	{
		switch (activeScreenType)
		{
			case MenuButtonTypes.Resume:
				IsPaused = !IsPaused;
				mainCanvas.FadeScreen(IsPaused, null, buttonCollidersMain);
				Time.timeScale = IsPaused ? 0f : 1f;
				break;
			case MenuButtonTypes.Options: ExecuteClickFunction(MenuButtonTypes.ExitOptions); break;
			case MenuButtonTypes.SoundMenu: ExecuteClickFunction(MenuButtonTypes.ExitOptionsAudio); break;
			default: Debug.Log("ERROR: incorrect button type"); return;
		}
	}

	public virtual void LoadScene(string sceneName)
	{
		StartCoroutine(LoadingScene(sceneName));
	}

	private IEnumerator LoadingScene(string sceneName)
	{
		AcceptsInput = false;
		//OVRScreenFade.Instance.FadeOut();
		Player.Instance.FadeEverything(false);
		yield return new WaitForSecondsRealtime(OVRScreenFade.Instance.fadeTime);
		Time.timeScale = 1f;
		IsPaused = false;
		SceneManager.LoadSceneAsync(sceneName);
	}

	public void ExecuteClickFunction(MenuButtonTypes value)
	{
		var val = 0;
		var data = new SaveDataCSV("", 0);
		switch (value)
		{
			case MenuButtonTypes.Resume: ChangePauseMenuState(); break;
			case MenuButtonTypes.Restart: LoadScene(SceneManager.GetActiveScene().name); break;
			case MenuButtonTypes.Options:
				activeScreenType = value;
				mainCanvas.FadeScreen(false, null, buttonCollidersMain);
				optionsCanvas.FadeScreen(true, null, buttonCollidersOptions);
				break;
			case MenuButtonTypes.Quit:
				var nam = "StartScene";
				if (SceneManager.GetActiveScene().name == nam) Application.Quit();
				else LoadScene(nam);
				break;
			case MenuButtonTypes.ExitOptions:
				activeScreenType = MenuButtonTypes.Resume;
				optionsCanvas.FadeScreen(false, null, buttonCollidersOptions);
				mainCanvas.FadeScreen(true, null, buttonCollidersMain);
				break;
			case MenuButtonTypes.InvertControls:
				val = SaveLoadManager.InvertedControls() ? 0 : 1;
				data = new SaveDataCSV(SaveLoadManager.OptionsKeys[2], val);
				SaveLoadManager.SaveOptionsData(data);
				break;
			case MenuButtonTypes.SafetyMode:
				val = SaveLoadManager.SafetyMode() ? 0 : 1;
				data = new SaveDataCSV(SaveLoadManager.OptionsKeys[0], val);
				SaveLoadManager.SaveOptionsData(data);
				break;
			case MenuButtonTypes.SoundMenu:
				activeScreenType = value;
				optionsCanvas.FadeScreen(false, null, buttonCollidersOptions);
				optionsAudioCanvas.FadeScreen(true, null, buttonCollidersAudio);
				break;
			case MenuButtonTypes.StargazerMode:
				val = SaveLoadManager.StargazerMode() ? 0 : 1;
				data = new SaveDataCSV(SaveLoadManager.OptionsKeys[1], val);
				SaveLoadManager.SaveOptionsData(data);
				StartCoroutine(Stargazing());
				break;
			case MenuButtonTypes.ExitOptionsAudio:
				activeScreenType = MenuButtonTypes.Options;
				optionsAudioCanvas.FadeScreen(false, null, buttonCollidersAudio);
				optionsCanvas.FadeScreen(true, null, buttonCollidersOptions);
				break;
			case MenuButtonTypes.NextLevel:
				var cutscene = FindObjectOfType<CutSceneIntroManager>();
				if (null != cutscene) LoadScene(cutscene.levels.GetNextLevel());
				else if (GameLevel.Instance.Settings.DoSurvey) EndScreen.Instance.LoadSurvey();
				else EndScreen.Instance.LoadLevel();
				break;
			case MenuButtonTypes.LevelSelect: LoadScene("MapScene"); break;
			case MenuButtonTypes.CancelConfirm:
				StartGameButton.LevelSelected = false;
				StartGameButton.Instance.ConfirmScreen.FadeScreen(false, null, StartGameButton.Instance.ConfirmButtons);
				break;
			case MenuButtonTypes.NewGame:
				StartGameButton.Instance.ConfirmScreen.FadeScreen(false, null, StartGameButton.Instance.ConfirmButtons, true);
				StartGameButton.Instance.StartGame();
				break;
			default: Debug.Log("ERROR: incorrect button type"); return;
		}
	}

	private IEnumerator Stargazing()
	{
		//OVRScreenFade.Instance.FadeOut();
		Player.Instance.FadeEverything(false);
		yield return new WaitForSecondsRealtime(OVRScreenFade.Instance.fadeTime);
		if (null == stargazers) stargazers = FindObjectsOfType<StargazerPivot>();
		foreach (var v in stargazers) v.ChangeState(SaveLoadManager.StargazerMode());
		SetPlayerStargazing(false);
		//OVRScreenFade.Instance.FadeIn();
		Player.Instance.FadeEverything(true);
	}

	public static void SetPlayerStargazing(bool setup)
	{
		var playa = Player.Instance.transform;
		var y = playa.localPosition.y;
		var z = playa.localPosition.z;
		if (setup) y = z;
		var newPos = SaveLoadManager.StargazerMode() ? new Vector3(0f, z, 0f) : new Vector3(0f ,0f, y);
		playa.localPosition = newPos;
		Player.Instance.PivotUI.localPosition = newPos;
		//OVRManager.display.RecenterPose();
		if (null != PlayerSpinControls.Instance) PlayerSpinControls.Instance.SetSpinRotation();
	}
}