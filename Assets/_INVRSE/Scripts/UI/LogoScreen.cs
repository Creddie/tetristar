﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogoScreen : MonoBehaviour
{
	public static LogoScreen Instance;

	[SerializeField] private float logoDuration = 3f;
	[SerializeField] private GameObject gameSceneObj;
	private ScreenFader faderComp;
	private Image imageComp;
	[SerializeField] AudioClip themeSongIntro;
	[SerializeField] AudioClip themeSongLoop;
	[SerializeField] private Sprite[] logos;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		if (null != GameObject.Find("LOGO_DONE"))
		{
			MusicManager.Instance.FadeMusic(true, themeSongIntro);
			MusicManager.Instance.SetupNextSong(themeSongIntro.length, themeSongLoop);
			Finish();
			return;
		}
		MenuScreen.AcceptsInput = false;
		faderComp = GetComponent<ScreenFader>();
		imageComp = GetComponentInChildren<Image>();
		gameSceneObj.SetActive(false);
		StartCoroutine(LogoProgression());
	}

	private IEnumerator LogoProgression()
	{
		yield return new WaitForSecondsRealtime(SceneLoading.Instance.LoadingTime + OVRScreenFade.Instance.fadeTime);
		var spawned = new GameObject();
		DontDestroyOnLoad(spawned);
		spawned.name = "LOGO_DONE";
		for (int i = 0; i < logos.Length; i++)
		{
			imageComp.sprite = logos[i];
			faderComp.FadeScreen(true, null, null);
			yield return new WaitForSecondsRealtime(faderComp.fadeTime + logoDuration);
			faderComp.FadeScreen(false, null, null);
			yield return new WaitForSecondsRealtime(faderComp.fadeTime);
		}
		Player.Instance.FadeEverything(false);
		yield return new WaitForSecondsRealtime(OVRScreenFade.Instance.fadeTime);
		gameSceneObj.SetActive(true);
		Player.Instance.FadeEverything(true);
		yield return new WaitForSecondsRealtime(OVRScreenFade.Instance.fadeTime);
		Finish();
	}

	private void Finish()
	{
		MenuScreen.AcceptsInput = true;
		gameObject.SetActive(false);
	}
}