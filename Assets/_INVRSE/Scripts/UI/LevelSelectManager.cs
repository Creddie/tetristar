﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectManager : MonoBehaviour
{
	public static LevelSelectManager Instance;
	public static bool LevelSelected;

	private List<LevelSelectButton> buttons = new List<LevelSelectButton>();
	[SerializeField] private AudioClip menuIntro;
	[SerializeField] private AudioClip menuLoop;
	public LevelList levels;
	public LevelPlanetPrefabs PlanetPrefabs;

	private void Awake()
	{
		Instance = this;
		LevelSelected = false;
	}

	private void Start()
	{
		//SaveLoadManager.Init(levels);
		MusicManager.Instance.FadeMusic(true, menuIntro);
		MusicManager.Instance.SetupNextSong(menuIntro.length, menuLoop);

		for (int i = 0; i < transform.childCount; i++)
		{
			var button = transform.GetChild(i).GetComponent<LevelSelectButton>();
			buttons.Add(button);
			button.ID = i + 1;
			button.SetVisuals();
		}
		enabled = false;
	}
}