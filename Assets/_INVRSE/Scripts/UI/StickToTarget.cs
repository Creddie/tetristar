﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickToTarget : MonoBehaviour
{
	[SerializeField] private Transform target;

	void FixedUpdate ()
	{
		transform.position = target.position;
	}
}