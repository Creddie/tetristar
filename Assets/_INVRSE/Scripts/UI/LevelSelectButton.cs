﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectButton : ButtonTemplate, VRInteractable
{
	[Header("Level Select Settings")]
	[HideInInspector] public int ID;
	[SerializeField] private TextMeshPro textComp;
	[SerializeField] private SpriteRenderer lockedUnlockedSprite;
	[SerializeField] private Sprite unlockedSprite;
	[SerializeField] private SpriteRenderer[] stars;
	private int levelNameID;
	[SerializeField] private bool cutscene;

	private void Start()
	{
		if (cutscene) StartCoroutine(WaitingToLoad());
	}

	private IEnumerator WaitingToLoad()
	{
		yield return new WaitForEndOfFrame();
		var wordInt = 0;
		int.TryParse(SaveLoadManager.LevelSaveData[2].StarsEarned, out wordInt);
		if (wordInt > 0) lockedUnlockedSprite.sprite = unlockedSprite;
		else GetComponent<Collider>().enabled = false;
		levelNameID = 5;
		textComp.SetText("");
		foreach (var i in stars) i.gameObject.SetActive(false);
	}

	private void Highlight(bool getLit)
	{
		Highlight(getLit, scalePivot);
	}

	private void LevelSelected()
	{
		LevelSelectManager.LevelSelected = true;
		Highlight(false);
		StartCoroutine(LoadingGame());
	}

	public void SetVisuals()
	{
		if (ID >= settings.LevelList.LevelNames.Length - 1)
		{
			Debug.Log("ERROR: More buttons than levels.");
			return;
		}
		levelNameID = ID < 4 ? ID + 1 : ID + 2; //skip over cutscene
		///text
		textComp.SetText(ID.ToString());
		///planet
		//var toSpawn = LevelSelectManager.Instance.PlanetPrefabs.PlanetPrefabs[ID];
		//var spawned = Instantiate(toSpawn, scalePivot) as GameObject;
		//spawned.transform.localPosition = Vector3.zero;
		//spawned.transform.localRotation = Quaternion.identity;
		//if (cutscene)
		//{
		//	var wordInt = 0;
		//	int.TryParse(SaveLoadManager.LevelSaveData[2].StarsEarned, out wordInt);
		//	if (wordInt > 0) lockedUnlockedSprite.sprite = unlockedSprite;
		//	else GetComponent<Collider>().enabled = false;
		//	foreach (var i in stars) i.gameObject.SetActive(false);
		//	return;
		//}
		///stars
		for (int i = 0; i < SaveLoadManager.LevelSaveData.Count; i++)
		{
			var key = "highscore_" + settings.LevelList.LevelNames[levelNameID];
			if (key == SaveLoadManager.LevelSaveData[i].LevelKey)
			{
				var wordInt = 0;
				int.TryParse(SaveLoadManager.LevelSaveData[i].StarsEarned, out wordInt);
				if (wordInt > 0) lockedUnlockedSprite.sprite = unlockedSprite;
				///find first locked level after last unlocked level
				else if(i > 0)
				{
					var offset = ID == 4 ? 2 : 1;	///skip over cutscene
					key = "highscore_" + settings.LevelList.LevelNames[levelNameID - offset];
					if (key == SaveLoadManager.LevelSaveData[i - 1].LevelKey)
					{
						wordInt = 0;
						int.TryParse(SaveLoadManager.LevelSaveData[i - 1].StarsEarned, out wordInt);
						if(wordInt == 0) GetComponent<Collider>().enabled = false;
						else lockedUnlockedSprite.sprite = unlockedSprite;
						//if (wordInt != 0) lockedUnlockedSprite.sprite = unlockedSprite;
						break;
					}
				}
				else lockedUnlockedSprite.sprite = unlockedSprite;
				for (int j = 0; j < wordInt; j++) stars[j].sprite = settings.ActiveMenuButtonSprite;
				break;
			}
		}
	}

	private IEnumerator LoadingGame()
	{
		Player.Instance.FadeEverything(false);
		yield return new WaitForSeconds(OVRScreenFade.Instance.fadeTime);
		SceneManager.LoadSceneAsync(LevelSelectManager.Instance.levels.LevelNames[levelNameID]);
	}

	#region Interface Interactable
	public void OnPointerEnter()
	{
		if (LevelSelectManager.LevelSelected || MenuScreen.IsPaused) return;
		Highlight(true);
	}

	public void OnPointerExit()
	{
		if (!gameObject.activeSelf || LevelSelectManager.LevelSelected || (!isLit && MenuScreen.IsPaused)) return;
		Highlight(false);
	}

	public void OnPointerClick()
	{
		if (LevelSelectManager.LevelSelected || MenuScreen.IsPaused) return;
		PlaySound("ButtonClick");
		LevelSelected();
	}

	public void OnDrag(Vector3 delta) { }
	public bool OnDragEnd() { return false; }
	public bool IsDraggable() { return false; }
	public void OnTouchDrag(Vector2 delta) { }
	public void OnTouchDragEnd() { }
	public bool IsTouchDraggable() { return false; }
	#endregion
}