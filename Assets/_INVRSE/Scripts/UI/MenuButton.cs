﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButton : ButtonTemplate, VRInteractable
{
	[Header("Menu Settings")]
	[SerializeField] private MenuButtonTypes buttonType;
	private Image thisImage;
	private bool canUse;

	protected override void Awake()
	{
		base.Awake();
		canUse = true;
		thisImage = GetComponentInChildren<Image>();
		StartCoroutine(WaitingToLoad());
	}

	private IEnumerator WaitingToLoad()
	{
		yield return new WaitForSeconds(.1f);
		UpdateSprite(true);
	}

	private void UpdateSprite(bool setup)
	{
		var val = false;
		switch (buttonType)
		{
			case MenuButtonTypes.InvertControls:
				val = SaveLoadManager.InvertedControls();
				break;
			case MenuButtonTypes.SafetyMode:
				val = SaveLoadManager.SafetyMode();
				break;
			case MenuButtonTypes.StargazerMode:
				val = SaveLoadManager.StargazerMode();
				break;
			default: if(!setup) Highlight(false, scalePivot); return;
		}
		thisImage.sprite = val ? settings.ActiveMenuButtonSprite : settings.InactiveMenuButtonSprite;
	}

	#region Interactable Interface
	public void OnPointerEnter()
	{
		if (!gameObject.activeSelf || !canUse) return;
		Highlight(true, scalePivot);
	}

	public void OnPointerExit()
	{
		if(canUse) Highlight(false, scalePivot);
	}

	public void OnPointerClick()
	{
		if (!canUse) return;
		MenuScreen.Instance.ExecuteClickFunction(buttonType);
		UpdateSprite(false);
		var soundName = "ButtonClick";
		if (buttonType == MenuButtonTypes.NextLevel || buttonType == MenuButtonTypes.LevelSelect || buttonType == MenuButtonTypes.Quit || buttonType == MenuButtonTypes.Restart || buttonType == MenuButtonTypes.NewGame)
		{
			canUse = false;
			soundName = "ChangeScene";
		}
		PlaySound(soundName);
	}

	public void OnDrag(Vector3 delta) { }
	public bool OnDragEnd() { return false; }
	public bool IsDraggable() { return false; }
	public void OnTouchDrag(Vector2 delta) { }
	public void OnTouchDragEnd() { }
	public bool IsTouchDraggable() { return false; }
	#endregion
}