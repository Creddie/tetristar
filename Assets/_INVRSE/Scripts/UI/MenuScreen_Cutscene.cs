﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScreen_Cutscene : MenuScreen
{
	public override void ChangePauseMenuState()
	{
		IsPaused = !IsPaused;
		mainCanvas.FadeScreen(IsPaused, null, buttonCollidersMain);
	}

	public override void LoadScene(string sceneName)
	{
		base.LoadScene(sceneName);
	}
}