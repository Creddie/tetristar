﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
	[SerializeField] private bool onlyOnce;
	[SerializeField] private bool reverse;
	private Vector3 target;
	//private Vector3 stargazerTarget;

	private void Awake()
	{
		//if (SaveLoadManager.StargazerMode()) reverse = !reverse;
		target = reverse ? -Player.Instance.transform.position : Player.Instance.transform.position;
		//stargazerTarget = new Vector3(target.x, target.y, target.z);
		//StartCoroutine(WaitingToStop());
		if (onlyOnce)
		{
			transform.LookAt(target);
			enabled = false;
		}
	}

	//private IEnumerator WaitingToStop()
	//{
	//	yield return new WaitForEndOfFrame();
	//	yield return new WaitForEndOfFrame();
	//	if (onlyOnce)
	//	{
	//		transform.LookAt(target);
	//		enabled = false;
	//	}
	//}

	private void Update()
	{
		//var targ = SaveLoadManager.StargazerMode() ? stargazerTarget : target;
		var dir = SaveLoadManager.StargazerMode() ? Vector3.back : Vector3.up;
		transform.LookAt(target, dir);
	}
}