﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartButton : MonoBehaviour, VRInteractable
{
	public void OnDrag(Vector3 delta) { }
	public bool OnDragEnd() { return false; }
	public bool IsDraggable() { return false; }
	public void OnPointerExit() { }
	public void OnPointerEnter() { }
	public void OnTouchDrag(Vector2 delta) { }
	public void OnTouchDragEnd() { }
	public bool IsTouchDraggable() { return false; }

	[SerializeField] private bool skipsLevel;
	[SerializeField] private bool goToLevelSelect;

	public void OnPointerClick()
	{
		if (skipsLevel) SceneManager.LoadSceneAsync(GameLevel.Instance.Settings.LevelList.GetNextLevel());
		else if (goToLevelSelect) SceneManager.LoadSceneAsync("MapScene");
		else SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
	}
}