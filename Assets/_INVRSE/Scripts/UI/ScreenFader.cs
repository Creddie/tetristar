﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class ScreenFader : MonoBehaviour
{
	private float timer;
	public float fadeTime = .1f;
	private CanvasGroup faderComp;
	[SerializeField] private bool startsActive;
	private IEnumerator fadingCoro;

	private void Awake()
	{
		faderComp = GetComponent<CanvasGroup>();
		if(!startsActive) faderComp.alpha = 0f;
	}

	private void Start() { }

	public void FadeScreen(bool reveal, GameObject obj, Collider[] components, bool justColliders = false)
	{
		if(justColliders)
		{
			if (null != components)
				for (int i = 0; i < components.Length; i++)
					components[i].enabled = reveal;
			if (null != obj) obj.SetActive(reveal);
			return;
		}

		timer = 0f;
		if (null != fadingCoro) StopCoroutine(fadingCoro);
		fadingCoro = FadingScreen(reveal, obj, components);
		StartCoroutine(fadingCoro);
	}

	private IEnumerator FadingScreen(bool reveal, GameObject obj, Collider[] components)
	{
		var start = reveal ? 0f : 1f;
		var end = reveal ? 1f : 0f;
		faderComp.alpha = start;
		while (timer < fadeTime)
		{
			timer += Time.unscaledDeltaTime;
			faderComp.alpha = Mathf.Lerp(start, end, timer / fadeTime);
			yield return new WaitForEndOfFrame();
		}
		faderComp.alpha = end;
		if (null != components)
			for (int i = 0; i < components.Length; i++)
				components[i].enabled = reveal;
		if (null != obj) obj.SetActive(reveal);
	}
}