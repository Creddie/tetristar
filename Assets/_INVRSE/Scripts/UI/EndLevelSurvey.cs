﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class EndLevelSurvey : MonoBehaviour
{
	public static EndLevelSurvey Instance;
	public static bool HasChosen;

	[SerializeField] private GameObject canvas;
	[SerializeField] private Material imageBW;
	[SerializeField] private Material imageColor;
	[SerializeField] private Image[] stars;
	private int currentRating;
	private ScreenFader faderComp;
	private Collider[] starColliders;

	private void Awake()
	{
		Instance = this;
		HasChosen = false;
		faderComp = GetComponent<ScreenFader>();
		canvas.SetActive(false);
		var temp = new List<Collider>();
		foreach (var v in stars) temp.Add(v.GetComponent<Collider>());
		starColliders = temp.ToArray();
	}

	//private void Start()
	//{
	//	if(null != GameLevel.Instance) transform.position = FindObjectOfType<Player>().transform.position + GameLevel.Instance.Settings.SurveyOffset;
	//}

	public void Fade(bool reveal)
	{
		if (reveal) canvas.SetActive(true);
		faderComp.FadeScreen(reveal, null, starColliders);
	}

	public void UpdateStars(Image sender, bool getLit, bool submit)
	{
		currentRating = System.Array.IndexOf(stars, sender);
		ExecuteChange(currentRating);
		if (submit)
		{
			HasChosen = true;
			Fade(false);
			EndScreen.Instance.LoadLevel(currentRating);
			var evt = new EventCSVLine(EventCSVLine.EventType.LevelSurvey);
			evt.LevelRating = currentRating.ToString() + " / " + stars.Length.ToString();
			TelemetryManager.LogEvent(evt);
		}
	}

	private void ExecuteChange(int rating)
	{
		for(int i = 0; i < stars.Length; i++)
			stars[i].material = i <= currentRating ? imageColor : imageBW;
	}
}