﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditScroller : MonoBehaviour
{
	[SerializeField] private float scrollSpeed;
	[SerializeField] private RectTransform credits;
	[SerializeField] private AudioClip song;

	private void Start()
	{
		MusicManager.Instance.FadeMusic(true, song);
	}

	private void Update()
	{
		credits.localPosition = new Vector3(credits.localPosition.x, credits.localPosition.y + (scrollSpeed * Time.deltaTime), credits.localPosition.z);
	}
}