﻿using System.Collections;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class EndScreen : MonoBehaviour
{
	#region Variables
	public static EndScreen Instance;

	[Header("References")]
	[SerializeField] private SharedGameSettings settings;
	[SerializeField] private LevelList LevelList;
	private bool beenClicked;
	[SerializeField] private ScreenFader buttonFader;
	[SerializeField] private ScreenFader bonusFader;
	[SerializeField] private EndLevelSurvey survey;
	public AudioClip FanfareIntro;
	public AudioClip FanfareLoop;
	public AudioClip LoserIntro;
	public AudioClip LoserLoop;
	[SerializeField] private Animator starsAnimationComp;
	[SerializeField] private Collider[] buttonColliders;
	private AudioSource thisAudioSource;
	[HideInInspector] public int StarsEarned;

	[Header("Animation")]
	[SerializeField] private AnimationClip[] starsAnimations;

	[Header("Settings")]
	[SerializeField] private float delayAfterStars = 1f;
	[SerializeField] private float delayAfterBonus = 3f;

	[Header("Score Bonus")]
	[SerializeField] private int[] bonusPoints;
	#endregion

	protected void Awake()
	{
		Instance = this;
		thisAudioSource = GetComponent<AudioSource>();
		starsAnimationComp.gameObject.SetActive(false);
	}

	public void Activate()
	{
		PlayerSpinControls.Instance.gameObject.SetActive(false);
		StartCoroutine(DoBonus());
	}

	private IEnumerator DoBonus()
	{
		if(GameLevel.DidWin)
		{
			var bonus = 0;
			var moves = PlayerHUD.MovesTaken - GameLevel.Instance.ScoreBonusMoveCounterOffset;
			if (moves <= bonusPoints.Length) bonus = bonusPoints[moves - 1];
			var bonusFormatted = bonus == 0 ? "0" : string.Format("{0:#,#}", bonus);
			bonusFader.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText("Bonus + " + bonusFormatted);
			//bonusFader.FadeScreen(true, null, null);
			var scaler = bonusFader.GetComponent<WordBubbleScaler>();
			scaler.ChangeState(true);
			yield return new WaitForSecondsRealtime(bonusFader.fadeTime);
			//Scoreboard.LevelScore += bonus;
			Scoreboard.Instance.UpdateScore(bonus);
			PlaySound("CelebrationToast");
			yield return new WaitForSecondsRealtime(delayAfterBonus);
			//bonusFader.FadeScreen(false, null, null);
			scaler.ChangeState(false);
		}
		EndLevelTelemetry();
		StartCoroutine(DoStars());
	}

	private static void EndLevelTelemetry()
	{
		var evt = new EventCSVLine(EventCSVLine.EventType.FinishLevel);
		evt.TimeTaken = PlayerHUD.LevelTimer.ToString();
		evt.MovesTaken = PlayerHUD.MovesTaken.ToString();
		evt.TotalScore = Scoreboard.LevelScore.ToString();
		evt.TimeInStory = StoryManager.Instance.TimeInStory.ToString();
		TelemetryManager.LogEvent(evt);
	}

	private IEnumerator DoStars()
	{
		starsAnimationComp.gameObject.SetActive(true);
		StarsEarned = 0;
		var delay = 0f;
		if(GameLevel.DidWin)
		{
			///calculate stars earned
			StarsEarned = 1;
			if (Scoreboard.LevelScore >= GameLevel.Instance.Score3Stars) StarsEarned = 3;
			else if (Scoreboard.LevelScore >= GameLevel.Instance.Score2Stars) StarsEarned = 2;
			///save progression data
			var nam = SceneManager.GetActiveScene().name;
			var key = "highscore_" + nam;
			var data = new SaveDataCSV(key, Scoreboard.LevelScore, StarsEarned);
			SaveLoadManager.SaveLevelProgression(data);
			var scoreData = new Dictionary<string, object>() { { nam, Scoreboard.LevelScore } };
			AnalyticsEvent.LevelComplete(nam, scoreData);
		}
		delay = starsAnimations[0].length;
		starsAnimationComp.SetTrigger("Spawn");
		yield return new WaitForSecondsRealtime(delay);
		for (int i = 0; i < StarsEarned; i++)
		{
			var offset = i + 1;
			delay = starsAnimations[offset].length;
			var nam = "Star" + offset.ToString();
			starsAnimationComp.SetTrigger(nam);
			yield return new WaitForSecondsRealtime(delay);
			//TODO: vfx
		}
		yield return new WaitForSecondsRealtime(delayAfterStars);
		Collider[] arr;
		if (!GameLevel.DidWin)
		{
			buttonColliders[1].GetComponentInChildren<Image>().sprite = GameLevel.Instance.Settings.InactiveMenuButtonSprite;
			arr = new Collider[2] { buttonColliders[0], buttonColliders[2] };
		}
		else arr = buttonColliders;
		buttonFader.FadeScreen(true, null, arr);
	}

	public void LoadLevel(int rating = 0)
	{
		StartCoroutine(LoadingLevel(rating));
	}

	private IEnumerator LoadingLevel(int surveyRating = 0)
	{
		Player.Instance.FadeEverything(false);
		yield return new WaitForSeconds(OVRScreenFade.Instance.fadeTime);
		SceneManager.LoadSceneAsync(LevelList.GetNextLevel());
		//TODO: load map screen instead
	}

	public void LoadSurvey()
	{
		buttonFader.FadeScreen(false, null, buttonColliders);
		PlayerHUD.Instance.ShowHUD(false);
		survey.Fade(true);
	}

	public void PlaySound(string name)
	{
		if (null == thisAudioSource) return;
		SharedGameSettings.SoundEffectInfo s;
		if (GameLevel.Instance.Settings.GetRandomSound(name, out s))
			thisAudioSource.PlayOneShot(s.Clip, s.VolumeAdd);
	}
}