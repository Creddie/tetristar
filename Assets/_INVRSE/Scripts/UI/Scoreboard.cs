﻿using System.Collections;
using TMPro;
using UnityEngine;

public class Scoreboard : MonoBehaviour
{
	public static Scoreboard Instance;
	public static int LevelScore;

	private TextMeshProUGUI scoreLabel;
	private IEnumerator pulseCoro;
	[SerializeField] private float pulseSize = 1.25f;
	private Vector3 pulseScale;
	[SerializeField] private float pulseTime = .5f;
	private float timer;
	[SerializeField] private AnimationCurve sizeCurve;
	private Vector3 normalScale;

	private void Awake()
	{
		Instance = this;
		scoreLabel = GetComponentInChildren<TextMeshProUGUI>();
		LevelScore = 0;
		pulseScale = new Vector3(pulseSize, pulseSize, pulseSize);
		normalScale = scoreLabel.transform.localScale;
	}

	private void Start()
	{
		UpdateScore(0);
	}

	public void UpdateScore(int value)
	{
		LevelScore += value;
		var tex = LevelScore == 0 ? "0" : string.Format("{0:#,#}", LevelScore);
		if (GameLevel.Instance.HasScoreObjective) tex += " / " + PlayerHUD.Instance.ScoreGoal.ToString("#,#");
		scoreLabel.SetText(tex);
		ScorePulse(true);
	}

	public void ScorePulse(bool getLit)
	{
		timer = 0f;
		if (null != pulseCoro) StopCoroutine(pulseCoro);
		pulseCoro = ScorePulsing(getLit);
		StartCoroutine(pulseCoro);
	}

	private IEnumerator ScorePulsing(bool getLit)
	{
		var start = scoreLabel.transform.localScale;
		var end = getLit ? pulseScale : normalScale;
		while(timer < pulseTime)
		{
			scoreLabel.transform.localScale = sizeCurve.Evaluate(timer / pulseTime) * normalScale;
			//scoreLabel.transform.localScale = Vector3.Lerp(start, end, timer / pulseTime);
			timer += Time.unscaledDeltaTime;
			yield return new WaitForEndOfFrame();
		}
		scoreLabel.transform.localScale = end;
		if (getLit) ScorePulse(false);
	}
}