﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndStarsAnimEventCatcher : MonoBehaviour
{
	public void PlaySound()
	{
		//var nam = name == "Spawn" ? "EndLevelStar" : name;
		var nam = string.Empty;
		switch(EndScreen.Instance.StarsEarned)
		{
			case 0: return;
			case 1: nam = "1 Star"; break;
			case 2: nam = "2 Star"; break;
			case 3: nam = "3 Star"; break;
		}
		EndScreen.Instance.PlaySound(nam);
	}
}