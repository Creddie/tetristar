﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipStoryButton : ButtonTemplate, VRInteractable
{
	#region Variables
	public static SkipStoryButton Instance;
	[SerializeField] private StoryManager story;
	#endregion

	protected override void Awake()
	{
		Instance = this;
		base.Awake();
		//normalScale = scalePivot.localScale;
	}

	#region Interface
	public void OnDrag(Vector3 delta) { }
	public bool OnDragEnd() { return false; }
	public bool IsDraggable() { return false; }
	public void OnTouchDrag(Vector2 delta) { }
	public void OnTouchDragEnd() { }
	public bool IsTouchDraggable() { return false; }

	public void OnPointerEnter()
	{
		Highlight(true);
	}

	public void OnPointerExit()
	{
		if (!gameObject.activeSelf)
			return;

		Highlight(false);
	}

	public void OnPointerClick()
	{
		SkipStory();
	}
	#endregion

	private void Highlight(bool getLit)
	{
		Highlight(getLit, scalePivot);
	}

	private void SkipStory()
	{
		PlaySound("ButtonClick");
		story.SkipAhead();
	}
}