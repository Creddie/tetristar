﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class StartGameButton : ButtonTemplate, VRInteractable
{
	public static StartGameButton Instance;
	public static bool LevelSelected;

	[Header("Start Game Settings")]
	[SerializeField] private LevelList levels;
	[SerializeField] private AudioClip themeIntro;
	[SerializeField] private AudioClip themeLoop;
	[SerializeField] private TextMeshProUGUI versionText;
	[SerializeField] private bool newGame = true;
	public Collider[] ConfirmButtons;

	[Header("Start Game Settings")]
	public ScreenFader ConfirmScreen;

	protected override void Awake()
	{
		base.Awake();
		LevelSelected = false;
		normalScale = transform.localScale;
		if (!newGame) return;
		else Instance = this;
		//OVRManager.display.displayFrequency = 60f;
		OVRManager.tiledMultiResLevel = OVRManager.TiledMultiResLevel.LMSHigh;
		versionText.SetText(Application.version.ToString());
	}

	private void Start()
	{
		if (newGame)
		{
			MusicManager.Instance.FadeMusic(false, themeIntro);
			MusicManager.Instance.SetupNextSong(themeIntro.length, themeLoop);
		}
		else
		{
			//StartCoroutine(WaitingToLoad());
			var score = 0;
			int.TryParse(SaveLoadManager.LevelSaveData[0].Highscore, out score);
			if (score == 0) gameObject.SetActive(false);
			//Debug.Log(score);
		}
	}

	//private IEnumerator WaitingToLoad()
	//{
	//	yield return new WaitForEndOfFrame();
	//	var score = 0;
	//	int.TryParse(SaveLoadManager.LevelSaveData[0].Highscore, out score);
	//	if (score == 0) gameObject.SetActive(false);
	//	Debug.Log(score);
	//}

	public void Highlight(bool getLit)
	{
		Highlight(getLit, transform);
	}

	public void StartGame()
	{
		if(newGame) SaveLoadManager.ResetDataNewGame(settings.LevelList);	//new game resets progression data
		StartCoroutine(LoadingGame());
	}

	private IEnumerator LoadingGame()
	{
		Player.Instance.FadeEverything(false);
		//OVRScreenFade.Instance.FadeOut();
		yield return new WaitForSeconds(OVRScreenFade.Instance.fadeTime);
		MusicManager.Instance.StopAllCoroutines();
		var v = newGame ? 2 : 1;
		SceneManager.LoadSceneAsync(levels.LevelNames[v]);	//new game skips over map screen
	}

	public void OnPointerEnter()
	{
		if (LevelSelected || MenuScreen.IsPaused) return;
		Highlight(true);
	}

	public void OnPointerExit()
	{
		if (LevelSelected || (!isLit && MenuScreen.IsPaused)) return;
		Highlight(false);
	}

	public void OnPointerClick()
	{
		if (LevelSelected || MenuScreen.IsPaused) return;
		LevelSelected = true;
		PlaySound("ButtonClick");
		if (newGame)
		{
			Highlight(false);
			ConfirmScreen.FadeScreen(true, null, ConfirmButtons);
		}
		else StartGame();
	}

	public void OnDrag(Vector3 delta) { }
	public bool OnDragEnd() { return false; }
	public bool IsDraggable() { return false; }
	public void OnTouchDrag(Vector2 delta) { }
	public void OnTouchDragEnd() { }
	public bool IsTouchDraggable() { return false; }
}