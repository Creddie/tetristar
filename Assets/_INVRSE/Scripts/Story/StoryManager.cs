﻿using System.Collections;
using UnityEngine;

public class StoryManager : MonoBehaviour
{
	public static StoryManager Instance;
	public static bool IsActive;
	public static bool IsDone = true;

	[Header("Settings")]
	[SerializeField] protected float textRevealDelay = 3f;
	[SerializeField] protected float delayBetweenBubbles = 5f;
	[SerializeField] private float tutorialScaleTime = .5f;
	[SerializeField] private float tutorialDelayTime = 2f;

	[Header("References")]
	[SerializeField] protected AudioClip storySong;
	[SerializeField] protected Transform CharactersParent;
	[SerializeField] private Transform DialogueParent;
	[SerializeField] private SharedGameSettings Settings;
	public GameObject tutorialWindow;
	public GameObject[] Tooltips;
	protected Animation character;

	protected DialogueBox[] dialogues;
	protected int currentWordGroup;
	protected float timer;
	protected bool isLoading = true;
	private float timeStarted;
	private float scaleTimer;
	[HideInInspector] public int TimeInStory;

	protected virtual void Awake()
	{
		Instance = this;
		IsActive = false;
		IsDone = false;
		//transform.SetParent(Player.Instance.PivotUI, true);
		//transform.position = Player.Instance.transform.position + Settings.StoryOffset;
		//if (null != tutorialWindow)
		//{
		//	tutorialWindow.SetActive(false);
		//	tutorialWindow.transform.SetParent(Player.Instance.PivotUI, true);
		//	tutorialWindow.transform.localScale = Vector3.zero;
		//	var offset = tutorialWindow.name.Contains("High") ? Settings.TutorialWindowOffsetHigh : Settings.TutorialWindowOffset;
		//	tutorialWindow.transform.position = Player.Instance.transform.position + offset;
		//}
		if(null != DialogueParent)
		{
			dialogues = DialogueParent.GetComponentsInChildren<DialogueBox>(true);
			character = CharactersParent.GetChild(0).GetComponent<Animation>();
			HideAllChildren(CharactersParent);
			HideAllChildren(DialogueParent);
		}
	}

	private void Start()
	{
		MenuScreen.AcceptsInput = false;

		transform.SetParent(Player.Instance.PivotUI, true);
		transform.position = Player.Instance.transform.position + Settings.StoryOffset;
		if (null != tutorialWindow)
		{
			tutorialWindow.SetActive(false);
			tutorialWindow.transform.SetParent(Player.Instance.PivotUI, true);
			tutorialWindow.transform.localScale = Vector3.zero;
			var offset = tutorialWindow.name.Contains("High") ? Settings.TutorialWindowOffsetHigh : Settings.TutorialWindowOffset;
			tutorialWindow.transform.position = Player.Instance.transform.position + offset;
		}

		StartCoroutine(WaitingToStart());
	}

	private IEnumerator WaitingToStart()
	{
		yield return new WaitForEndOfFrame();
		//yield return new WaitForEndOfFrame();

		//var offset = SaveLoadManager.StargazerMode() ? new Vector3(Settings.StoryOffset.x, Settings.StoryOffset.z, -Settings.StoryOffset.y) : Settings.StoryOffset;
		//transform.position = Player.Instance.transform.position + offset;

		if (null != DialogueParent)
		{
			if (MusicManager.Instance != null) MusicManager.Instance.FadeMusic(true, storySong);
			DialogueParent.GetChild(0).gameObject.SetActive(false);
			character.gameObject.SetActive(true);
			character.Play("Companion_Enter");
			if (SceneLoading.Instance != null) yield return new WaitForSeconds(SceneLoading.Instance.LoadingTime + OVRScreenFade.Instance.fadeTime);
			timeStarted = Time.time;
			yield return new WaitForSeconds(textRevealDelay);
			ExecuteStart();
		}
		else
		{
			yield return new WaitForSeconds(SceneLoading.Instance.LoadingTime + OVRScreenFade.Instance.fadeTime);
			StartGameplay();
		}
	}

	protected void ExecuteStart()
	{
		isLoading = false;
		IsActive = true;
		currentWordGroup = 0;
		dialogues[currentWordGroup].gameObject.SetActive(true);
		dialogues[currentWordGroup].StartAnimation();
	}

	private void HideAllChildren(Transform trans)
	{
		for (int i = 0; i < trans.childCount; i++)
			trans.GetChild(i).gameObject.SetActive(false);
	}

	protected virtual void Update()
	{
		if (IsActive)
		{
			timer += Time.deltaTime;
			if (timer >= delayBetweenBubbles)
			{
				timer = 0f;
				SkipAhead();
			}
		}
	}

	public virtual void SkipAhead()
	{
		if (isLoading)
		{
			StopAllCoroutines();
			var state = character["Companion_Enter"];
			state.time = state.length;
			ExecuteStart();
			return;
		}

		if (!IsActive) return;

		//skip dialogue box animation
		if (dialogues[currentWordGroup].IsAnimating) dialogues[currentWordGroup].SkipAnimation();
		//next bubble
		else if (currentWordGroup < dialogues.Length - 1)
		{
			dialogues[currentWordGroup].gameObject.SetActive(false);
			currentWordGroup += 1;
			dialogues[currentWordGroup].gameObject.SetActive(true);
			dialogues[currentWordGroup].StartAnimation();
			timer = 0f;
		}
		else StartGameplay();
	}

	protected virtual void StartGameplay(bool characterExits = true)
	{
		if (null != DialogueParent)
		{
			dialogues[currentWordGroup].gameObject.SetActive(false);
			if(characterExits) character.Play("Companion_Exit");
			TimeInStory = Mathf.RoundToInt(Time.time - timeStarted);
			StartCoroutine(WaitingToEnd());
			SkipStoryButton.Instance.transform.GetChild(0).gameObject.SetActive(false);
		}
		else
		{
			MenuScreen.AcceptsInput = true;
			IsDone = true;
		}
		IsActive = false;
		PlayerHUD.Instance.ShowHUD(true);
		PlayerHUD.Instance.StartTimer();
		SetupLevelMusic();
		var basics = FindObjectOfType<PlayerHUD_Tutorial_Basics>();
		if (null == basics) GameLevel.Instance.RevealBoard(true);
		if (null != tutorialWindow)
		{
			//var offset = tutorialWindow.name.Contains("High") ? Settings.TutorialWindowOffsetHigh : Settings.TutorialWindowOffset;
			////offset = SaveLoadManager.StargazerMode() ? new Vector3(offset.x, offset.z, -offset.y) : offset;
			//tutorialWindow.transform.position = Player.Instance.transform.position + offset;
			if (null == basics) StartTutorial();
			//level 2 tutorial
			var p = FindObjectOfType<PlayerHUD_Tutorial_Intro>();
			if (null != p)
			{
				GameLevel.IsInteractive = true;
				p.InitialChange(tutorialDelayTime);
			}
			else
			{
				//level 3 tutorial
				var q = FindObjectOfType<PlayerHUD_Tutorial_Specials>();
				if (null != q)
				{
					GameLevel.IsInteractive = true;
					q.InitialChange(tutorialDelayTime);
				}
				//level 4 tutorial
				else
				{
					var v = FindObjectOfType<PlayerHUD_Tutorial_Hazards>();
					if (null != v) v.StartTooltip(tutorialDelayTime);
				}
			}
		}
		else
		{
			var w = FindObjectOfType<PlayerHUD_Tutorial_Timer>();
			if (null != w) w.StartTooltip();
		}
	}

	private void SetupLevelMusic()
	{
		var levelName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
		var levelIndex = System.Array.IndexOf(Settings.LevelList.LevelNames, levelName) - 1;
		var songIndex = levelIndex % Settings.LevelSongsIntro.Length;
		var songIntro = Settings.LevelSongsIntro[songIndex];
		var songLoop = Settings.LevelSongsLoop[songIndex];
		MusicManager.Instance.ThisAudioSource.loop = true;
		MusicManager.Instance.FadeMusic(true, songIntro);
		MusicManager.Instance.SetupNextSong(songIntro.length, songLoop);
	}

	public void StartTutorial()
	{
		StartCoroutine(WaitingToStartTutorial());
	}

	private IEnumerator WaitingToStartTutorial()
	{
		yield return new WaitForSeconds(tutorialDelayTime);
		tutorialWindow.SetActive(true);
		ToggleTutorial(true);
	}

	public void ToggleTutorial(bool getLit)
	{
		if (null == tutorialWindow) return;
		PlayerHUD.TooltipIsActive = getLit;
		StartCoroutine(ScalingTutorial(getLit));
	}

	private IEnumerator ScalingTutorial(bool getLit)
	{
		scaleTimer = 0f;
		while(scaleTimer < tutorialScaleTime)
		{
			scaleTimer += Time.deltaTime;
			var curve = getLit ? GameLevel.Instance.Settings.SizeCurveTooltipGrow : GameLevel.Instance.Settings.SizeCurveTooltipShrink;
			tutorialWindow.transform.localScale = curve.Evaluate(scaleTimer / tutorialScaleTime) * Vector3.one;
			yield return new WaitForEndOfFrame();
		}
	}

	private IEnumerator WaitingToEnd()
	{
		yield return new WaitForSeconds(.5f);
		SkipStoryButton.Instance.gameObject.SetActive(false);
		yield return new WaitForSeconds(3f);
		CharactersParent.gameObject.SetActive(false);
		IsDone = true;
		MenuScreen.AcceptsInput = true;
	}
}