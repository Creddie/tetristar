﻿using UnityEngine;

public class DialogueBox : MonoBehaviour
{
	public float TextRevealInterval = .05f;

	private TMPro.TextMeshProUGUI Text;
	private string MyText;
	public bool IsAnimating { get; private set; }
	private float Clock;
	private int i;

	private void Awake()
	{
		Text = GetComponentInChildren<TMPro.TextMeshProUGUI>();
		if (Text == null)
			Debug.LogError(typeof(DialogueBox).Name + " couldn't find TextMeshPro component");
		else
			MyText = Text.text;
		IsAnimating = false;
	}

	public void StartAnimation()
	{
		Text.text = string.Empty;
		i = 0;
		Clock = Time.time;
		IsAnimating = true;
	}

	public void SkipAnimation()
	{
		Text.text = MyText;
		IsAnimating = false;
	}

	private void Update()
	{
		if (!IsAnimating)
			return;

		if(Time.time - Clock > TextRevealInterval)
		{
			Clock = Time.time;
			i++;
			Text.text = MyText.Substring(0, i);
			if (i >= MyText.Length)
				IsAnimating = false;
		}
	}
}
