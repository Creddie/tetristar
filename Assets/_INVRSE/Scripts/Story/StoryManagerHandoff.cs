﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryManagerHandoff : StoryManager
{
	private Animation otherCharacter;
	private bool otherCharacterReady = true;
	[SerializeField] private float otherCharacterLeaveDelay = 1f;

	protected override void Awake()
	{
		base.Awake();
		otherCharacter = CharactersParent.GetChild(1).GetComponent<Animation>();
	}

	protected override void Update()
	{
		if (!otherCharacterReady) return;
		base.Update();
	}

	private IEnumerator WaitingForOtherCharacter()
	{
		otherCharacterReady = false;
		yield return new WaitForSeconds(textRevealDelay);
		dialogues[currentWordGroup].gameObject.SetActive(true);
		dialogues[currentWordGroup].StartAnimation();
		otherCharacterReady = true;
	}

	private IEnumerator WaitingForOtherCharacterToLeave()
	{
		otherCharacter.Play("Companion_Exit");
		yield return new WaitForSeconds(otherCharacterLeaveDelay);
		character.Play("Companion_Exit");
	}

	protected override void StartGameplay(bool characterExits = true)
	{
		base.StartGameplay(false);
		StartCoroutine(WaitingForOtherCharacterToLeave());
	}

	public override void SkipAhead()
	{
		if (isLoading)
		{
			StopAllCoroutines();
			var state = character["Companion_Enter"];
			state.time = state.length;
			ExecuteStart();
			return;
		}

		if (!IsActive) return;

		//skip dialogue box animation
		if (dialogues[currentWordGroup].IsAnimating) dialogues[currentWordGroup].SkipAnimation();
		//next bubble
		else if (currentWordGroup < dialogues.Length - 1)
		{
			dialogues[currentWordGroup].gameObject.SetActive(false);
			currentWordGroup += 1;
			timer = 0f;

			if (currentWordGroup == 1)
			{
				otherCharacter.gameObject.SetActive(true);
				otherCharacter.Play("Companion_Enter");
				StartCoroutine(WaitingForOtherCharacter());
			}
			else
			{
				if (!otherCharacterReady)
				{
					StopAllCoroutines();
					otherCharacterReady = true;
					var state = otherCharacter["Companion_Enter"];
					state.time = state.length;
					currentWordGroup -= 1;
				}
				dialogues[currentWordGroup].gameObject.SetActive(true);
				dialogues[currentWordGroup].StartAnimation();
			}
		}
		else StartGameplay();
	}
}