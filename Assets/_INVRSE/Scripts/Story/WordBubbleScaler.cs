﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordBubbleScaler : MonoBehaviour
{
	public float ScaleTime = .5f;
	private float timer;
	private Vector3 normalScale;
	private IEnumerator scalingCoro;
	[SerializeField] private bool startsActive;

	private void Awake()
	{
		normalScale = transform.localScale;
		if(!startsActive) transform.localScale = Vector3.zero;
	}

	private void OnEnable()
	{
		if (!startsActive && name != "Bonus") ChangeState(true);
	}

	public void ChangeState(bool getLit)
	{
		if (null != scalingCoro) StopCoroutine(scalingCoro);
		scalingCoro = Scaling(getLit);
		if(gameObject.activeSelf) StartCoroutine(scalingCoro);
	}

	private IEnumerator Scaling(bool getLit)
	{
		timer = 0f;
		var curve = getLit ? Player.Instance.Settings.SizeCurveTooltipGrow : Player.Instance.Settings.SizeCurveTooltipShrink;
		while(timer < ScaleTime)
		{
			timer += Time.deltaTime;
			transform.localScale = curve.Evaluate(timer / ScaleTime) * normalScale;
			yield return new WaitForEndOfFrame();
		}
		if (!getLit) gameObject.SetActive(false);
	}
}