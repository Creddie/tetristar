﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CutSceneIntroManager : MonoBehaviour
{
	public LevelList levels;
	[SerializeField] private GameObject balloons;
	[SerializeField] private GameObject bottle;
	[SerializeField] private GameObject mailManBottle;
	[SerializeField] private AudioClip cutsceneSong;
	[SerializeField] private float timerAdjust = 5f;
	private float timer;
	private bool isFading;

	private void Awake()
	{
		mailManBottle.SetActive(false);
		//SaveLoadManager.Init(levels);
	}

	private void FixedUpdate()
	{
		timer += Time.fixedDeltaTime;
		if (timer >= cutsceneSong.length - timerAdjust)
		{
			timer = 0f;
			Debug.Log("load level");
			LoadLevel();
		}
	}

	private void Start()
	{
		MusicManager.Instance.ThisAudioSource.loop = false;
		MusicManager.Instance.FadeMusic(true, cutsceneSong);
		MusicManager.Instance.ThisAudioSource.loop = false;
	}

	//private IEnumerator WaitingToLoad()
	//{
	//	yield return new WaitForEndOfFrame();
	//	yield return new WaitForEndOfFrame();
	//	if(SaveLoadManager.StargazerMode()) RenderSettings.skybox.
	//}

	public void Handoff()
	{
		balloons.SetActive(false);
		bottle.SetActive(false);
		mailManBottle.SetActive(true);
	}

	public void FadeOut()
	{
		isFading = true;
		OVRScreenFade.Instance.FadeOut();
	}

	private void LoadLevel()
	{
		if (!isFading) FadeOut();
		StartCoroutine(LoadingLevel());
	}

	private IEnumerator LoadingLevel()
	{
		yield return new WaitForSeconds(OVRScreenFade.Instance.fadeTime);
		MusicManager.Instance.ThisAudioSource.loop = true;
		SceneManager.LoadSceneAsync(levels.GetNextLevel());
	}
}