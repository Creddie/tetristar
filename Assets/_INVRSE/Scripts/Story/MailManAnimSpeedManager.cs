﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MailManAnimSpeedManager : MonoBehaviour
{
	[SerializeField] private float speed = .5f;
	private Animation animComp;

	private void Awake()
	{
		animComp = GetComponent<Animation>();
		animComp["Companion_Enter"].speed = speed;
		animComp["Companion_Exit"].speed = speed;
	}
}